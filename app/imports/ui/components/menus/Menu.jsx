import React, {Component} from 'react';
import { BrowserRouter as Link } from "react-router-dom";
import { withTracker } from 'meteor/react-meteor-data';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';

import { FaDatabase, FaColumns, FaListAlt, FaUsers, FaCogs, FaComment } from 'react-icons/fa';

import MenuItem from './MenuItem';
import NestedMenuItem from './NestedMenuItem';

import {Collection_Roles} from '../../../api/collections';

class MenuContainer extends Component{

  render(){
    let props = this.props;
    
    return (
      <List>
      {
        props.pages.map(function (page, key) {
          if (page.subPages && page.subPages.length > 0){
            return (
              <NestedMenuItem match={props.match} icon={page.menu.icon} rolePermission={props.rolePermission} subPages={page.subPages} key={key} text={page.menu.text} />
            );
          }else{
              if (page.adminAccessOnly) {
                if (props.rolePermission && props.rolePermission.allAccess && props.rolePermission.adminAccessOnly){
                    return (
                      <MenuItem match={props.match} icon={page.menu.icon} key={key} text={page.menu.text} url={page.url} />
                    );
                }
              } else {
                if (props.rolePermission && props.rolePermission.allAccess){
                    return (
                      <MenuItem match={props.match} icon={page.menu.icon} key={key} text={page.menu.text} url={page.url} />
                    );
                  }else{
                    if (props.rolePermission && Object.keys(props.rolePermission).length > 0 && (props.rolePermission[page.name + '_access'] || page.name == 'Dashboard')){
                      return (
                        <MenuItem match={props.match} icon={page.menu.icon} key={key} text={page.menu.text} url={page.url} />
                      );
                    }
                  }
              }
            
          }
        })
      }
      </List>
    );
  }
  

}

export default Menu = withTracker((props) => {
  let rolePermission = {};
  if (Meteor.user()){
    if (Meteor.user().profile.role == 'evarosoft_owner'){
        rolePermission.allAccess = true;
        rolePermission.adminAccessOnly = true;
    }else if((Collection_Roles.findOne({rolename: 'admin'}) && Meteor.user().profile.role == Collection_Roles.findOne({rolename: 'admin'})._id)){
        rolePermission.allAccess = true;
        rolePermission.adminAccessOnly = false;
    }else if (Meteor.user().profile.role == 'owner'){
        rolePermission.allAccess = true;
        rolePermission.adminAccessOnly = true;
    }else{
        rolePermission = Collection_Roles.findOne(Meteor.user().profile.role);
        rolePermission.adminAccessOnly = false;
    }
  }
  
  return {
    rolePermission,
  };
})(MenuContainer);
