import React from 'react';
import { BrowserRouter as Link } from "react-router-dom";

import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconLibraryBooks from '@material-ui/icons/LibraryBooks'
import IconExpandLess from '@material-ui/icons/ExpandLess';
import IconExpandMore from '@material-ui/icons/ExpandMore';
import IconDashboard from '@material-ui/icons/Dashboard';
import IconShoppingCart from '@material-ui/icons/ShoppingCart';
import IconPeople from '@material-ui/icons/People';
import IconBarChart from '@material-ui/icons/BarChart';

import { FaDatabase, FaColumns, FaListAlt, FaUsers, FaCogs, FaComment } from 'react-icons/fa';

import MenuItem from './MenuItem';

export default function NestedMenuItem (props){


  var subMenuSelected = false;
  props.subPages.forEach((page, key) => {
    if (props.match.path == ('/:appname' + page.url)){
      subMenuSelected = true;
    }
  });

  const [nestedOpen, setNestedOpen] = React.useState(subMenuSelected);

  function handleNestedClick() {
    setNestedOpen(!nestedOpen);
  }

  function checkForParentMenu(){
    var visible = false;
    props.subPages.map(function (page, key) {
      if (props.rolePermission && Object.keys(props.rolePermission).length > 0 && props.rolePermission[page.name + '_access']){
        visible = true;
      }
    });
    if (props.rolePermission && props.rolePermission.allAccess) visible = true;
    return visible;
  }

  return (
    <div>
      {checkForParentMenu() ? 
      <div>
        <ListItem button onClick={handleNestedClick}>
        <ListItemIcon>
          {props.icon}
        </ListItemIcon>
        <ListItemText primary={props.text} />
        {nestedOpen ? <IconExpandLess /> : <IconExpandMore />}
        </ListItem>
        <Collapse in={nestedOpen} timeout="auto" unmountOnExit>
          <Divider />
          <List component="div" disablePadding>
            {
              props.subPages.map(function (page, key) {
                if (props.rolePermission && Object.keys(props.rolePermission).length > 0 && (props.rolePermission.allAccess || props.rolePermission[page.name + '_access'])){
                  return (
                    <MenuItem match={props.match} key={key} text={page.menu.text} icon={page.menu.icon} url={page.url} />
                  );
                }
              })
            }
          </List>
          <Divider />
        </Collapse>
      </div>: ""
      }
      
    </div>
  );

}
