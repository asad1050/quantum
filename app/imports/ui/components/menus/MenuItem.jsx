import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';

import { FaDatabase, FaColumns, FaListAlt, FaUsers, FaCogs, FaComment, FaCaretRight } from 'react-icons/fa';
const drawerWidth = 240;
const useStyles = makeStyles(theme =>
  createStyles({
    appMenu: {
      width: '100%',
    },
    navList: {
      width: drawerWidth,
    },
    menuItem: {
      width: drawerWidth,
    },
    menuItemIcon: {
      color: '#97c05c',
    },
    linkStyle: {
      textDecoration: 'none',
      color: 'black',
    },
  }),
);

export default function MenuItem (props){
  const classes = useStyles();
  return (
    <Link to={'/' + sessionStorage.getItem("appname") + props.url} className={classes.linkStyle} >
      <ListItem button className={classes.menuItem} selected={(props.match.path == ('/:appname' + props.url)) ? true : false}>
        {
          (props.icon ?
            <ListItemIcon>
              {props.icon}
            </ListItemIcon>
            :
            <ListItemIcon>
              <FaCaretRight />
            </ListItemIcon>
          )
        }
        <ListItemText primary={props.text} />
      </ListItem>
    </Link>
  );

}
