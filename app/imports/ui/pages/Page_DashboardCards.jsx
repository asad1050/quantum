import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

import { FaEye, FaCopy, FaEdit, FaTrash, FaPlusSquare } from 'react-icons/fa';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

class Page_DashboardCards extends Component {

  render() {
    let apps = this.props.apps;
    return (
      <Styles>
        <Container maxWidth="lg" className="container">
          <Grid container spacing={3}>
            {/* Cards */}
            <Grid item xs={12} md={4} lg={3} >
              <Card className="card">
                <CardActionArea>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2" className="app-title" >
                        Welcome!
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p" className="card-description" >
                        to Quantum Business Management Software
                      </Typography>
                    </CardContent>
                  </CardActionArea>
              </Card>
            </Grid>

          </Grid>
        </Container>
      </Styles>
    );
  }
}

export default Page_DashboardCardsContainer = withTracker(() => {
  const appsHandle = Meteor.subscribe('apps');
  const loading = !appsHandle.ready();
  const apps = [];
  // const apps = AppCollection.find().fetch();
  const appsExists = !loading && !!apps;
  return {
    loading,
    apps,
    appsExists,
  };
})(Page_DashboardCards);
