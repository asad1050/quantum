import React from 'react';
import swal from 'sweetalert2';
import $ from 'jquery';
import styled from 'styled-components';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import GlobalConfig from '../../config/global.config'

const Styles = styled.div`
  .forgot-password{
    display: none;
  }
  .signup{
    display: none;
  }
  .business-name{
    text-align: center;
    padding-top: 5%;
  }
`
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" target='_blank' href="https://evarosoft.com/">
        Evarosoft
      </Link>{' '}
      {new Date().getFullYear()} 
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function handleSignin(props, event) {
  event.preventDefault();
  const email = $('#email').val().trim();
  const password = $('#password').val().trim();
  swal.fire({
    title: 'Authenticating',
    onBeforeOpen: () => {
      swal.showLoading();
      Meteor.loginWithPassword(email, password, (err, res) => {
        if (err){
          swal.fire({
            type: 'error',
            title: 'Oops...',
            text: err.message,
          });
        }else{
          if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.appname && Meteor.user().profile.appid && ((Meteor.user().profile.appname == sessionStorage.getItem("appname") && Meteor.user().profile.appid == sessionStorage.getItem("appid")) || Meteor.user().profile.appid === 'evarosoft_owner')){
            sessionStorage.setItem("authuserid", Meteor.userId());
            Meteor.call('app.initData', sessionStorage.getItem("appid"));
            sessionStorage.setItem("username", Meteor.user().username);
            if (Meteor.user().profile.appunit) {
                sessionStorage.setItem("appunit", Meteor.user().profile.appunit);
            } else {
                Meteor.call('app.fetchMainUnit', Meteor.user().profile.appid, (err, mainApp) => {
                    sessionStorage.setItem("appunit", mainApp._id);
                });
            }
            sessionStorage.setItem("userfullname", (Meteor.user().profile.firstName + ' ' + Meteor.user().profile.lastName));
            swal.fire({
              type: 'success',
              title: 'Cheers',
              text: 'You have signed in successfully',
            });
            $('#email').val('');
            $('#password').val('');
            window.location.href = '/' + props.match.params.appname + '/dashboard';
          }else{
            swal.fire({
              type: 'error',
              title: 'Oops...',
              text: 'Wrong email/username or password',
            });
          }
        }
      });
    },
  });
}

export default function SignIn(props) {
  const classes = useStyles();
  return (
    <Styles>
        <Typography component="h1" className="business-name" variant="h5">
            {GlobalConfig().appName}
          </Typography>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} onSubmit={handleSignin.bind(this, props)}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address / Username"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" className="forgot-password" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="signup" className="signup" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    </Styles>
  );
}
