import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Purchase, Collection_ProductItems, Collection_Suppliers, Collection_PurchasePayment } from '../../../api/collections';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_PurchasePayment extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_PurchasePayment",
      title: "Purchase Payment",
      extraIds: [
        { prefix: 'pp', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_PurchasePayment,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      changeOtherCollectionDataOnCreation: [
        {
          type: 'directValue',
          collection: Collection_Purchase._name,
          entryKey: 'editable',
          entryValue: false,
          DBCollectionId: 'purchase.id',
        },
        {
          type: 'directValue',
          collection: Collection_Purchase._name,
          entryKey: 'deletable',
          entryValue: false,
          DBCollectionId: 'purchase.id',
        },
      ],
      formEntryLogics: [//Checked at add and update
        {
          entry1: 'paymentamount',
          entry2: 'pendingamount',
          operand: '<='
        },
      ],
      formComponentLogics: [//Perform logics while editing (only for numerics)
        {
          entry1: 'totalamount',
          entry2: 'paidamount',
          operand: '-',
          result: 'pendingamount',
        },
      ],
      restrictUpdate: true,
      createSuccessMessage: "Payment added successfully",
      createErrorMessage: "Error while adding payment",
      updateSuccessMessage: "",
      updateErrorMessage: "",
      deleteSuccessMessage: "Payment deleted successfully",
      deleteErrorMessage: "Error while deleting payment",
      instantSearchConfig: {
        instantSearchLogic: {'orderstatus': 'pending'},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: true,
        textChangeSearch: true,
        detailedSearchLogic: {'orderstatus': 'pending'},
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'ordersupplier', label: 'Supplier'},
          { dbName: 'purchaseorderdate', label: 'Purchased On'},
        ],
        detailedSearchBy: [
          {
            type: 'datebox',
            id: 'paymentreceivedate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Purchased On",
            size: [12,6,6],//XS, MD, LG
            dateRange: false,
          },
          {
            type: 'datebox',
            id: 'expectedon',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Expected On",
            size: [12,6,6],//XS, MD, LG
            dateRange: false,
          },
          {
            type: 'autocomplete',
            id: 'ordersupplier',
            required: false,
            disabled: false,
            label: "Order Supplier",
            size: [12,12,12],//XS, MD, LG
            dataCollection: Collection_Suppliers,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'suppliername'],//Show specific keys in search
            data: [],// also Required if blank
          },
        ],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'autocomplete',
          id: 'purchase',
          required: true,
          disabled: false,
          label: "Unpaid Purchase Order",
          size: [12,12,12],//XS, MD, LG
          dataCollection: Collection_Purchase,
          dataLogic: { $where : "this.totalamount > this.paidamount"},
          uniqueKey: 'eid',
          searchKeys: ['eid', 'ordersupplier.data.suppliername'],//Show specific keys in search
          data: [],// also Required if blank
          onSelectCallback: function (data, form) {
            form.setState({'orderitems': data.orderitems_simple});
            form.setState({'totalamount': data.totalamount});
            form.setState({'paidamount': data.paidamount});
            setTimeout(() => {
              $('#paidamount').keyup(); 
            });
          },
        },
        {
          type: 'textbox',
          id: 'paymentnote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'multiselecttable',
          id: 'orderitems',
          label: "Items",
          size: [12,12,12],//XS, MD, LG
          options: { //--- All options on this url: https://material-table.com/#/docs/all-props
            actionsColumnIndex: -1,
            exportButton: true,
            exportFileName: 'order items',
            paging: false,
            pageSize: 20,
            pageSizeOptions: [20, 30, 40],
            search: false,
            sorting: false,
            isEditable: false,
            isDeletable: false,
            isCreatable: false,
          },
          columns: [
            { fieldType: 'custom', title: "Items", required: true, component: {
              type: 'autocomplete',
              id: 'purchaseitems',
              dataCollection: Collection_ProductItems,
              uniqueKey: 'eid',
              searchKeys: ['eid', 'itemname', 'itembarcode'],//Show specific keys in search
              extraInformation: [
                {
                  key: 'itemquantity',
                  label: 'Qty',
                },
                {
                  key: 'itemprice',
                  label: 'Price',
                },
              ],
              data: [],// also Required if blank
            } },
            { fieldType: 'field', title: "Quantity", field: 'purchasequantity', required: true, type:"numeric", emptyValue: 0 },
            { fieldType: 'field', title: "Purchase Cost", field: 'purchasecost', required: true, type:"numeric", emptyValue: 0 },
            { fieldType: 'field', title: "Total", field: 'totalcost', type:"numeric", editable: 'never', logic: {entry1: 'purchasequantity', entry2: 'purchasecost', operand: '*'} },
          ],
        },
        {
          type: 'textbox',
          id: 'totalamount',
          required: false,
          disabled: true,
          label: "Total Amount",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'paidamount',
          required: false,
          disabled: true,
          label: "Paid Amount",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'pendingamount',
          required: false,
          disabled: true,
          label: "Pending Amount",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'paymentamount',
          required: true,
          disabled: false,
          label: "Payment Amount",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          DBLogicalRelations: [
            {
              action: 'add', DBCollection: Collection_Purchase._name, DBCollectionKey: 'paidamount', DBCollectionId: 'purchase.id',
            }
          ],
          accountRelations: [
            {
              credit: { action: 'byAccountName', DBCollectionKey: 'Cash In Hand' },
              debit: { action: 'byDataKey', DBCollectionKey: 'purchase.data.ordersupplier.data.account' },
              description: "Payment paid against purchase",
            },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
