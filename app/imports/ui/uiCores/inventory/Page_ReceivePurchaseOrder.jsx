import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_PurchaseOrder, Collection_ProductItems, Collection_Suppliers, Collection_ReceivePurchaseOrder } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_ReceivePurchaseOrder extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_ReceivePurchaseOrder",
      title: "Receive Purchase Order",
      extraIds: [
        { prefix: 'rpo', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_ReceivePurchaseOrder,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      restrictUpdate: true,
      createSuccessMessage: "Prucahse Order received successfully",
      createErrorMessage: "Error while receiving Prucahse Order",
      updateSuccessMessage: "Prucahse Order received successfully",
      updateErrorMessage: "Error while receiving Prucahse Order",
      deleteSuccessMessage: "Prucahse Order received successfully",
      deleteErrorMessage: "Error while deleting Prucahse Order",
      instantSearchConfig: {
        instantSearchLogic: {'orderstatus': 'pending'},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: true,
        textChangeSearch: true,
        detailedSearchLogic: {'orderstatus': 'pending'},
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'ordersupplier', label: 'Supplier'},
          { dbName: 'purchaseorderdate', label: 'Purchased On'},
          { dbName: 'expectedon', label: 'Expected On'},
        ],
        detailedSearchBy: [
          {
            type: 'datebox',
            id: 'purchaseorderdate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Purchased On",
            size: [12,6,6],//XS, MD, LG
            dateRange: false,
          },
          {
            type: 'datebox',
            id: 'expectedon',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Expected On",
            size: [12,6,6],//XS, MD, LG
            dateRange: false,
          },
          {
            type: 'autocomplete',
            id: 'ordersupplier',
            required: false,
            disabled: false,
            label: "Order Supplier",
            size: [12,12,12],//XS, MD, LG
            dataCollection: Collection_Suppliers,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'suppliername'],//Show specific keys in search
            data: [],// also Required if blank
          },
        ],
      },
      defaultEntries: [
        {
          name: 'quantityreceived',
          value: 0,
        },
        {
          name: 'orderstatus',
          value: 'pending',
        },
      ],
      components: [
        {
          type: 'autocomplete',
          id: 'purchaseorder',
          required: true,
          disabled: false,
          label: "Purchase Order",
          size: [12,12,12],//XS, MD, LG
          dataCollection: Collection_PurchaseOrder,
          dataLogic: {orderstatus: 'pending', orderstatus: 'partiallyreceived'},
          uniqueKey: 'eid',
          searchKeys: ['eid', 'ordersupplier.data.suppliername'],//Show specific keys in search
          data: [],// also Required if blank
          onSelectCallback: function (data, form) {
            console.log("DATA: ", data);
            console.log("THIS: ", this);
            form.setState({'orderitems': data.orderitems});
          },
        },
        {
          type: 'textbox',
          id: 'ordernote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'multiselecttable',
          id: 'orderitems',
          label: "Items",
          size: [12,12,12],//XS, MD, LG
          options: { //--- All options on this url: https://material-table.com/#/docs/all-props
            actionsColumnIndex: -1,
            exportButton: true,
            exportFileName: 'order items',
            paging: false,
            pageSize: 20,
            pageSizeOptions: [20, 30, 40],
            search: false,
            sorting: false,
          },
          columns: [
            { fieldType: 'custom', title: "Items", required: true, component: {
              type: 'autocomplete',
              id: 'purchaseitems',
              dataCollection: Collection_ProductItems,
              uniqueKey: 'eid',
              searchKeys: ['eid', 'itemname', 'itembarcode'],//Show specific keys in search
              extraInformation: [
                {
                  key: 'itemquantity',
                  label: 'Qty',
                },
                {
                  key: 'itemprice',
                  label: 'Price',
                },
              ],
              data: [],// also Required if blank
            } },
            { fieldType: 'field', title: "Quantity", field: 'purchasequantity', required: true, type:"numeric", emptyValue: 0 },
            { fieldType: 'field', title: "Purchase Cost", field: 'purchasecost', required: true, type:"numeric", emptyValue: 0 },
            { fieldType: 'field', title: "Total", field: 'totalcost', type:"numeric", editable: 'never', logic: {entry1: 'purchasequantity', entry2: 'purchasecost', operand: '*'} },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
