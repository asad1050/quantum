import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Purchase, Collection_ProductItems, Collection_Suppliers } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_Purchase extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_Purchase",
      title: "Purchase",
      extraIds: [
        { prefix: 'p', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_Purchase,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      createSuccessMessage: "Prucahse Order created successfully",
      createErrorMessage: "Error while creating Prucahse Order",
      updateSuccessMessage: "Prucahse Order updated successfully",
      updateErrorMessage: "Error while updating Prucahse Order",
      deleteSuccessMessage: "Prucahse Order deleted successfully",
      deleteErrorMessage: "Error while deleting Prucahse Order",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: true,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'ordersupplier', label: 'Supplier'},
          { dbName: 'purchaseorderdate', label: 'Purchased On', formatDate: true},
          { dbName: 'supplierinvoicenumber', label: 'Invoice #'},
          { dbName: 'totalamount', label: 'Total Amount'},
          { dbName: 'paidamount', label: 'Paid Amount'},
        ],
        detailedSearchBy: [
          {
            type: 'datebox',
            id: 'purchaseorderdate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Purchased On",
            size: [12,12,12],//XS, MD, LG
            dateRange: true,
          },
          {
            type: 'autocomplete',
            id: 'ordersupplier',
            required: false,
            disabled: false,
            label: "Order Supplier",
            size: [12,12,12],//XS, MD, LG
            dataCollection: Collection_Suppliers,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'suppliername'],//Show specific keys in search
            data: [],// also Required if blank
          },
          {
            type: 'textbox',
            id: 'supplierinvoicenumber',
            required: false,
            disabled: false,
            label: "Supplier Invoice Number",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'eid',
            required: false,
            disabled: false,
            label: "Order Id",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
        ],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'autocomplete',
          id: 'ordersupplier',
          required: true,
          disabled: false,
          label: "Supplier",
          size: [12,12,12],//XS, MD, LG
          dataCollection: Collection_Suppliers,
          uniqueKey: 'eid',
          searchKeys: ['eid', 'suppliername'],//Show specific keys in search
          data: [],// also Required if blank
        },
        {
          type: 'datebox',
          id: 'purchaseorderdate',
          format: 'dd/MM/yyyy',
          required: true,
          disabled: false,
          label: "Purchase Order Date",
          size: [12,6,6],//XS, MD, LG
        },
        {
          type: 'textbox',
          id: 'supplierinvoicenumber',
          required: false,
          disabled: false,
          label: "Supplier Invoice Number",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'ordernote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'multiselecttable',
          id: 'orderitems',
          label: "Items",
          required: true,
          hasSumRow: true,
          size: [12,12,12],//XS, MD, LG
          options: { //--- All options on this url: https://material-table.com/#/docs/all-props
            actionsColumnIndex: -1,
            exportButton: true,
            exportFileName: 'order items',
            paging: false,
            pageSize: 20,
            pageSizeOptions: [20, 30, 40],
            search: false,
            sorting: false,
            isEditable: true,
            isDeletable: true,
            isCreatable: true,
          },
          columns: [
            { fieldType: 'custom', title: "Items", required: true, useColumnForTotalTitle: true, component: {
              type: 'autocomplete',
              id: 'purchaseitems',
              dataCollection: Collection_ProductItems,
              uniqueKey: 'eid',
              searchKeys: ['eid', 'itemname', 'itembarcode'],//Show specific keys in search
              extraInformation: [
                {
                  key: 'itemquantity',
                  label: 'Qty',
                },
                {
                  key: 'itemprice',
                  label: 'Price',
                },
              ],
              data: [],// also Required if blank
            } },
            {
              fieldType: 'field', title: "Quantity", field: 'purchasequantity', required: true, type:"numeric", emptyValue: 0,
              sumColumn: true,
              DBLogicalRelations: [
                {
                  action: 'add', DBCollection: Collection_ProductItems._name, DBCollectionKey: 'itemquantity', DBCollectionId: 'purchaseitems_id',
                }
              ],
            },
            {
              fieldType: 'field', title: "Purchase Cost", field: 'purchasecost', required: true, type:"numeric", emptyValue: 0,
              sumColumn: true,
              DBLogicalRelations: [
                {
                  action: 'replace', DBCollection: Collection_ProductItems._name, DBCollectionKey: 'itemcost', DBCollectionId: 'purchaseitems_id',
                }
              ],
            },
            {
              fieldType: 'field', title: "Sale Price", field: 'purchaseprice', required: true, type:"numeric", emptyValue: 0,
              sumColumn: true,
              DBLogicalRelations: [
                {
                  action: 'replace', DBCollection: Collection_ProductItems._name, DBCollectionKey: 'itemprice', DBCollectionId: 'purchaseitems_id',
                }
              ],
            },
            {
              fieldType: 'field', title: "Total", field: 'totalcost', type:"numeric", editable: 'never',
              sumColumn: true,
              logic: {entry1: 'purchasequantity', entry2: 'purchasecost', operand: '*'},
              assignSumToComponent: 'totalamount',
            },
          ],
        },
        {
          type: 'textbox',
          id: 'paidamount',
          required: false,
          disabled: true,
          label: "Paid Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'totalamount',
          required: true,
          disabled: true,
          label: "Total Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          accountRelations: [
            {
              credit: { action: 'byDataKey', DBCollectionKey: 'ordersupplier.data.account' },
              debit: { action: 'byAccountName', accountName: 'Inventory' },
              description: "Supply received",
            },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
