import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Suppliers, Collection_ProductItems, Collection_Purchase, Collection_PurchaseOrder, Collection_ReceivePurchaseOrderPurchase } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_Suppliers extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_Suppliers",
      title: "Suppliers",
      extraIds: [
        { prefix: 's', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_Suppliers,
      chechForExistanceBeforeDeleteInCollections: [
        {
          collection: Collection_ProductItems._name,
          entryKey: 'itemdefaultsupplier.id',
        },
        {
          collection: Collection_Purchase._name,
          entryKey: 'ordersupplier.id',
        },
      ],
      createSuccessMessage: "Supplier created successfully",
      createErrorMessage: "Error while creating Supplier",
      updateSuccessMessage: "Supplier updated successfully",
      updateErrorMessage: "Error while updating Supplier",
      deleteSuccessMessage: "Supplier deleted successfully",
      deleteErrorMessage: "Error while deleting Supplier",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid', 'suppliername'],
      },
      accountConfig: {
        nameKey: 'suppliername',
        accountCategoryByName: 'Accounts Payable',
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'categoryname', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'textbox',
          id: 'suppliername',
          required: true,
          disabled: false,
          label: "Name",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'suppliercontact',
          required: false,
          disabled: false,
          label: "Contact",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'supplieremail',
          required: false,
          disabled: false,
          label: "Email",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'supplierphonenumber',
          required: false,
          disabled: false,
          label: "Phone #",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'supplierwebsite',
          required: false,
          disabled: false,
          label: "Website",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'supplieraddress',
          required: false,
          disabled: false,
          label: "Address",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'suppliercity',
          required: false,
          disabled: false,
          label: "City",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'supplierpostalcode',
          required: false,
          disabled: false,
          label: "Postal Code / Zip Code",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'supplierstate',
          required: false,
          disabled: false,
          label: "State / Province",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'autocomplete',
          id: 'suppliercountry',
          required: false,
          disabled: false,
          label: "Country",
          size: [12,3,3],//XS, MD, LG
          searchKeys: ['name'],//Show specific keys in search
          uniqueKey: 'code',
          data: Countries.Countries,// also Required if blank
        },
        {
          type: 'textbox',
          id: 'customercompany',
          required: false,
          disabled: false,
          label: "Company",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'suppliernote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
