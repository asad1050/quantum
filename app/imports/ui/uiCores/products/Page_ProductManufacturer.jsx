import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_ProductItems, Collection_ProductManufacturer } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_ProductManufacturer extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_ProductManufacturer",
      title: "Product Manufacturer",
      extraIds: [
        { prefix: 'pm', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_ProductManufacturer,
      chechForExistanceBeforeDeleteInCollections: [
        {
          collection: Collection_ProductItems._name,
          entryKey: 'itemcategory',
        },
      ],
      createSuccessMessage: "Category created successfully",
      createErrorMessage: "Error while creating Category",
      updateSuccessMessage: "Category updated successfully",
      updateErrorMessage: "Error while updating Category",
      deleteSuccessMessage: "Category deleted successfully",
      deleteErrorMessage: "Error while deleting Category",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid', 'manufacturername'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'manufacturername', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'textbox',
          id: 'manufacturername',
          required: true,
          disabled: false,
          label: "Name",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'manufacturerwebsite',
          required: false,
          disabled: false,
          label: "Website",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'manufacturernote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
      ],
    };

    // formAttr = {
    //   id: "testForms",
    //   title: "Forms Sample",
    //   extraIds: [
    //     { prefix: '', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
    //     { prefix: 'd-', keyName: 'did', postfix: '', logic: {datebox1: new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()).toISOString()} },
    //   ],
    //   searchKey: 'eid',//Key would be used to search for value from collection
    //   inputVarient: "standard",// 'standard', 'filled', 'outlined'
    //   collection: Collection_ProductCategories,
    //   createSuccessMessage: "created Successfully",
    //   createErrorMessage: "unable to create",
    //   updateSuccessMessage: "",
    //   updateErrorMessage: "",
    //   deleteSuccessMessage: "Success",
    //   deleteErrorMessage: "Errr",
    //   instantSearchConfig: {
    //     instantSearchLogic: {},
    //     searchStringDBKeys: ['cid', 'name'],
    //   },
    //   openEditDialogConfig: {
    //     detailedSearch: true,
    //     textChangeSearch: false,
    //     tableKeys: [
    //       { dbName: '_id', label: 'Id'},
    //       { dbName: 'sampletext', label: 'Sample Text'},
    //       { dbName: 'sampletext1', label: 'Sample Text 1'},
    //       { dbName: 'datebox1', label: 'Date', formatDate: true},
    //     ],
    //     detailedSearchBy: [
    //       {
    //         type: 'textbox',
    //         id: 'sampletext',
    //         disabled: false,
    //         label: "Sample Text Box",
    //         size: [12,6,6],//XS, MD, LG
    //         password: false,
    //         readOnly: false,
    //         number: false,
    //       },
    //       {
    //         type: 'datebox',
    //         id: 'datebox1',
    //         format: 'dd/MM/yyyy',
    //         disabled: false,
    //         label: "Sample Date Box 1",
    //         size: [12,6,6],//XS, MD, LG
    //         dateRange: true,
    //       },
    //       {
    //         type: 'checkbox',
    //         id: 'checkbox1',
    //         disabled: false,
    //         label: "Sample Check box 1",
    //         size: [12,6,6],//XS, MD, LG
    //         color: 'primary',
    //       },
    //       {
    //         type: 'selectbox',
    //         id: 'selectbox1',
    //         disabled: false,
    //         label: "Sample Select box 1",
    //         size: [12,6,6],//XS, MD, LG
    //         dataCollection: Collection_ProductCategories,
    //         dataCollectionValue: '_id',
    //         dataCollectionLabel: 'sampletext',
    //         data: [
    //           { value: 'option1', label: 'option1' },
    //           { value: 'option2', label: 'option2' },
    //           { value: 'option3', label: 'option3' },
    //         ],
    //       },
    //     ],
    //   },
    //   components: [
    //     {
    //       type: 'textbox',
    //       id: 'sampletext',
    //       required: true,
    //       disabled: false,
    //       label: "Sample Text Box",
    //       size: [12,6,6],//XS, MD, LG
    //       password: false,
    //       readOnly: false,
    //       number: false,
    //     },
    //     {
    //       type: 'textbox',
    //       id: 'sampletext1',
    //       required: false,
    //       disabled: false,
    //       label: "Sample Text Box 1",
    //       size: [12,6,6],//XS, MD, LG
    //     },
    //     {
    //       type: 'datebox',
    //       id: 'datebox1',
    //       format: 'dd/MM/yyyy',
    //       required: true,
    //       disabled: false,
    //       label: "Sample Date Box 1",
    //       size: [12,6,6],//XS, MD, LG
    //     },
    //     {
    //       type: 'checkbox',
    //       id: 'checkbox1',
    //       required: false,
    //       disabled: false,
    //       label: "Sample Check box 1",
    //       size: [12,6,6],//XS, MD, LG
    //       color: 'primary',
    //     },
    //     {
    //       type: 'radiobox',
    //       id: 'radiobox1',
    //       required: true,
    //       disabled: false,
    //       label: "Sample Radio box 1",
    //       size: [12,6,6],//XS, MD, LG
    //       options: [
    //         { value: 'option1', label: 'option1' },
    //         { value: 'option2', label: 'option2' },
    //         { value: 'option3', label: 'option3' },
    //       ],
    //     },
    //     {
    //       type: 'selectbox',
    //       id: 'selectbox1',
    //       required: false,
    //       disabled: false,
    //       label: "Sample Select box 1",
    //       size: [12,6,6],//XS, MD, LG
    //       dataCollection: Collection_ProductCategories,
    //       dataCollectionValue: '_id',
    //       dataCollectionLabel: 'sampletext',
    //       data: [
    //         { value: 'option1', label: 'option1' },
    //         { value: 'option2', label: 'option2' },
    //         { value: 'option3', label: 'option3' },
    //       ],
    //     },
        // {
        //   type: 'autocomplete',
        //   id: 'autocomplete1',
        //   required: false,
        //   disabled: false,
        //   label: "Sample Auto Complete 1",
        //   size: [12,12,12],//XS, MD, LG
        //   dataCollection: Collection_ProductCategories,
        //   searchKeys: ['eid', 'categoryname'],//Show specific keys in search
        //   data: [
        //     { value: 'option1', label: 'option1' },
        //     { value: 'option2', label: 'option2' },
        //     { value: 'option3', label: 'option3' },
        //   ],// also Required if blank
        // },
    //   ],
    // };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
