import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_ProductItems, Collection_ProductCategories, Collection_Suppliers, Collection_ProductManufacturer } from '../../../api/collections';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_ProductItems extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_ProductItems",
      title: "Product Items",
      extraIds: [
        { prefix: 'i', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_ProductItems,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      uniqueKeys: ['eid', 'itembarcode', 'itemsku'],// These calues should not be same in DB
      formEntryLogics: [//Checked at add and update
        {
          entry1: 'itemcost',
          entry2: 'itemprice',
          operand: '<='
        },
      ],
      formComponentLogics: [//Perform logics while editing (only for numerics)
        {
          entry1: 'itemprice',
          entry2: 'itemcost',
          operand: '%',
          result: 'itemmarkup',
        },
      ],
      createSuccessMessage: "Product Item created successfully",
      createErrorMessage: "Error while creating Product Item",
      updateSuccessMessage: "Product Item updated successfully",
      updateErrorMessage: "Error while updating Product Item",
      deleteSuccessMessage: "Product Item deleted successfully",
      deleteErrorMessage: "Error while deleting Product Item",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid', 'itemname', 'itembarcode'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'categoryname', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      defaultEntries: [
        {
          name: 'itemquantity',
          value: 0,
        },
      ],
      components: [
        {
          type: 'textbox',
          id: 'itemname',
          required: true,
          disabled: false,
          label: "Name",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'selectbox',
          id: 'itemcategory',
          required: true,
          disabled: false,
          label: "Category",
          size: [12,6,6],//XS, MD, LG
          dataCollection: Collection_ProductCategories,
          dataCollectionValue: '_id',
          dataCollectionLabel: 'categoryname',
        },
        {
          type: 'checkbox',
          id: 'itemavailableforsale',
          required: false,
          disabled: false,
          label: "Item available for sale",
          size: [12,6,6],//XS, MD, LG
          color: 'primary',
        },
        {
          type: 'radiobox',
          id: 'itemsoldby',
          required: true,
          disabled: false,
          label: "Sold By",
          size: [12,6,6],//XS, MD, LG
          options: [
            { value: 'each', label: 'each' },
            { value: 'wight', label: 'weight' },
          ],
        },
        {
          type: 'textbox',
          id: 'itemprice',
          required: true,
          disabled: false,
          label: "Price",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'itemcost',
          required: true,
          disabled: false,
          label: "Cost",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'itemmarkup',
          required: true,
          disabled: true,
          label: "Markup %",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'itembarcode',
          required: false,
          disabled: false,
          label: "Barcode",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'itemsku',
          required: false,
          disabled: false,
          label: "SKU",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'itemlowstock',
          required: false,
          disabled: false,
          label: "Item Low Stock, for warning",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'itemoptimalstock',
          required: false,
          disabled: false,
          label: "Item Optimal Stock, to fill in purchase order",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'autocomplete',
          id: 'itemmanufacturer',
          required: false,
          disabled: false,
          label: "Item Manufacturer",
          size: [12,6,6],//XS, MD, LG
          dataCollection: Collection_ProductManufacturer,
          uniqueKey: 'eid',
          searchKeys: ['eid', 'manufacturername'],//Show specific keys in search
          data: [],// also Required if blank
        },
        {
          type: 'autocomplete',
          id: 'itemdefaultsupplier',
          required: false,
          disabled: false,
          label: "Item Default Supplier",
          size: [12,6,6],//XS, MD, LG
          dataCollection: Collection_Suppliers,
          uniqueKey: 'eid',
          searchKeys: ['eid', 'suppliername'],//Show specific keys in search
          data: [],// also Required if blank
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
