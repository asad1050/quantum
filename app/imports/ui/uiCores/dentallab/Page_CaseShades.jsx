import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_CaseShades, Collection_Cases } from '../../../api/collections';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_CaseShades extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_CaseShades",
      title: "Case Shades",
      extraIds: [
        { prefix: 'cs', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_CaseShades,
      chechForExistanceBeforeDeleteInCollections: [
        {
          collection: Collection_Cases._name,
          entryKey: 'caseshade',
        },
      ],
      createSuccessMessage: "Case Shades created successfully",
      createErrorMessage: "Error while creating Case Shades",
      updateSuccessMessage: "Case Shades updated successfully",
      updateErrorMessage: "Error while updating Case Shades",
      deleteSuccessMessage: "Case Shades deleted successfully",
      deleteErrorMessage: "Error while deleting Case Shades",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid', 'name'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
        ],
        detailedSearchBy: [],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'textbox',
          id: 'name',
          required: true,
          disabled: false,
          label: "Name",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
