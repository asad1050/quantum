import React, { Component, forwardRef } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import swal from 'sweetalert2';
import { Collection_Cases, Collection_CaseShades, Collection_CaseTypes, Collection_CaseUnits, Collection_Customers, Collection_WorkTypes } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms';
import DentalLabCaseUnitSelectorStyle from '../../../modules/modules/customComponents/DentalLabCaseUnitSelector';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

import { formatDate } from '../../../models/supporters.js';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
  .autocomplete-style{
    margin-top: 5px;
  }
`

export default class Page_Cases extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_Cases",
      title: "Cases",
      extraIds: [
        { prefix: 'c', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_Cases,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      uniqueKeys: ['eid'],// These calues should not be same in DB
      createSuccessMessage: "Case created successfully",
      createErrorMessage: "Error while creating Case",
      updateSuccessMessage: "Case updated successfully",
      updateErrorMessage: "Error while updating Case",
      deleteSuccessMessage: "Case deleted successfully",
      deleteErrorMessage: "Error while deleting Case",
      printReceipt: (form, data) => {
        if (window.debugMode) console.log("Form mode: ", form.state.mode);
        if (sessionStorage.getItem("settings") !== 'undefined') {
          var settings = JSON.parse(sessionStorage.getItem("settings"));
        } else {
          var settings = {};
          settings.receipttitle1 = 'Quantum';
          settings.receipttitle2 = '';
          settings.receiptfootertext = '';
        }
        
        if (window.debugMode) console.log('Form Mode: ', form.state.mode);
        if (form.state.mode == 'update') {
          data = form.state.selectedEntry;
        }
        if (window.debugMode) console.log("print Input reponse", data);
        var caseshade = '';
        if (data.caseshade && data.caseshade.data) {
          caseshade = data.caseshade.data.name;
        }
        var unitChart = '';
        var units = data.caseunits;
        for (var i=0;i<(units.length - 1);i++) {
          var unit = units[i];
          if (i !== 0) unitChart += ', ';
          unitChart += unit.unit;
        }
        var output = `
        <html><body style="font-family: Calibri;">
        <div style="width: 100%;text-align: center;" >
        <div><span style="font-size: 22px;font-weight: bold;" >` + settings.receipttitle1 +  `</span><br>
        <span style="text-align: right;" >` + settings.receipttitle2 +  `</span> </div>
        </div>
        <div style="width: 100%;padding-top: 8px;" >
        <div style="float:left;" >CASE # ` + data.eid + `</div>
        <div style="float:right;" >Date: ` + formatDate(data.casedate) + `</div>
        </div>
        <br><br>
        <div>
        <b><span style="font-size: 18px;" >Client: ` + data.customer.data.customername + `</span> <br>
        Shade:  ` + caseshade + ` <br>
        Work Type: ` + data.worktype.data.worktypename + ` <br>
        Units: ` + (data.caseunits.length - 1) + `</b> <br>
        Charting: ` + unitChart + ` <br>
        Trial Date: ` + formatDate(data.trialdate) + ` <br>
        Delivery Date: ` + formatDate(data.deliverydate) + ` <br>
        </div>
        <div style="width: 100%;text-align: center;" > ` + settings.receiptfootertext + ` </div>
        </body></html>
        `;
        return output;
      },
      instantSearchConfig: {
        searchStringDBKeys: ['eid', 'casestatus'],
      },
      openEditDialogConfig: {
        detailedSearch: true,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'casedate', label: 'Case Date', formatDate: true},
        ],
        detailedSearchBy: [
          {
            type: 'datebox',
            id: 'casedate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Purchased On",
            size: [12,12,12],//XS, MD, LG
            dateRange: true,
          },
          {
            type: 'textbox',
            id: 'eid',
            required: false,
            disabled: false,
            label: "Case Id",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
        ],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'datebox',
          id: 'casedate',
          format: 'dd/MM/yyyy',
          required: false,
          disabled: false,
          label: "Case Date",
          size: [12,4,4],//XS, MD, LG
        },
        {
          type: 'datebox',
          id: 'trialdate',
          format: 'dd/MM/yyyy',
          required: false,
          disabled: false,
          label: "Trial Date",
          size: [12,4,4],//XS, MD, LG
          defaultValue: null, 
        },
        {
          type: 'datebox',
          id: 'deliverydate',
          format: 'dd/MM/yyyy',
          required: false,
          disabled: false,
          label: "Delivery Date",
          size: [12,4,4],//XS, MD, LG
          defaultValue: null, 
          },
        {
            type: 'autocomplete',
            id: 'casetype',
            required: false,
            disabled: false,
            label: "Case Type",
            size: [12,6,6],//XS, MD, LG
            dataCollection: Collection_CaseTypes,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'name'],//Show specific keys in search
            data: [],// also Required if blank
        },
        {
            type: 'autocomplete',
            id: 'caseshade',
            required: false,
            disabled: false,
            label: "Case Shade",
            size: [12,6,6],//XS, MD, LG
            dataCollection: Collection_CaseShades,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'name'],//Show specific keys in search
            data: [],// also Required if blank
        },
        {
            type: 'selectbox',
            id: 'casestatus',
            disabled: false,
            label: "Status",
            size: [12,2,2],//XS, MD, LG
            data: [
              { value: 'pending', label: 'pending' },
              { value: 'processing', label: 'processing' },
              { value: 'ready', label: 'ready' },
              { value: 'delivered', label: 'delivered' },
            ],
        },
        {
            type: 'autocomplete',
            id: 'customer',
            required: true,
            disabled: false,
            label: "Customer",
            size: [12,5,5],//XS, MD, LG
            dataCollection: Collection_Customers,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'customername'],//Show specific keys in search
            data: [],// also Required if blank
        },
        {
            type: 'autocomplete',
            id: 'worktype',
            required: true,
            disabled: false,
            label: "Work Type",
            size: [12,5,5],//XS, MD, LG
            dataCollection: Collection_WorkTypes,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'worktypename'],//Show specific keys in search
            data: [],// also Required if blank
            extraInformation: [
              {
              key: 'worktypeprice',
              label: 'Price',
              },
            ],
            onSelectCallback: (data, form) => {
              form.setState({ 'caseunits_storeable': [] });
              form.setState({ 'caseunits': [] });
            },
        },
        {
            type: 'custom',
            id: 'caseunits',
            required: true,
            disabled: false,
            label: "Case Units",
            collection: Collection_CaseUnits,
            size: [12,12,12],//XS, MD, LG
            component: (props, form) => {
              return (<DentalLabCaseUnitSelectorStyle {...props} form={form}  />);
            },
            onOk: (data, form) => {
              if (form.state['worktype'] && Object.keys(form.state['worktype']).length > 0){
                let unitData = [];
                for (var i=0;i<data.length;i++){
                  unitData.push({
                    unit: data[i],
                    price: form.state['worktype'].worktypeprice,
                  });
                } 
                

                form.setState({ 'caseunits_storeable': unitData });
                var totalamount = 0;
                for (var i=0;i<unitData.length;i++){
                  totalamount += parseFloat(unitData[i].price);
                }
                unitData.push({
                  unit: 'Total',
                  price: totalamount,
                });
                form.setState({ 'caseunits': unitData });
                form.setState({ 'totalamount': totalamount });
              }else{
                swal.fire({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Please select worktype first' ,
                });
              }
          },
        },
        {
          type: 'multiselecttable',
          id: 'caseunits',
          label: "Units",
          required: true,
          size: [12,12,12],//XS, MD, LG
          options: { //--- All options on this url: https://material-table.com/#/docs/all-props
            actionsColumnIndex: -1,
            maxBodyHeight: '200px',
            exportButton: false,
            exportFileName: 'case units',
            paging: false,
            pageSize: 5,
            pageSizeOptions: [20, 30, 40],
            search: false,
            sorting: false,
            isEditable: false,
            isDeletable: false,
            isCreatable: false,
          },
          columns: [
            {
              fieldType: 'field', title: "Units", field: 'unit', required: true,
              sumColumn: true,
              DBLogicalRelations: [
              ],
              useColumnForTotalTitle: true,
            },
            {
              fieldType: 'field', title: "price", field: 'price', type:"numeric", editable: 'never',
              sumColumn: true,
              assignSumToComponent: 'totalamount',
            },
          ],
        },
        {
          type: 'blank',
          size: [12,6,6],//XS, MD, LG
        },
        {
          type: 'textbox',
          id: 'totalamount',
          required: true,
          disabled: true,
          label: "Total Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          accountRelations: [
            {
              debit: { action: 'byDataKey', DBCollectionKey: 'customer.data.account' },
              credit: { action: 'byAccountName', accountName: 'Sales' },
              description: "Sale Completed",
              includeInTransactions: 'customer.data',
            },
          ],
        },
        {
            type: 'textbox',
            id: 'description',
            required: false,
            disabled: false,
            label: "Description",
            size: [12,12,12],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
