import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_GeneralSettings } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import EntriesUpdater from '../../../modules/modules/entriesUpdater';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_GeneralSettings extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let entriesUpdaterAttr = {
      id: "Form_GeneralSettings",
      title: "General Settings",
      idKey: 'appid',
      idValue: sessionStorage.getItem("appid"),
      collection: Collection_GeneralSettings,
      collectionLogic: { appid: sessionStorage.getItem("appid") },
      updateSuccessMessage: "Settings updated successfully",
      updateErrorMessage: "Error while updating settings",
      components: [
        {
            type: 'textbox',
            id: 'businessname',
            required: true,
            disabled: false,
            label: "Business Name",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
        {
          type: 'textbox',
          id: 'address',
          required: true,
          disabled: false,
          label: "Address",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'phonenumber',
          required: true,
          disabled: false,
          label: "Phone #",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'website',
          required: false,
          disabled: false,
          label: "Website",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'receipttitle1',
          required: false,
          disabled: false,
          label: "Receipt Title 2",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'receipttitle2',
          required: false,
          disabled: false,
          label: "Receipt Title 1",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'receiptfootertext',
          required: false,
          disabled: false,
          label: "Receipt Footer Text",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
      ],
    };

    return (
      <div>
        <EntriesUpdater entriesUpdaterAttr={entriesUpdaterAttr} />
      </div>
    );
  }
}
