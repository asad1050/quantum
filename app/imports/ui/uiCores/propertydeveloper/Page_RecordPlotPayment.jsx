import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import collectionPointer from '../../../api/collectionPointer';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_RecordPlotPayment extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_RecordPlotPayment",
      title: "Record Payment",
      extraIds: [
        { prefix: 'rp', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: collectionPointer('recordplotpayment'),
      chechForExistanceBeforeDeleteInCollections: [

      ],
      changeOtherCollectionDataOnCreation: [
        {
          type: 'directValue',
          collection: collectionPointer('propertysales')._name,
          entryKey: 'editable',
          entryValue: false,
          DBCollectionId: 'propertysale.id',
        },
        {
          type: 'directValue',
          collection: collectionPointer('propertysales')._name,
          entryKey: 'deletable',
          entryValue: false,
          DBCollectionId: 'propertysale.id',
        },
      ],
      formEntryLogics: [//Checked at add and update
        {
          entry1: 'paymentamount',
          entry2: 'pendingamount',
          operand: '<='
        },
      ],
      formComponentLogics: [//Perform logics while editing (only for numerics)
        {
          entry1: 'totalamount',
          entry2: 'paidamount',
          operand: '-',
          result: 'pendingamount',
        },
      ],
      restrictUpdate: true,
      createSuccessMessage: "Payment added successfully",
      createErrorMessage: "Error while adding payment",
      updateSuccessMessage: "",
      updateErrorMessage: "",
      deleteSuccessMessage: "Payment deleted successfully",
      deleteErrorMessage: "Error while deleting payment",
      instantSearchConfig: {
        instantSearchLogic: {'orderstatus': 'pending'},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: true,
        textChangeSearch: true,
        detailedSearchLogic: {'orderstatus': 'pending'},
        tableKeys: [],
        detailedSearchBy: [
          {
            type: 'datebox',
            id: 'paymentreceivedate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date Purchased On",
            size: [12,6,6],//XS, MD, LG
            dateRange: false,
          },
          {
            type: 'autocomplete',
            id: 'customer',
            required: false,
            disabled: false,
            label: "Customer",
            size: [12,12,12],//XS, MD, LG
            dataCollection: collectionPointer('customers'),
            uniqueKey: 'eid',
            searchKeys: ['eid', 'customername'],//Show specific keys in search
            data: [],// also Required if blank
          },
        ],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'autocomplete',
          id: 'propertysale',
          required: true,
          disabled: false,
          label: "Select Sold Plot",
          size: [12,12,12],//XS, MD, LG
          dataCollection: collectionPointer('propertysales'),
          uniqueKey: 'eid',
          searchKeys: ['eid', 'customer.value', 'plot.data.plotnumber', 'plot.data.blocknumber'],//Show specific keys in search
          data: [],// also Required if blank
          onSelectCallback: function (data, form) {
            form.setState({'totalamount': data.totalamount});
            form.setState({'paidamount': data.paidamount});
            setTimeout(() => {
              $('#paidamount').keyup(); 
            });
          },
        },
        {
          type: 'textbox',
          id: 'totalamount',
          required: false,
          disabled: true,
          label: "Total Amount",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'paidamount',
          required: false,
          disabled: true,
          label: "Paid Amount",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'pendingamount',
          required: false,
          disabled: true,
          label: "Pending Amount",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
          type: 'textbox',
          id: 'paymentamount',
          required: true,
          disabled: false,
          label: "Payment Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          DBLogicalRelations: [
            {
              action: 'add', DBCollection: collectionPointer('propertysales')._name, DBCollectionKey: 'paidamount', DBCollectionId: 'propertysale.id',
            }
          ],
          accountRelations: [
            {
              debit: { action: 'byAccountName', DBCollectionKey: 'Cash In Hand' },
              credit: { action: 'byDataKey', DBCollectionKey: 'propertysale.data.account' },
              description: "Installment recorded",
            },
          ],
        },
        {
            type: 'selectbox',
            id: 'paymenttype',
            disabled: false,
            label: "Payment Type",
            size: [12,6,6],//XS, MD, LG
            data: [
              { value: 'cash', label: 'Cash' },
              { value: 'cheque', label: 'Cheque' },
              { value: 'banktransfer', label: 'Bank Transfer' },
            ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
