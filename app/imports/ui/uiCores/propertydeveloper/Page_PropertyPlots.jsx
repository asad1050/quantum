import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import collectionPointer from '../../../api/collectionPointer';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_PropertyPlots extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_PropertyPlots",
      title: "Plots",
      extraIds: [
        { prefix: 'plt', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: collectionPointer('propertyplots'),
      chechForExistanceBeforeDeleteInCollections: [

      ],
      uniqueKeys: ['eid', 'plotnumber'],// These calues should not be same in DB
      formEntryLogics: [//Checked at add and update
      ],
      formComponentLogics: [//Perform logics while editing (only for numerics)
        {
          entry1: 'plotmarlas',
          entry2: 'ratepermarla',
          operand: '*',
          result: 'totalprice',
        },
      ],
      createSuccessMessage: "Plots created successfully",
      createErrorMessage: "Error while creating Plots",
      updateSuccessMessage: "Plots updated successfully",
      updateErrorMessage: "Error while updating Plots",
      deleteSuccessMessage: "Plots deleted successfully",
      deleteErrorMessage: "Error while deleting Plots",
      instantSearchConfig: {
        instantSearchLogic: { 'sold': false },
        searchStringDBKeys: ['eid', 'plotnumber'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'categoryname', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      defaultEntries: [
        {
          name: 'sold',
          value: false,
        },
        {
            name: 'soldto',
            value: false,
        },
      ],
      components: [
        {
          type: 'textbox',
          id: 'plotnumber',
          required: true,
          disabled: false,
          label: "Plot #",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
            type: 'textbox',
            id: 'blocknumber',
            required: true,
            disabled: false,
            label: "Block #",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'dimentions',
            required: true,
            disabled: false,
            label: "Dimentions",
            size: [12,12,12],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
        {
            type: 'textbox',
            id: 'plotmarlas',
            required: true,
            disabled: false,
            label: "Plot Marlas",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
        {
            type: 'textbox',
            id: 'ratepermarla',
            required: true,
            disabled: false,
            label: "Rate Per Marla",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
        {
            type: 'textbox',
            id: 'totalprice',
            required: true,
            disabled: true,
            label: "Rate Per Marla",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
