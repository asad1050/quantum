import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import collectionPointer from '../../../api/collectionPointer';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_PropertySale extends Component {
  render() {
    

    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_PropertySale",
      title: "Booking/Sale",
      extraIds: [
        { prefix: 'ps', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: collectionPointer('propertysales'),
      chechForExistanceBeforeDeleteInCollections: [

      ],
      printReceipt: {
        
      },
      createSuccessMessage: "Sale created successfully",
      createErrorMessage: "Error while creating Sale",
      updateSuccessMessage: "Sale updated successfully",
      updateErrorMessage: "Error while updating Sale",
      deleteSuccessMessage: "Sale deleted successfully",
      deleteErrorMessage: "Error while deleting Sale",
      formEntryLogics: [//Checked at add and update
      ],
      formComponentLogics: [//Perform logics while editing (only for numerics)
        {
          entry1: 'grossamount',
          entry2: 'discountpercent',
          operand: '%*',
          result: 'discount',
        },
        {
            entry1: 'grossamount',
            entry2: 'discount',
            operand: '100-%',
            result: 'discountpercent',
          },
        {
            entry1: 'grossamount',
            entry2: 'discount',
            operand: '-',
            result: 'totalamount',
        },
      ],
      changeOtherCollectionDataOnCreation: [
        {
          type: 'directValue',
          collection: 'propertyplots',
          entryKey: 'sold',
          entryValue: true,
          DBCollectionId: 'plot.id',
        },
        {
            type: 'fromFormData',
            collection: 'propertyplots',
            entryKey: 'soldto',
            entryValueKey: 'customer.id',
            DBCollectionId: 'plot.id',
        },
      ],
      changeOtherCollectionDataOnDelete: [
        {
          type: 'directValue',
          collection: 'propertyplots',
          entryKey: 'sold',
          entryValue: false,
          DBCollectionId: 'plot.id',
        },
        {
            type: 'directValue',
            collection: 'propertyplots',
            entryKey: 'soldto',
            entryValue: false,
            DBCollectionId: 'plot.id',
        },
      ],
      instantSearchConfig: {
        instantSearchLogic: { 'plotreturned': false },
        searchStringDBKeys: ['eid', 'customer.data.customername'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [],
        detailedSearchBy: [],
      },
      defaultEntries: [
        {
            name: 'employeename',
            value: sessionStorage.getItem("userfullname"),
        },
        {
            name: 'paidamount',
            value: 0,
        },
        {
            name: 'plotreturned',
            value: false,
        },
      ],
      accountConfig: {
        nameKey: ['customer.value', 'plot.value', 'plot.data.blocknumber'],
        accountCategoryByName: 'Accounts Receivable',
      },
      components: [
        {
            type: 'blank',
            size: [12,8,8],//XS, MD, LG
        },
        {
          type: 'datebox',
          id: 'saledate',
          format: 'dd/MM/yyyy',
          required: true,
          disabled: false,
          label: "Sale Date",
          size: [12,4,4],//XS, MD, LG
        },
        {
            type: 'autocomplete',
            id: 'customer',
            required: false,
            disabled: false,
            label: "Customer",
            size: [12,6,6],//XS, MD, LG
            dataCollection: collectionPointer('customers'),
            uniqueKey: 'eid',
            searchKeys: ['eid', 'customername', 'customeraddress'],//Show specific keys in search
            data: [],// also Required if blank
        },
        {
            type: 'autocomplete',
            id: 'plot',
            required: false,
            disabled: false,
            label: "Plot",
            size: [12,6,6],//XS, MD, LG
            dataCollection: collectionPointer('propertyplots'),
            uniqueKey: 'eid',
            searchKeys: ['eid', 'plotnumber'],//Show specific keys in search
            dataLogic: { sold: false },
            onSelectCallback: (item, form) => {
                console.log(item);
                form.setState({'grossamount': item.totalprice, 'totalamount': item.totalprice, 'discount': 0, 'discountpercent': 0});
            },
            data: [],// also Required if blank
        },
        {
          type: 'blank',
          size: [12,6,6],//XS, MD, LG
        },
        {
          type: 'textbox',
          id: 'grossamount',
          required: true,
          disabled: true,
          label: "Gross Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
        },
        {
            type: 'textbox',
            id: 'discount',
            required: true,
            disabled: false,
            label: "Discount",
            size: [12,3,3],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
        {
            type: 'textbox',
            id: 'discountpercent',
            required: true,
            disabled: false,
            label: "Discount %",
            size: [12,3,3],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
        {
            type: 'textbox',
            id: 'totalamount',
            required: true,
            disabled: true,
            label: "Total Amount",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
            accountRelations: [
              {
                debit: { action: 'byDataKey', DBCollectionKey: 'account' },
                credit: { action: 'byAccountName', accountName: 'Sales' },
                description: "Plot Sold",
              },
            ],
        },
        {
            type: 'selectbox',
            id: 'paymentterm',
            disabled: false,
            label: "Payment Term",
            size: [12,12,12],//XS, MD, LG
            data: [
              { value: 'tokenmoney', label: 'Token Money' },
              { value: 'downpayment', label: 'Down Payment' },
              { value: 'installment', label: 'Installment' },
            ],
        },
        {
          type: 'multiselecttable',
          id: 'paymentschedule',
          label: "Payment Schedule",
          size: [12,12,12],//XS, MD, LG
          options: { //--- All options on this url: https://material-table.com/#/docs/all-props
            actionsColumnIndex: -1,
            exportButton: true,
            exportFileName: 'payment schedule',
            paging: false,
            pageSize: 40,
            pageSizeOptions: [20, 30, 40],
            search: false,
            sorting: false,
            isEditable: true,
            isDeletable: true,
            isCreatable: true,
          },
          columns: [
            { fieldType: 'custom', title: "Payment Date", required: true, component: {
                type: 'datebox',
                id: 'paymentschedule_date',
                format: 'dd/MM/yyyy',
                required: true,
                disabled: false,
                label: "Payment Date",
              } },
            { fieldType: 'field', title: "Payment", field: 'paymentschedule_payment', required: true, type:"numeric", emptyValue: 0 },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
