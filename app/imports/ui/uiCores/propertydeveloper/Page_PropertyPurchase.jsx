import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import collectionPointer from '../../../api/collectionPointer';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_PropertyPurchase extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_PropertyPurchase",
      title: "Property Purchase",
      extraIds: [
        { prefix: 'prp', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: collectionPointer('propertypurchase'),
      restrictUpdate: true,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      createSuccessMessage: "Prucahse created successfully",
      createErrorMessage: "Error while creating Prucahse",
      updateSuccessMessage: "Prucahse updated successfully",
      updateErrorMessage: "Error while updating Prucahse",
      deleteSuccessMessage: "Prucahse deleted successfully",
      deleteErrorMessage: "Error while deleting Prucahse",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [
        ],
        detailedSearchBy: [],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'autocomplete',
          id: 'supplier',
          required: true,
          disabled: false,
          label: "Supplier",
          size: [12,12,12],//XS, MD, LG
          dataCollection: collectionPointer('suppliers'),
          uniqueKey: 'eid',
          searchKeys: ['eid', 'suppliername', 'supplieraddress'],//Show specific keys in search
          data: [],// also Required if blank
        },
        {
          type: 'datebox',
          id: 'purchasedate',
          format: 'dd/MM/yyyy',
          required: true,
          disabled: false,
          label: "Purchase Date",
          size: [12,6,6],//XS, MD, LG
        },
        {
          type: 'textbox',
          id: 'ordernote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'totalamount',
          required: true,
          disabled: false,
          label: "Total Amount",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          accountRelations: [
            {
              credit: { action: 'byAccountName', accountName: 'Cash In Hand' },
              debit: { action: 'byAccountName', accountName: 'General Expenses' },
              description: "Property Purchased",
            },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
