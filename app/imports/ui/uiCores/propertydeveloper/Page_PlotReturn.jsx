import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import collectionPointer from '../../../api/collectionPointer';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_PlotReturn extends Component {
  render() {
    

    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_PlotReturn",
      title: "Plot Return",
      extraIds: [
        { prefix: 'pltr', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      restrictUpdate: true,
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: collectionPointer('plotreturn'),
      chechForExistanceBeforeDeleteInCollections: [
      ],
      createSuccessMessage: "Return created successfully",
      createErrorMessage: "Error while creating Return",
      updateSuccessMessage: "Return updated successfully",
      updateErrorMessage: "Error while updating Return",
      deleteSuccessMessage: "Return deleted successfully",
      deleteErrorMessage: "Error while deleting Return",
      formEntryLogics: [//Checked at add and update
      ],
      formComponentLogics: [//Perform logics while editing (only for numerics)
        ],
      changeOtherCollectionDataOnCreation: [
        {
          type: 'directValue',
          collection: 'propertyplots',
          entryKey: 'sold',
          entryValue: false,
          DBCollectionId: 'plotsale.data.plot.id',
        },
        {
            type: 'directValue',
            collection: 'propertyplots',
            entryKey: 'soldto',
            entryValue: false,
            DBCollectionId: 'plotsale.data.plot.id',
          },
          {
            type: 'directValue',
            collection: 'propertysales',
            entryKey: 'plotreturned',
            entryValue: true,
            DBCollectionId: 'plotsale.id',
          },
      ],
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid', 'customer.data.customername'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [],
        detailedSearchBy: [],
      },
      defaultEntries: [
        {
            name: 'employeename',
            value: sessionStorage.getItem("userfullname"),
        },
      ],
      components: [
        {
            type: 'blank',
            size: [12,8,8],//XS, MD, LG
        },
        {
          type: 'datebox',
          id: 'returndate',
          format: 'dd/MM/yyyy',
          required: true,
          disabled: false,
          label: "Return Date",
          size: [12,4,4],//XS, MD, LG
        },
        {
            type: 'autocomplete',
            id: 'plotsale',
            required: false,
            disabled: false,
            label: "Choose Plot Sale",
            size: [12,6,6],//XS, MD, LG
            dataCollection: collectionPointer('propertysales'),
            uniqueKey: 'eid',
            searchKeys: ['eid', 'customer.value', 'plot.value'],//Show specific keys in search
            dataLogic: { plotreturned: false },
            onSelectCallback: (item, form) => {
                console.log(item);
                form.setState({'paidamount': item.paidamount, 'totalamount': item.totalamount, 'discount': item.discount, 'discountpercent': item.discountpercent});
            },
            data: [],// also Required if blank
        },
        {
          type: 'textbox',
          id: 'paidamount',
          required: true,
          disabled: true,
          label: "Paid Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          accountRelations: [
                {
                credit: { action: 'byAccountName', accountName: 'Cash In Hand' },
                debit: { action: 'byDataKey', DBCollectionKey: 'plotsale.data.account' },
                description: "Intallments Returned",
                },
            ],
        },
        {
            type: 'textbox',
            id: 'totalamount',
            required: true,
            disabled: true,
            label: "Total Amount",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
            accountRelations: [
                {
                debit: { action: 'byAccountName', accountName: 'Sales' },
                credit: { action: 'byDataKey', DBCollectionKey: 'plotsale.data.account' },
                description: "Plot Returned",
                },
            ],
          },
        {
            type: 'textbox',
            id: 'discount',
            required: true,
            disabled: true,
            label: "Discount",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
        {
            type: 'textbox',
            id: 'discountpercent',
            required: true,
            disabled: true,
            label: "Discount %",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: true,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
