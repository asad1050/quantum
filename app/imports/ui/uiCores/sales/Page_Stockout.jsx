import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Purchase, Collection_ProductItems, Collection_Stockout } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_Stockout extends Component {
  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_Stockout",
      title: "Stock Out",
      extraIds: [
        { prefix: 's', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_Stockout,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      createSuccessMessage: "Stockout created successfully",
      createErrorMessage: "Error while creating Stockout",
      updateSuccessMessage: "Stockout updated successfully",
      updateErrorMessage: "Error while updating Stockout",
      deleteSuccessMessage: "Stockout deleted successfully",
      deleteErrorMessage: "Error while deleting Stockout",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: true,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'saledate', label: 'Purchased On', formatDate: true},
          { dbName: 'saleitems', label: 'Sale Items', isTable: true},
        ],
        detailedSearchBy: [
          {
            type: 'datebox',
            id: 'saledate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Stockout Date",
            size: [12,12,12],//XS, MD, LG
            dateRange: true,
          },
          {
            type: 'textbox',
            id: 'eid',
            required: false,
            disabled: false,
            label: "Sale Id",
            size: [12,12,12],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
        ],
      },
      defaultEntries: [
        {
          name: 'employeename',
          value: sessionStorage.getItem("userfullname"),
        },
      ],
      components: [
        {
            type: 'blank',
            size: [12,8,8],//XS, MD, LG
        },
        {
          type: 'datebox',
          id: 'saledate',
          format: 'dd/MM/yyyy',
          required: true,
          disabled: false,
          label: "Purchase Order Date",
          size: [12,4,4],//XS, MD, LG
        },
        {
          type: 'multiselecttable',
          id: 'saleitems',
          label: "Items",
          required: true,
          size: [12,12,12],//XS, MD, LG
          options: { //--- All options on this url: https://material-table.com/#/docs/all-props
            actionsColumnIndex: -1,
            exportButton: false,
            exportFileName: 'order items',
            paging: false,
            pageSize: 20,
            pageSizeOptions: [20, 30, 40],
            search: false,
            sorting: false,
            isEditable: true,
            isDeletable: true,
            isCreatable: true,
          },
          columns: [
            { 
                fieldType: 'custom', title: "Items", required: true, useColumnForTotalTitle: true, component: 
                {
                    type: 'autocomplete',
                    id: 'saleitem',
                    dataCollection: Collection_ProductItems,
                    dataLogic: { itemavailableforsale: true },
                    uniqueKey: 'eid',
                    searchKeys: ['eid', 'itemname', 'itembarcode'],//Show specific keys in search
                    extraInformation: [
                        {
                        key: 'itemquantity',
                        label: 'Qty',
                        },
                        {
                        key: 'itemprice',
                        label: 'Price',
                        },
                    ],
                    data: [],// also Required if blank
                    mapTableColumns: [
                        {
                            columnName: 'saleprice',
                            dataKey: 'itemprice',
                        },
                    ],
                },
                accountRelations: [
                    {
                      debit: { action: 'byAccountName', dataKey: 'saleitem.data.itemcost', accountName: 'Cost Of Goods' },
                      credit: { action: 'byAccountName', dataKey: 'saleitem.data.itemcost', accountName: 'Inventory' },
                      description: "Sale Completed",
                    },
                ], 
            },
            {
              fieldType: 'field', title: "Quantity", field: 'salequantity', required: true, type:"numeric", emptyValue: 0,
              sumColumn: true,
              DBLogicalRelations: [
                {
                  action: 'subtract', DBCollection: Collection_ProductItems._name, DBCollectionKey: 'itemquantity', DBCollectionId: 'saleitem_id',
                }
              ],
            },
            {
              fieldType: 'field', title: "Price", field: 'saleprice', type:"numeric", emptyValue: 0, editable: 'never',
              sumColumn: true,
            },
            {
              fieldType: 'field', title: "Total", field: 'totalprice', type:"numeric", editable: 'never',
              sumColumn: true,
              logic: {entry1: 'salequantity', entry2: 'saleprice', operand: '*'},
              assignSumToComponent: 'totalamount',
            },
          ],
        },
        {
          type: 'blank',
          size: [12,6,6],//XS, MD, LG
        },
        {
          type: 'textbox',
          id: 'totalamount',
          required: true,
          disabled: true,
          label: "Total Amount",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          accountRelations: [
            {
              debit: { action: 'byAccountName', accountName: 'Cash In Hand' },
              credit: { action: 'byAccountName', accountName: 'Sales' },
              description: "Sale Completed",
            },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
