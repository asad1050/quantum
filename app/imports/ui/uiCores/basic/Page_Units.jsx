import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import collectionPointer from '../../../api/collectionPointer';

import Forms from '../../../modules/modules/Forms.js';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_Units extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_Units",
      title: "Units",
      extraIds: [
        { prefix: 'unit_', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: collectionPointer('units'),
      chechForExistanceBeforeDeleteInCollections: [
        {
            collection: collectionPointer('productcategories'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('productmanufacturer'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('productitems'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('customers'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('cities'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('purchase'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('purchasepayment'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('purchaseorder'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('receviepurchaseorder'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('suppliers'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('stockout'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('generalsales'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('roles'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('accountgroups'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('accounts'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('accountcategories'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('transactions'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('accountreception'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('accountpayment'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('worktypes'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('casetypes'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('caseshades'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('cases'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('propertyplots'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('propertysales'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('recordplotpayment'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('propertypurchase'),
            entryKey: 'appunit',
        },
        {
            collection: collectionPointer('plotreturn'),
            entryKey: 'appunit',
        },
      ],
      createSuccessMessage: "Unit created successfully",
      createErrorMessage: "Error while creating Unit",
      updateSuccessMessage: "Unit updated successfully",
      updateErrorMessage: "Error while updating Unit",
      deleteSuccessMessage: "Unit deleted successfully",
      deleteErrorMessage: "Error while deleting Unit",
      instantSearchConfig: {
        instantSearchLogic: {},
        unitIndependent: true,
        searchStringDBKeys: ['eid', 'name'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'name', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      accountConfig: {},
      defaultEntries: [
      ],
      components: [
        {
          type: 'textbox',
          id: 'name',
          required: true,
          disabled: false,
          label: "Name",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'description',
          required: false,
          disabled: false,
          label: "Description",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
