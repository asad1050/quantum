import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Customers, Collection_Cities } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_Customers extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_Customers",
      title: "Customers",
      extraIds: [
        { prefix: 'c', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_Customers,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      createSuccessMessage: "Customer created successfully",
      createErrorMessage: "Error while creating Customer",
      updateSuccessMessage: "Customer updated successfully",
      updateErrorMessage: "Error while updating Customer",
      deleteSuccessMessage: "Customer deleted successfully",
      deleteErrorMessage: "Error while deleting Customer",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid', 'customername'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'customername', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      accountConfig: {
        nameKey: 'customername',
        accountCategoryByName: 'Accounts Receivable',
      },
      components: [
        {
          type: 'textbox',
          id: 'customername',
          required: true,
          disabled: false,
          label: "Name",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
            type: 'textbox',
            id: 'customerguardian',
            required: false,
            disabled: false,
            label: "Guardian (S/O, D/O, W/O)",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
        {
            type: 'datebox',
            id: 'customerdateofbirth',
            format: 'dd/MM/yyyy',
            required: false,
            disabled: false,
            label: "Date of Birth",
            size: [12,4,4],//XS, MD, LG
        },
        {
          type: 'textbox',
          id: 'customeremail',
          required: false,
          disabled: false,
          label: "Email",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'customerphonenumber',
          required: false,
          disabled: false,
          label: "Phone #",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'customerwebsite',
          required: false,
          disabled: false,
          label: "Website",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'customeraddress',
          required: false,
          disabled: false,
          label: "Address",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'autocomplete',
          id: 'customercity',
          required: false,
          disabled: false,
          label: "City",
          size: [12,4,4],//XS, MD, LG
          searchKeys: ['name'],//Show specific keys in search
          uniqueKey: 'eid',
          dataCollection: Collection_Cities,
          data: [],// also Required if blank
        },
        {
            type: 'textbox',
            id: 'customerstate',
            required: false,
            disabled: false,
            label: "State / Province",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
        {
          type: 'textbox',
          id: 'customerpostalcode',
          required: false,
          disabled: false,
          label: "Postal Code / Zip Code",
          size: [12,4,4],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'autocomplete',
          id: 'customercountry',
          required: false,
          disabled: false,
          label: "Country",
          size: [12,4,4],//XS, MD, LG
          searchKeys: ['name'],//Show specific keys in search
          uniqueKey: 'code',
          data: Countries.Countries,// also Required if blank
        },
        {
          type: 'selectbox',
          id: 'billoption',
          disabled: false,
          label: "Bill Option",
          size: [12,4,4],//XS, MD, LG
          data: [
            { value: 'monthly', label: 'Monthly' },
            { value: 'fortnightly', label: 'Fortnightly' },
          ],
        },
        {
          type: 'textbox',
          id: 'customercompany',
          required: false,
          disabled: false,
          label: "Company",
          size: [12,3,3],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
            type: 'textbox',
            id: 'customeroccupation',
            required: false,
            disabled: false,
            label: "Occupation",
            size: [12,3,3],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
        {
          type: 'textbox',
          id: 'customernote',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,6,6],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
