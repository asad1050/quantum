import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import _ from 'underscore';
import { Collection_ProductItems, Collection_ProductCategories } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Reports.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_StockReport extends Component {

  constructor(props){
		super(props)
		this.state = {
      searchLogic: false,
    };
	}

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let reportAttr = {
        id: 'Report_StockReport',
        title: 'Stock Report',
        inputVarient: "standard",// 'standard', 'filled', 'outlined'
        collection: Collection_ProductItems,
        allowBlankSearch: true,
        reportTableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'itemname', label: 'Name'},
          { dbName: 'itemcategory_inputValue', label: 'Category'},
          { dbName: 'itemprice', label: 'Price'},
          { dbName: 'itemcost', label: 'Cost'},
          { dbName: 'itemmarkup', label: 'Margin'},
          { dbName: 'itemmanufacturer.data.manufacturername', label: 'Manufacturer'},
          { dbName: 'itemquantity', label: 'Quantity'},
        ],
        reportSearchBy: [
          {
            type: 'textbox',
            id: 'itemname',
            disabled: false,
            label: "Name",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'eid',
            disabled: false,
            label: "Id",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'itembarcode',
            disabled: false,
            label: "Barcode",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'itemsku',
            disabled: false,
            label: "SKU",
            size: [12,4,4],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'selectbox',
            id: 'itemcategory',
            disabled: false,
            label: "Category",
            size: [12,4,4],//XS, MD, LG
            dataCollection: Collection_ProductCategories,
            dataCollectionValue: '_id',
            dataCollectionLabel: 'categoryname',
            data: [],
          },
        ],
        baseFunctions: {
          handleChangeSearchLogic: (searchLogic) => {
            this.setState({ searchLogic: searchLogic });
          },
        },
      };
      
      console.log(this.state.searchLogic);
      
    return (
      <div>
        <Reports reportAttr={reportAttr} searchLogic={this.state.searchLogic} />
      </div>
    );
  }
}
