import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import _ from 'underscore';
import { Collection_Accounts, Collection_Transactions, Collection_AccountCategories } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Reports.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_IncomeStatement extends Component {

  constructor(props){
		super(props)
		this.state = {
      searchLogic: {},
    };
	}

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let reportAttr = {
        id: 'Report_IncomeStatement',
        title: 'Income Statement',
        inputVarient: "standard",// 'standard', 'filled', 'outlined'
        collection: Collection_Transactions,
        dataLogic: {},
        customReportLogic: (reportInputData, report) => {
            let customReportData = []; //--- Default (Required) it will container actual data
            let customReportDataRaw = [];
            
            //--- Total Sales
            let totalSalesSearchObject = _.clone(reportInputData);
            totalSalesSearchObject['creditAccount'] = (Collection_Accounts.findOne({ name: 'Sales', appid: sessionStorage.getItem("appid") })._id);
            totalSalesSearchObject.appid = sessionStorage.getItem("appid");
            let Sales = Collection_Transactions.find(totalSalesSearchObject).fetch();
            let totalSales = 0;
            Sales.forEach((sale)=>{
                totalSales += sale.transactionamount;
            });
            //--- Total Cost Of Goods
            let totalCostOfGoodsSearchObject = _.clone(reportInputData);
            totalCostOfGoodsSearchObject['debitAccount'] = (Collection_Accounts.findOne({ name: 'Cost Of Goods', appid: sessionStorage.getItem("appid") })._id);
            totalCostOfGoodsSearchObject.appid = sessionStorage.getItem("appid");
            let Costs = Collection_Transactions.find(totalCostOfGoodsSearchObject).fetch();
            let totalCostOfGoods = 0;
            Costs.forEach((cost)=>{
                totalCostOfGoods += cost.transactionamount;
            });
            //--- Total Expenses
            let expenseCategories = Collection_AccountCategories.find({ group_inputValue: 'Expenses', appid: sessionStorage.getItem("appid") }).fetch();
            let expenseAccountGroups = {};
            let totalExpensesByAccounts = [];
            expenseCategories.forEach((cat) => {
              let expAccount = Collection_Accounts.find({ category: cat._id, name: { $nin: ['Cost Of Goods'] }, appid: sessionStorage.getItem("appid") }).fetch();
              expenseAccountGroups[cat.name] = expAccount;
            });
            for (var i=0;i<Object.keys(expenseAccountGroups).length;i++){
              var accountsByGroup = expenseAccountGroups[Object.keys(expenseAccountGroups)[i]];
              accountsByGroup.forEach((expAccount)=>{

                let totalExepnsesSearchObject = _.clone(reportInputData);
                totalExepnsesSearchObject['debitAccount'] = expAccount._id;
                totalExepnsesSearchObject.appid = sessionStorage.getItem("appid");
                let Expenses = Collection_Transactions.find(totalExepnsesSearchObject).fetch();
                let totalExpenses = 0;
                Expenses.forEach((expense)=>{
                  totalExpenses += expense.transactionamount;
                });
                totalExpensesByAccounts.push({
                  accountName: expAccount.name,
                  totalExpense: totalExpenses,
                });
              });
            }
            let totalIncome = 0;
            if (Object.keys(reportInputData).length > 0){
              //--- Revenues
              customReportData.push({
                group: 'Revenues',
                account: '',
                amount: '',
              });
              customReportData.push({
                group: '',
                account: 'Sales',
                amount: totalSales,
              });
              totalIncome += totalSales;
              //--- Cost Of Goods
              customReportData.push({
                group: '',
                account: 'Cost of Goods',
                amount: totalCostOfGoods,
              });
              totalIncome -= totalCostOfGoods;
              customReportData.push({
                group: 'Gross Profit',
                account: '',
                amount: totalIncome,
              });
              //--- Expenses
              customReportData.push({
                group: 'Expenses',
                account: '',
                amount: '',
              });
              let totalExpensesOverAll = 0;
              totalExpensesByAccounts.forEach((expAccount) => {
                totalExpensesOverAll += expAccount.totalExpense;
                totalIncome -= expAccount.totalExpense;
                customReportData.push({
                  group: '',
                  account: expAccount.accountName,
                  amount: expAccount.totalExpense,
                });
              });
              customReportData.push({
                group: '',
                account: 'Total Expenses',
                amount: totalExpensesOverAll,
              });
              customReportData.push({
                group: 'Net Profit',
                account: '',
                amount: totalIncome,
              });
            }
            
            


            // let totalExepnsesSearchObject = _.clone(reportInputData);
            // totalCostOfGoodsSearchObject['debitAccount'] = (Collection_Accounts.findOne({ name: 'Cost Of Goods' })._id);
            // let Expenses = Collection_Transactions.find(totalExepnsesSearchObject).fetch();
            // let totalExpenses = 0;
            // Expenses.forEach((expense)=>{
            //   totalExpenses += expense.transactionamount;
            // });
            
            
            
            // if (reportInputData && reportInputData.account && Object.keys(reportInputData.account).length > 0){
            //     var remainingSearchLogic = _.clone(reportInputData);
            //     delete remainingSearchLogic.account;
            //     console.log(remainingSearchLogic);
                
            //     customReportDataRaw = Collection_Transactions.find({$or: [ {debitAccount: reportInputData.account._id}, {creditAccount: reportInputData.account._id} ], $and: [remainingSearchLogic]}).fetch();
            // }else{
            //     customReportDataRaw = Collection_Transactions.find(reportInputData).fetch();
            // }
            // if (customReportDataRaw && customReportDataRaw.length > 0){
            //     customReportDataRaw.forEach((data, key)=>{
            //     if (data.debitAccount == reportInputData.account._id){
            //         customReportData.push({
            //         transactiondate: data.transactiondate,
            //         account: data.debitAccount,
            //         debitAmount: data.transactionamount,
            //         creditAmount: '',
            //         balance: data.debitAccountBalance, 
            //         });
            //     }else if(data.creditAccount == reportInputData.account._id){
            //         customReportData.push({
            //         transactiondate: data.transactiondate,
            //         account: data.creditAccount,
            //         debitAmount: '',
            //         creditAmount: data.transactionamount,
            //         balance: data.creditAccountBalance, 
            //         });
            //     }
            //     });
            // }
            
            return customReportData;//--- Default
        },
        reportTableKeys: [
          { type: 'fromDB', dbName: 'group', label: 'Group'},
          { type: 'fromDB', dbName: 'account', label: 'Account'},
          { type: 'fromDB', dbName: 'amount', label: 'Amount'},
        ],
        reportSearchBy: [
          {
            type: 'datebox',
            id: 'transactiondate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date",
            size: [12,12,12],//XS, MD, LG
            dateRange: true,
          },
        ],
        baseFunctions: {
          handleChangeSearchLogic: (searchLogic) => {
            this.setState({ searchLogic: searchLogic });
          },
        },
      };

    return (
      <div>
        <Reports reportAttr={reportAttr} searchLogic={this.state.searchLogic} />
      </div>
    );
  }
}
