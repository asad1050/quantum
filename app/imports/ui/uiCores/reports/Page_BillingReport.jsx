import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import _ from 'underscore';
import { Collection_Accounts, Collection_Transactions, Collection_Cases, Collection_Customers, Collection_Cities } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Reports.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_BillingReport extends Component {

  constructor(props){
		super(props)
		this.state = {
      searchLogic: {},
    };
	}

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let reportAttr = {
        id: 'Page_BillingReport',
        title: 'Billing Report',
        inputVarient: "standard",// 'standard', 'filled', 'outlined'
        collection: Collection_Transactions,
        dataLogic: {},
        extraPrintDetails: (reportInputData, report) => {
            var extraHTMLString = "";
            reportInputData.appid = sessionStorage.getItem("appid");
            if (reportInputData.reporttype == 'bill' && reportInputData.account && Object.keys(reportInputData.account).length > 0) {
              var customer = Collection_Customers.findOne({ account: reportInputData.account._id, appid: sessionStorage.getItem("appid") });
              if (customer){
                  extraHTMLString += '<span style="font-family: Calibri;text-align: center;" > Statement From: <span style="font-weight: 400;" >' + new Date(reportInputData.transactiondate.$gte).toDateString() + ' </span> To: <span style="font-weight: 400;" >' + new Date(reportInputData.transactiondate.$lt).toDateString() + '</span> </span>';
                  extraHTMLString += '<p style="float: left; font-family: Calibri;" > Bill';
                  if (reportInputData.billoption == 'monthly') {
                    extraHTMLString += ' | <span style="font-weight: 400;" > Monthly </span>';
                  } else {
                    extraHTMLString += ' | <span style="font-weight: 400;" > Fortnightly </span>';
                  }
                  extraHTMLString += '<br> Customer: <span style="font-weight: 400;" >' + customer.customername;
                  if (customer.customeraddress && customer.customeraddress.trim() !== "") extraHTMLString += ' </br> ' + customer.customeraddress;
                  if (customer.customercity && customer.customercity.value && customer.customercity.value.trim() !== "") extraHTMLString += ' </br> ' + customer.customercity.value;
                  extraHTMLString += '</span></p> <p style="float: right; font-family: Calibri;" > Account #: <span style="font-weight: 400;" > ' + reportInputData.account.eid + ' </span> </p>';
              }
            } else if (reportInputData.reporttype == 'bill') {
              extraHTMLString += '<span style="font-family: Calibri;text-align: center;" > Statement From: <span style="font-weight: 400;" > ' + new Date(reportInputData.transactiondate.$gte).toDateString() + ' </span> To: <span style="font-weight: 400;" > ' + new Date(reportInputData.transactiondate.$lt).toDateString() + ' </span> </span>';
              extraHTMLString += '<p style="float: left; font-family: Calibri;" > Bill';
              if (reportInputData.billoption == 'monthly') {
                extraHTMLString += ' | <span style="font-weight: 400;" > Monthly </span> </p>';
              } else {
                extraHTMLString += ' | <span style="font-weight: 400;" > Fortnightly </span> </p>';
              }
            } else {
              extraHTMLString += '<span style="font-family: Calibri;text-align: center;" > Statement From: <span style="font-weight: 400;" > ' + new Date(reportInputData.transactiondate.$gte).toDateString() + ' </span> To: <span style="font-weight: 400;" > ' + new Date(reportInputData.transactiondate.$lt).toDateString() + ' </span> </span>';
              extraHTMLString += '<p style="float: left; font-family: Calibri;" > Summery';
              if (reportInputData.billoption == 'monthly') {
                extraHTMLString += ' | <span style="font-weight: 400;" > Monthly </span> </p>';
              } else {
                extraHTMLString += ' | <span style="font-weight: 400;" > Fortnightly </span> </p>';
              }
            }
            
            return extraHTMLString;
        },
        customReportLogic: (reportInputData, report) => {
          let customReportData = []; //--- Default (Required) it will container actual data
          let customReportDataRaw = [];
          if (window.debugMode) console.log("reportInputData: ", reportInputData);
          if (window.debugMode) console.log("Report: ", report);
          
          if (reportInputData.reporttype == 'bill' && reportInputData && reportInputData.account && Object.keys(reportInputData.account).length > 0){
            //--- Changing table header settings
            report.props.reportAttr.reportTableKeys= [
              { type: 'fromDB', dbName: 'casenumber', label: 'Case #'},
              { type: 'fromDB', dbName: 'date', label: 'Date', formatDate: true},
              { type: 'fromDB', dbName: 'worktype', label: 'Type'},
              { type: 'fromDB', dbName: 'caseshade', label: 'Shade'},
              { type: 'fromDB', dbName: 'caseunits', label: 'Units'},
              { type: 'fromDB', dbName: 'price', label: 'Price'},
              { type: 'fromDB', dbName: 'numberofunits', label: 'NOU'},
              { type: 'fromDB', dbName: 'totalamount', label: 'Total Amount'},
              { type: 'fromDB', dbName: 'received', label: 'Received'},
            ];
            
            reportInputData.appid = sessionStorage.getItem("appid");
            var remainingSearchLogic = _.clone(reportInputData);
            delete remainingSearchLogic.account;
            delete remainingSearchLogic.reporttype;
            delete remainingSearchLogic.customercity;
            delete remainingSearchLogic.billoption;
            if (window.debugMode) console.log("REM SEARACH LOGIC: ", remainingSearchLogic);
            remainingSearchLogic.appid = sessionStorage.getItem("appid");
            
            customReportDataRaw = Collection_Transactions.find({$or: [ {formName: 'Form_Cases'}, {formName: 'Form_CasePayment'} ],$or: [ {debitAccount: reportInputData.account._id}, {creditAccount: reportInputData.account._id} ], $and: [remainingSearchLogic]}).fetch();
            if (window.debugMode) console.log("RAW DATA---------: ", customReportDataRaw);
            if (customReportDataRaw && customReportDataRaw.length > 0){
              customReportDataRaw.forEach((data, key)=>{
                  var selectedCase = Collection_Cases.findOne( { accounttransactions: [data._id], appid: sessionStorage.getItem("appid") } );
                  if (selectedCase){
                      var selectedCaseUnits = selectedCase.caseunits;
                      var selectedCaseNumberOfUnits = selectedCaseUnits.length;
                      var selectedCaseUnitsString = "";
                      selectedCaseUnits.forEach((selectedCaseUnit, selectedCaseUnitKey)=>{
                          if (selectedCaseUnit.unit !== 'Total'){
                              if (selectedCaseUnitKey !== 0){
                                  selectedCaseUnitsString += ',';
                              }
                              selectedCaseUnitsString += selectedCaseUnit.unit;
                          }
                      });
                      customReportData.push({
                          casenumber: selectedCase.eid,
                          date: data.transactiondate,
                          worktype: (selectedCase.worktype.data.worktypename) ? selectedCase.worktype.data.worktypename : '',
                          caseshade: (selectedCase.caseshade.data.name) ? selectedCase.caseshade.data.name : '',
                          caseunits: selectedCaseUnitsString,
                          price: selectedCase.worktype.data.worktypeprice,
                          numberofunits: selectedCaseNumberOfUnits,
                          totalamount: selectedCase.totalamount,
                          received: 0,
                        });
                  } else {
                      var receivedAmount = data.transactionamount;
                      if (data.debitAccount == reportInputData.account._id){
                          receivedAmount = data.transactionamount * -1;
                      } else if (data.creditAccount == reportInputData.account._id) {
                          receivedAmount = data.transactionamount;
                      }
                      customReportData.push({
                          casenumber: "",
                          date: data.transactiondate,
                          worktype: data.transactiondescription,
                          caseshade: "",
                          caseunits: "",
                          price: "",
                          numberofunits: "",
                          totalamount: 0,
                          received: receivedAmount,
                        });
                  }
              });
              var totalAmount = 0;
              var totalReceived = 0;
              customReportData.forEach((customReportDataUnit, customReportDataKey) => {
                  totalAmount += parseFloat(customReportDataUnit.totalamount);
                  totalReceived += parseFloat(customReportDataUnit.received);
              });
              var customerAccount = Collection_Accounts.findOne({ _id: reportInputData.account._id, appid: sessionStorage.getItem("appid") });
              //--- Total row
              customReportData.push({
                  casenumber: "",
                  date: "",
                  worktype: "",
                  caseshade: "",
                  caseunits: "Total",
                  price: "",
                  numberofunits: "",
                  totalamount: '<b>'+ totalAmount +'</b >',
                  received: '<b>'+ totalReceived +'</b>',
                });
              var prevousBalance = customerAccount.balance;
              prevousBalance += totalReceived;
              prevousBalance -= totalAmount;
              //--- Previous Balance
              customReportData.push({
                casenumber: "",
                date: "",
                worktype: "",
                caseshade: "",
                caseunits: "Previous Balance",
                price: "",
                numberofunits: "",
                totalamount: "",
                received: prevousBalance,
              }); 
              //--- Current Balance
              customReportData.push({
                  casenumber: "",
                  date: "",
                  worktype: "",
                  caseshade: "",
                  caseunits: "Current Balance",
                  price: "",
                  numberofunits: "",
                  totalamount: "",
                  received: '<b>'+customerAccount.balance+'</b>',
                });
            }
          }else if (reportInputData.reporttype == 'bill') {
            //--- Changing table header settings
            report.props.reportAttr.reportTableKeys= [
              { type: 'fromDB', dbName: 'casenumber', label: ''},
              { type: 'fromDB', dbName: 'date', label: '', formatDate: true},
              { type: 'fromDB', dbName: 'worktype', label: ''},
              { type: 'fromDB', dbName: 'caseshade', label: ''},
              { type: 'fromDB', dbName: 'caseunits', label: ''},
              { type: 'fromDB', dbName: 'price', label: ''},
              { type: 'fromDB', dbName: 'numberofunits', label: ''},
              { type: 'fromDB', dbName: 'totalamount', label: ''},
              { type: 'fromDB', dbName: 'received', label: ''},
            ];

            
            var remainingSearchLogic = _.clone(reportInputData);
            delete remainingSearchLogic.account;
            delete remainingSearchLogic.reporttype;
            delete remainingSearchLogic.customercity;
            delete remainingSearchLogic.billoption;
            remainingSearchLogic.appid = sessionStorage.getItem("appid");
            
            if (reportInputData.customercity && Object.keys(reportInputData.customercity).length > 0) {
              remainingSearchLogic['additionalField.customercity.id'] = reportInputData.customercity._id;
            }
            if (reportInputData.billoption && Object.keys(reportInputData.billoption).length > 0) {
              remainingSearchLogic['additionalField.billoption'] = reportInputData.billoption;
            }
            if (window.debugMode) console.log("RES INPUT -------: ", remainingSearchLogic);
            if (reportInputData.account && Object.keys(reportInputData.account).length > 0) {
              customReportDataRaw = Collection_Transactions.find({$or: [ {formName: 'Form_Cases'}, {formName: 'Form_CasePayment'} ],$or: [ {debitAccount: reportInputData.account._id}, {creditAccount: reportInputData.account._id} ], $and: [remainingSearchLogic]}).fetch();
            } else {
              customReportDataRaw = Collection_Transactions.find({$or: [ {formName: 'Form_Cases'}, {formName: 'Form_CasePayment'} ],  $and: [remainingSearchLogic]}).fetch();
            }
            if (window.debugMode) console.log("RAW DATA---------: ", customReportDataRaw);

            if (customReportDataRaw && customReportDataRaw.length > 0){
              var customerWiseData = {};
              customReportDataRaw.forEach((data, key)=>{
                  if (data.formName == 'Form_Cases') {
                    var customerAccount = data.debitAccount;
                    var selectedCase = Collection_Cases.findOne( { accounttransactions: [data._id], appid: sessionStorage.getItem("appid") } );
                    if (selectedCase){
                      var selectedCaseUnits = selectedCase.caseunits;
                      var selectedCaseNumberOfUnits = selectedCaseUnits.length;
                      var selectedCaseUnitsString = "";
                      selectedCaseUnits.forEach((selectedCaseUnit, selectedCaseUnitKey)=>{
                          if (selectedCaseUnit.unit !== 'Total'){
                              if (selectedCaseUnitKey !== 0){
                                  selectedCaseUnitsString += ',';
                              }
                              selectedCaseUnitsString += selectedCaseUnit.unit;
                          }
                      });
                      if (typeof customerWiseData[customerAccount] == 'undefined') customerWiseData[customerAccount] = [];
                      customerWiseData[customerAccount].push({
                        _id: data._id,
                        casenumber: selectedCase.eid,
                        date: data.transactiondate,
                        transactiondate: data.transactiondate,
                        transactionamount: data.transactionamount,
                        worktype: (selectedCase.worktype.data.worktypename) ? selectedCase.worktype.data.worktypename : '',
                        caseshade: (selectedCase.caseshade.data.name) ? selectedCase.caseshade.data.name : '',
                        caseunits: selectedCaseUnitsString,
                        price: selectedCase.worktype.data.worktypeprice,
                        numberofunits: selectedCaseNumberOfUnits,
                        totalamount: selectedCase.totalamount,
                        received: 0,
                        additionalField: data.additionalField,
                      });
                    }
                  } else if (data.formName == 'Form_CasePayment') {
                    var customerAccount = data.creditAccount;
                    var receivedAmount = data.transactionamount;
                    if (typeof customerWiseData[customerAccount] == 'undefined') customerWiseData[customerAccount] = [];
                    customerWiseData[customerAccount].push({
                      _id: data._id,
                      casenumber: "",
                      date: data.transactiondate,
                      transactiondate: data.transactiondate,
                      transactionamount: data.transactionamount,
                      worktype: data.transactiondescription,
                      caseshade: "",
                      caseunits: "",
                      price: "",
                      numberofunits: "",
                      totalamount: 0,
                      received: receivedAmount,
                      additionalField: data.additionalField,
                    });
                  }
              });
              if (window.debugMode) console.log("CUSTOMER WISE DATQA: ", customerWiseData);
              for (var i=0;i<Object.keys(customerWiseData).length;i++) {
                var customerDataKey = Object.keys(customerWiseData)[i];
                var customerData = customerWiseData[customerDataKey];
                if (window.debugMode) console.log("Data: ", customerData);
                if (window.debugMode) console.log("KEY: ", customerDataKey);
                var customerAccount = Collection_Accounts.findOne({ _id: customerData[0].additionalField.account });

                //--- Creating bill header

                customReportData.push({
                  casenumber: customerData[0].additionalField.customername,
                  date: "",
                  worktype: "",
                  caseshade: "",
                  caseunits: "",
                  price: "",
                  numberofunits: "",
                  totalamount: "",
                  received: ("Account: " + customerAccount.eid),
                });

                customReportData.push({
                  casenumber: customerData[0].additionalField.customercity.value,
                  date: "",
                  worktype: "",
                  caseshade: "",
                  caseunits: "",
                  price: "",
                  numberofunits: "",
                  totalamount: "",
                  received: "",
                });
                customReportData.push({
                  casenumber: "",
                  date: "",
                  worktype: "",
                  caseshade: "",
                  caseunits: "",
                  price: "",
                  numberofunits: "",
                  totalamount: "",
                  received: "",
                });
                customReportData.push({
                  casenumber: "Case #",
                  date: "Date",
                  worktype: "Type",
                  caseshade: "Shade",
                  caseunits: "Units",
                  price: "Price",
                  numberofunits: "NOU",
                  totalamount: "Total Amount",
                  received: "Received",
                });

                //--- End creating bill header

                if (customerData.length > 0) {
                  //--- Create bill
                  var currentReortData = [];
                  customerData.forEach((data, key)=>{
                    if (window.debugMode) console.log("DATA PACKETS : ", data);
                    
                    var selectedCase = Collection_Cases.findOne( { accounttransactions: [data._id], appid: sessionStorage.getItem("appid") } );
                    if (selectedCase){
                        var selectedCaseUnits = selectedCase.caseunits;
                        var selectedCaseNumberOfUnits = selectedCaseUnits.length;
                        var selectedCaseUnitsString = "";
                        selectedCaseUnits.forEach((selectedCaseUnit, selectedCaseUnitKey)=>{
                            if (selectedCaseUnit.unit !== 'Total'){
                                if (selectedCaseUnitKey !== 0){
                                    selectedCaseUnitsString += ',';
                                }
                                selectedCaseUnitsString += selectedCaseUnit.unit;
                            }
                        });
                        customReportData.push({
                            casenumber: selectedCase.eid,
                            date: data.transactiondate,
                            worktype: (selectedCase.worktype.data.worktypename) ? selectedCase.worktype.data.worktypename : '',
                            caseshade: (selectedCase.caseshade.data.name) ? selectedCase.caseshade.data.name : '',
                            caseunits: selectedCaseUnitsString,
                            price: selectedCase.worktype.data.worktypeprice,
                            numberofunits: selectedCaseNumberOfUnits,
                            totalamount: selectedCase.totalamount,
                            received: 0,
                          });
                        currentReortData.push({
                          casenumber: selectedCase.eid,
                          date: data.transactiondate,
                          worktype: selectedCase.worktype.value,
                          caseshade: selectedCase.caseshade.value,
                          caseunits: selectedCaseUnitsString,
                          price: selectedCase.worktype.data.worktypeprice,
                          numberofunits: selectedCaseNumberOfUnits,
                          totalamount: selectedCase.totalamount,
                          received: 0,
                        });
                    } else {
                        var receivedAmount = data.transactionamount;
                        if (data.debitAccount == customerData[0].additionalField.account){
                            receivedAmount = data.transactionamount * -1;
                        } else if (data.creditAccount == customerData[0].additionalField.account) {
                            receivedAmount = data.transactionamount;
                        }
                        customReportData.push({
                            casenumber: "",
                            date: data.transactiondate,
                            worktype: data.transactiondescription,
                            caseshade: "",
                            caseunits: "",
                            price: "",
                            numberofunits: "",
                            totalamount: 0,
                            received: receivedAmount,
                          });

                        currentReortData.push({
                          casenumber: "",
                          date: data.transactiondate,
                          worktype: data.transactiondescription,
                          caseshade: "",
                          caseunits: "",
                          price: "",
                          numberofunits: "",
                          totalamount: 0,
                          received: receivedAmount,
                        });
                    }
                });
                var totalAmount = 0;
                var totalReceived = 0;
                currentReortData.forEach((customReportDataUnit, customReportDataKey) => {
                    totalAmount += parseFloat(customReportDataUnit.totalamount);
                    totalReceived += parseFloat(customReportDataUnit.received);
                });
                //--- Total row
                customReportData.push({
                    casenumber: "",
                    date: "",
                    worktype: "",
                    caseshade: "",
                    caseunits: "Total",
                    price: "",
                    numberofunits: "",
                    totalamount: '<b>'+totalAmount+'</b>',
                    received: '<b>'+totalReceived+'</b>',
                  });
                var prevousBalance = customerAccount.balance;
                prevousBalance += totalReceived;
                prevousBalance -= totalAmount;
                //--- Previous Balance
                customReportData.push({
                  casenumber: "",
                  date: "",
                  worktype: "",
                  caseshade: "",
                  caseunits: "Previous Balance",
                  price: "",
                  numberofunits: "",
                  totalamount: "",
                  received: prevousBalance,
                }); 
                //--- Current Balance
                customReportData.push({
                    casenumber: "",
                    date: "",
                    worktype: "",
                    caseshade: "",
                    caseunits: "Current Balance",
                    price: "",
                    numberofunits: "",
                    totalamount: "",
                    received: '<b>'+customerAccount.balance+'</b>',
                  });

                  //--- Blank Space
                  customReportData.push({
                    casenumber: " ",
                    date: "",
                    worktype: " ",
                    caseshade: " ",
                    caseunits: " ",
                    price: " ",
                    numberofunits: " ",
                    totalamount: " ",
                    received: " ",
                  });
                  customReportData.push({
                    casenumber: " ",
                    date: "",
                    worktype: " ",
                    caseshade: " ",
                    caseunits: " ",
                    price: " ",
                    numberofunits: " ",
                    totalamount: " ",
                    received: " ",
                  });
                  customReportData.push({
                    casenumber: "______",
                    date: "_____",
                    worktype: "______",
                    caseshade: "______",
                    caseunits: "______",
                    price: "______",
                    numberofunits: "______",
                    totalamount: "______",
                    received: "______",
                  });
                  customReportData.push({
                    casenumber: " ",
                    date: "",
                    worktype: " ",
                    caseshade: " ",
                    caseunits: " ",
                    price: " ",
                    numberofunits: " ",
                    totalamount: " ",
                    received: " ",
                  });
                  customReportData.push({
                    casenumber: " ",
                    date: "",
                    worktype: " ",
                    caseshade: " ",
                    caseunits: " ",
                    price: " ",
                    numberofunits: " ",
                    totalamount: " ",
                    received: " ",
                  });

                  //--- End create bill
                }
                

                
              }
              
            }

          }else{
            //--- Changing table header settings
            report.props.reportAttr.reportTableKeys= [
              { type: 'fromDB', dbName: 'customername', label: 'Dr/Customer Name'},
              { type: 'fromDB', dbName: 'previousbalance', label: 'Prev Balance'},
              { type: 'fromDB', dbName: 'billamount', label: 'Bill Amt'},
              { type: 'fromDB', dbName: 'total', label: 'Total'},
              { type: 'fromDB', dbName: 'received', label: 'Received'},
              { type: 'fromDB', dbName: 'balance', label: 'Balance'},
            ];

            var remainingSearchLogic = _.clone(reportInputData);
            delete remainingSearchLogic.account;
            delete remainingSearchLogic.reporttype;
            delete remainingSearchLogic.customercity;
            delete remainingSearchLogic.billoption;
            remainingSearchLogic.appid = sessionStorage.getItem("appid");
            
            if (reportInputData.customercity && Object.keys(reportInputData.customercity).length > 0) {
              remainingSearchLogic['additionalField.customercity.id'] = reportInputData.customercity._id;
            }
            if (reportInputData.billoption && Object.keys(reportInputData.billoption).length > 0) {
              remainingSearchLogic['additionalField.billoption'] = reportInputData.billoption;
            }
            if (window.debugMode) console.log("RES INPUT -------: ", remainingSearchLogic);
            if (reportInputData.account && Object.keys(reportInputData.account).length > 0) {
              customReportDataRaw = Collection_Transactions.find({$or: [ {formName: 'Form_Cases'}, {formName: 'Form_CasePayment'} ],$or: [ {debitAccount: reportInputData.account._id}, {creditAccount: reportInputData.account._id} ], $and: [remainingSearchLogic]}).fetch();
            } else {
              customReportDataRaw = Collection_Transactions.find({$or: [ {formName: 'Form_Cases'}, {formName: 'Form_CasePayment'} ],  $and: [remainingSearchLogic]}).fetch();
            }
            if (window.debugMode) console.log("RAW DATA---------: ", customReportDataRaw);

            if (customReportDataRaw && customReportDataRaw.length > 0){
              var customerWiseData = {};
              customReportDataRaw.forEach((data, key)=>{
                  if (data.formName == 'Form_Cases') {
                    var customerAccount = data.debitAccount;
                    var selectedCase = Collection_Cases.findOne( { accounttransactions: [data._id], appid: sessionStorage.getItem("appid") } );
                    if (selectedCase){
                      var selectedCaseUnits = selectedCase.caseunits;
                      var selectedCaseNumberOfUnits = selectedCaseUnits.length;
                      var selectedCaseUnitsString = "";
                      selectedCaseUnits.forEach((selectedCaseUnit, selectedCaseUnitKey)=>{
                          if (selectedCaseUnit.unit !== 'Total'){
                              if (selectedCaseUnitKey !== 0){
                                  selectedCaseUnitsString += ',';
                              }
                              selectedCaseUnitsString += selectedCaseUnit.unit;
                          }
                      });
                      if (typeof customerWiseData[customerAccount] == 'undefined') customerWiseData[customerAccount] = [];
                      customerWiseData[customerAccount].push({
                        casenumber: selectedCase.eid,
                        date: data.transactiondate,
                        worktype: selectedCase.worktype.value,
                        caseshade: selectedCase.caseshade.value,
                        caseunits: selectedCaseUnitsString,
                        price: selectedCase.worktype.data.worktypeprice,
                        numberofunits: selectedCaseNumberOfUnits,
                        totalamount: selectedCase.totalamount,
                        received: 0,
                        additionalField: data.additionalField,
                      });
                    }
                  } else if (data.formName == 'Form_CasePayment') {
                    var customerAccount = data.creditAccount;
                    var receivedAmount = data.transactionamount;
                    if (typeof customerWiseData[customerAccount] == 'undefined') customerWiseData[customerAccount] = [];
                    customerWiseData[customerAccount].push({
                      casenumber: "",
                      date: data.transactiondate,
                      worktype: data.transactiondescription,
                      caseshade: "",
                      caseunits: "",
                      price: "",
                      numberofunits: "",
                      totalamount: 0,
                      received: receivedAmount,
                      additionalField: data.additionalField,
                    });
                  }
              });
              if (window.debugMode) console.log("CUSTOMER WISE DATQA: ", customerWiseData);
              for (var i=0;i<Object.keys(customerWiseData).length;i++) {
                var customerDataKey = Object.keys(customerWiseData)[i];
                var customerData = customerWiseData[customerDataKey];
                if (window.debugMode) console.log("Data: ", customerData);
                if (window.debugMode) console.log("KEY: ", customerDataKey);
                if (customerData.length > 0) {
                  //--- City Data
                  customReportData.push({
                    customername: customerData[0].additionalField.customercity.value,
                    previousbalance: "",
                    billamount: '',
                    total: '',
                    received: "",
                    balance: "",
                  });

                  // customerData.forEach((data) => {
                  //   customReportData.push(data);
                  // });

                  var totalAmount = 0;
                  var totalReceived = 0;
                  customerData.forEach((customReportDataUnit, customReportDataKey) => {
                      totalAmount += parseFloat(customReportDataUnit.totalamount);
                      totalReceived += parseFloat(customReportDataUnit.received);
                  });
                  var customerAccount = Collection_Accounts.findOne({ _id: customerDataKey });
                  var prevousBalance = customerAccount.balance;
                  prevousBalance += totalReceived;
                  prevousBalance -= totalAmount;
                  //--- Total row
                  customReportData.push({
                    customername: customerData[0].additionalField.customername,
                    previousbalance: prevousBalance,
                    billamount: '0',
                    total: totalAmount,
                    received: totalReceived,
                    balance: customerAccount.balance,
                  });
                }
                

                
              }
              
            }
            
          }
          
          
          
          if (window.debugMode) console.log("REPORT DATA: ", customReportData);
          
          return customReportData;//--- Default
        },
        reportTableKeys: [
          { type: 'fromDB', dbName: 'casenumber', label: 'Case #'},
          { type: 'fromDB', dbName: 'date', label: 'Date', formatDate: true},
          { type: 'fromDB', dbName: 'worktype', label: 'Type'},
          { type: 'fromDB', dbName: 'caseshade', label: 'Shade'},
          { type: 'fromDB', dbName: 'caseunits', label: 'Units'},
          { type: 'fromDB', dbName: 'price', label: 'Price'},
          { type: 'fromDB', dbName: 'numberofunits', label: 'NOU'},
          { type: 'fromDB', dbName: 'totalamount', label: 'Total Amount'},
          { type: 'fromDB', dbName: 'received', label: 'Received'},
        ],
        reportSearchBy: [
          {
            type: 'datebox',
            id: 'transactiondate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date",
            size: [12,6,6],//XS, MD, LG
            dateRange: true,
          },
          {
            type: 'autocomplete',
            id: 'customercity',
            required: false,
            disabled: false,
            label: "City",
            size: [12,6,6],//XS, MD, LG
            dataCollection: Collection_Cities,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'name'],//Show specific keys in search
            data: [],// also Required if blank
          },
          {
            type: 'autocomplete',
            id: 'account',
            required: false,
            disabled: false,
            label: "Customer",
            size: [12,4,4],//XS, MD, LG
            dataCollection: Collection_Accounts,
            dataLogic: { group_inputValue: 'Accounts Receivable' },
            uniqueKey: 'eid',
            searchKeys: ['eid', 'name'],//Show specific keys in search
            data: [],// also Required if blank
            extraInformation: [
              {
                  key: 'group_inputValue',
                  label: 'Group',
              },
            ],
          },
          {
            type: 'selectbox',
            id: 'reporttype',
            required: false,
            disabled: false,
            label: "Choose report type",
            size: [12,4,4],//XS, MD, LG
            data: [
              { value: 'summery', label: 'Summery' },
              { value: 'bill', label: 'Bill' },
            ],
          },
          {
            type: 'selectbox',
            id: 'billoption',
            required: false,
            disabled: false,
            label: "Choose bill option",
            size: [12,4,4],//XS, MD, LG
            data: [
              { value: 'monthly', label: 'Monthly' },
              { value: 'fortnightly', label: 'Fortnightly' },
            ],
          },
        ],
        baseFunctions: {
          handleChangeSearchLogic: (searchLogic) => {
            this.setState({ searchLogic: searchLogic });
          },
        },
      };

    return (
      <div>
        <Reports reportAttr={reportAttr} searchLogic={this.state.searchLogic} />
      </div>
    );
  }
}
