import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import _ from 'underscore';
import { Collection_Accounts, Collection_Transactions } from '../../../api/collections';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Reports.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_AccountLedger extends Component {

  constructor(props){
		super(props)
		this.state = {
      searchLogic: {},
    };
	}

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let reportAttr = {
        id: 'Report_AccountLedger',
        title: 'Account Ledger',
        inputVarient: "standard",// 'standard', 'filled', 'outlined'
        collection: Collection_Transactions,
        dataLogic: {},
        customReportLogic: (reportInputData, report) => {
          let customReportData = []; //--- Default (Required) it will container actual data
          let customReportDataRaw = [];
          console.log(reportInputData);
          console.log(report);
          console.log(Collection_Transactions.find({ appid: sessionStorage.getItem("appid") }, {sort: {transactiondate: 1}}).fetch());
          
          if (reportInputData && reportInputData.account && Object.keys(reportInputData.account).length > 0){
            reportInputData.appid = sessionStorage.getItem("appid");
            var remainingSearchLogic = _.clone(reportInputData);
            delete remainingSearchLogic.account;
            remainingSearchLogic.appid = sessionStorage.getItem("appid");
            console.log(remainingSearchLogic);
            
            customReportDataRaw = Collection_Transactions.find({$or: [ {debitAccount: reportInputData.account._id}, {creditAccount: reportInputData.account._id} ], $and: [remainingSearchLogic]}, {sort: {transactiondate: 1}}).fetch();
          }else{
            customReportDataRaw = Collection_Transactions.find(reportInputData, {sort: {transactiondate: 1}}).fetch();
          }
          if (customReportDataRaw && customReportDataRaw.length > 0){
            customReportDataRaw.forEach((data, key)=>{
              if (data.debitAccount == reportInputData.account._id){
                customReportData.push({
                  transactiondate: data.transactiondate,
                  account: data.debitAccount,
                  transactiondescription: data.transactiondescription,
                  debitAmount: data.transactionamount,
                  creditAmount: '',
                  balance: data.debitAccountBalance, 
                });
              }else if(data.creditAccount == reportInputData.account._id){
                customReportData.push({
                  transactiondate: data.transactiondate,
                  account: data.creditAccount,
                  transactiondescription: data.transactiondescription,
                  debitAmount: '',
                  creditAmount: data.transactionamount,
                  balance: data.creditAccountBalance, 
                });
              }
            });
          }
          
          return customReportData;//--- Default
        },
        reportTableKeys: [
          { type: 'fromDB', dbName: 'transactiondate', label: 'Date', formatDate: true},
          { type: 'fromDB', dbName: 'account', label: 'Account', subCollectionField: { collection: Collection_Accounts, selectionKey: '_id', valueKey: 'name' }},
          { type: 'fromDB', dbName: 'transactiondescription', label: 'Description'},
          { type: 'fromDB', dbName: 'debitAmount', label: 'Debit Amount', sumField: { sumField: true, fieldLabel: 'Total Debit Transaction' }},
          { type: 'fromDB', dbName: 'creditAmount', label: 'Credit Amount', sumField: { sumField: true, fieldLabel: 'Total Credit Transaction' }},
          { type: 'fromDB', dbName: 'balance', label: 'Balance'},
        ],
        reportSearchBy: [
          {
            type: 'datebox',
            id: 'transactiondate',
            format: 'dd/MM/yyyy',
            disabled: false,
            label: "Date",
            size: [12,6,6],//XS, MD, LG
            dateRange: true,
          },
          {
            type: 'autocomplete',
            id: 'account',
            required: true,
            disabled: false,
            label: "Account",
            size: [12,6,6],//XS, MD, LG
            dataCollection: Collection_Accounts,
            uniqueKey: 'eid',
            searchKeys: ['eid', 'name'],//Show specific keys in search
            data: [],// also Required if blank
            extraInformation: [
              {
                  key: 'group_inputValue',
                  label: 'Group',
              },
            ],
          },
        ],
        baseFunctions: {
          handleChangeSearchLogic: (searchLogic) => {
            this.setState({ searchLogic: searchLogic });
          },
        },
      };

    return (
      <div>
        <Reports reportAttr={reportAttr} searchLogic={this.state.searchLogic} />
      </div>
    );
  }
}
