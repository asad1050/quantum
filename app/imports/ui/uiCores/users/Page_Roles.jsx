import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Roles, Collection_AccountCategories, Collection_Users } from '../../../api/collections';
import GlobalConfig from '../../../config/global.config.js';

import Countries from '../../../modules/modules/Countries';
import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_GeneralSettings extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);

    let formAttr = {
        id: "Form_Roles",
        title: "Roles",
        extraIds: [
          { prefix: 'c', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
        ],
        searchKey: 'eid',//Key would be used to search for value from collection
        inputVarient: "standard",// 'standard', 'filled', 'outlined'
        collection: Collection_Roles,
        chechForExistanceBeforeDeleteInCollections: [
          {
            collection: Collection_Users._name,
            entryKey: 'profile.role',
          },
        ],
        uniqueKeys: ['eid', 'rolename'],// These calues should noe be same in DB
        createSuccessMessage: "Role created successfully",
        createErrorMessage: "Error while creating Role",
        updateSuccessMessage: "Role updated successfully",
        updateErrorMessage: "Error while updating Role",
        deleteSuccessMessage: "Role deleted successfully",
        deleteErrorMessage: "Error while deleting Role",
        instantSearchConfig: {
          instantSearchLogic: {},
          unitIndependent: true,
          searchStringDBKeys: ['eid', 'rolename'],
        },
        openEditDialogConfig: {
          detailedSearch: false,
          textChangeSearch: true,
          tableKeys: [
            { dbName: 'eid', label: 'Id'},
            { dbName: 'categoryname', label: 'Name'},
          ],
          detailedSearchBy: [],
        },
        defaultEntries: [
          {
            name: 'appid',
            value: sessionStorage.getItem("appid"),
          },
        ],
        components: [
        ],
      };

    let pages = GlobalConfig().pages;
    let views = [];
    pages.forEach((page, key)=>{
        if (page.subPages){
            page.subPages.forEach((subPage, subKey) => {
                if (typeof subPage.adminAccessOnly == 'undefined' || subPage.adminAccessOnly == false) {
                    views.push({
                        name: subPage.name,
                        type: subPage.type,
                    });
                }
            });
        }else{
            if (page.id !== 'dashboard_menu' && (typeof page.adminAccessOnly == 'undefined' || page.adminAccessOnly == false)){
                views.push({
                    name: page.name,
                    type: page.type,
                });
            }
        }
    });
    let entryComponents = [];
    entryComponents.push({
        type: 'textbox',
        id: 'rolename',
        required: true,
        disabled: false,
        label: "Role Name",
        size: [12,12,12],//XS, MD, LG
        password: false,
        readOnly: false,
        number: false,
      });
    views.forEach((view, key)=>{
        entryComponents.push({
            type: 'text',
            id: 'title',
            tag: 'subtitle1',
            label: view.name,
            size: [12,12,12],//XS, MD, LG
        });
        if (view.type == 'form'){
            entryComponents.push({
                type: 'checkbox',
                id: view.name + '_create',
                disabled: false,
                label: "Create",
                size: [2,2,2],//XS, MD, LG
                color: 'primary',
            });
            entryComponents.push({
                type: 'checkbox',
                id: view.name + '_update',
                disabled: false,
                label: "Update",
                size: [2,2,2],//XS, MD, LG
                color: 'primary',
            });
            entryComponents.push({
                type: 'checkbox',
                id: view.name + '_delete',
                disabled: false,
                label: "Delete",
                size: [2,2,2],//XS, MD, LG
                color: 'primary',
            });
            entryComponents.push({
              type: 'checkbox',
              id: view.name + '_access',
              disabled: false,
              label: "Accessible",
              size: [2,2,2],//XS, MD, LG
              color: 'primary',
          });
            entryComponents.push({
              type: 'checkbox',
              id: view.name + '_print',
              disabled: false,
              label: "Print",
              size: [2,2,2],//XS, MD, LG
              color: 'primary',
          });
        }else if(view.type == 'save' || view.type == 'report'){
            entryComponents.push({
                type: 'checkbox',
                id: view.name + '_access',
                disabled: false,
                label: "Accessible",
                size: [12,12,12],//XS, MD, LG
                color: 'primary',
            });
        }
    });
    formAttr.components = entryComponents;
    


    

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
