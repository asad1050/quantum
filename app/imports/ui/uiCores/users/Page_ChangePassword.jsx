import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import swal from 'sweetalert2';

import Countries from '../../../modules/modules/Countries';
import EntriesUpdater from '../../../modules/modules/entriesUpdater';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_ChangePassword extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let entriesUpdaterAttr = {
      id: "Form_ChangePassword",
      title: "Change Password",
      idKey: 'appid',
      idValue: sessionStorage.getItem("appid"),
      onSubmit: (inputData, form) => {
        if (inputData.newpassword !== inputData.confirmpassword) {
            swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'New password does not match with confirm password.' ,
            });
            return;
        }
        swal.fire({
            title: 'Updating Field',
            onBeforeOpen: () => {
              swal.showLoading();
              Accounts.changePassword(inputData.oldpassword, inputData.newpassword, (err, res) => {
                if (err) {
                    swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Unable to change password, Reason: ' + err.error ,
                    });
                } else {
                    form.resetFormInputs();
                    swal.fire({
                        type: 'success',
                        title: 'Success',
                        text: 'Password changed successfully',
                        focusConfirm: true,
                    })
                }
              });
            },
          });
      },
      updateSuccessMessage: "Settings updated successfully",
      updateErrorMessage: "Error while updating settings",
      components: [
        {
            type: 'textbox',
            id: 'oldpassword',
            required: true,
            disabled: false,
            label: "Old Password",
            size: [12,12,12],//XS, MD, LG
            password: true,
            readOnly: false,
            number: false,
        },
        {
          type: 'textbox',
          id: 'newpassword',
          required: true,
          disabled: false,
          label: "New Password",
          size: [12,6,6],//XS, MD, LG
          password: true,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'confirmpassword',
          required: true,
          disabled: false,
          label: "Confirm Password",
          size: [12,6,6],//XS, MD, LG
          password: true,
          readOnly: false,
          number: false,
        },
      ],
    };

    return (
      <div>
        <EntriesUpdater entriesUpdaterAttr={entriesUpdaterAttr} />
      </div>
    );
  }
}
