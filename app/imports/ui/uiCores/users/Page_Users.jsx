import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Users, Collection_Roles, Collection_Units } from '../../../api/collections';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_ProductCategories extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_ProductCategories",
      title: "Product Categories",
      userForm: true,
      extraIds: [
        { prefix: 'c', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_Users,
      chechForExistanceBeforeDeleteInCollections: [
      ],
      createSuccessMessage: "User created successfully",
      createErrorMessage: "Error while creating User",
      updateSuccessMessage: "User updated successfully",
      updateErrorMessage: "Error while updating User",
      deleteSuccessMessage: "User deleted successfully",
      deleteErrorMessage: "Error while deleting User",
      instantSearchConfig: {
        instantSearchLogic: { username: { $nin: ['asad', 'admin'] }, 'profile.appid': sessionStorage.getItem("appid") },
        searchStringDBKeys: ['username'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        tableKeys: [
          { dbName: 'eid', label: 'Id'},
          { dbName: 'categoryname', label: 'Name'},
        ],
        detailedSearchBy: [],
      },
      formEntryLogics: [//Checked at add and update
        {
          entry1: 'password',
          entry2: 'confirmpassword',
          operand: '=='
        },
      ],
      defaultEntries: [
        {
          name: 'appid',
          value: sessionStorage.getItem("appid"),
        },
      ],
      components: [
        {
            type: 'textbox',
            id: 'firstname',
            required: true,
            disabled: false,
            label: "First Name",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
        {
            type: 'textbox',
            id: 'lastname',
            required: true,
            disabled: false,
            label: "Last Name",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
        {
            type: 'textbox',
            id: 'username',
            required: true,
            disabled: false,
            label: "Username",
            size: [12,12,12],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
        },
        {
            type: 'textbox',
            id: 'password',
            required: false,
            disabled: false,
            label: "Password",
            size: [12,6,6],//XS, MD, LG
            password: true,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'confirmpassword',
            required: false,
            disabled: false,
            label: "Confirm Password",
            size: [12,6,6],//XS, MD, LG
            password: true,
            readOnly: false,
            number: false,
          },
          {
            type: 'textbox',
            id: 'email',
            required: true,
            disabled: false,
            label: "Email",
            size: [12,6,6],//XS, MD, LG
            password: false,
            readOnly: false,
            number: false,
          },
          {
            type: 'selectbox',
            id: 'role',
            required: true,
            disabled: false,
            label: "Role",
            size: [12,6,6],//XS, MD, LG
            dataCollection: Collection_Roles,
            unitIndependent: true,
            dataCollectionValue: '_id',
            dataCollectionLabel: 'rolename',
          },
          {
            type: 'selectbox',
            id: 'unit',
            required: true,
            disabled: false,
            label: "Unit",
            size: [12,12,12],//XS, MD, LG
            dataCollection: Collection_Units,
            unitIndependent: true,
            dataCollectionValue: '_id',
            dataCollectionLabel: 'name',
          },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
