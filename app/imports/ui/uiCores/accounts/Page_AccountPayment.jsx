import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Collection_Accounts, Collection_AccountPayment } from '../../../api/collections';

import Forms from '../../../modules/modules/Forms.js';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const Styles = styled.div`
  .card {
    max-width: 345;
  }
  .card-description {
    height: 80px;
  }
  .media {
    height: 140;
  }
  .container {
    padding: 32px 32px;
  }
  .btn-danger{
    color: #ff0000c4;
  }
  .add-icon-container{
    height: 144px;
    text-align: center;
  }
  .new-app-title {
    text-align: center;
  }
  .app-title{
    color: black;
  }
  .add-icon{
    margin-top: 30px;
    font-size: 80px;
  }
  .card-link {
    text-decoration: none;
  }
`

export default class Page_AccountPayment extends Component {

  render() {
    if (window.debugMode) console.log("Component Props: ", this.props);
    let formAttr = {
      id: "Form_AccountPayment",
      title: "Payment to Account",
      extraIds: [
        { prefix: 'accp', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
      ],
      searchKey: 'eid',//Key would be used to search for value from collection
      inputVarient: "standard",// 'standard', 'filled', 'outlined'
      collection: Collection_AccountPayment,
      chechForExistanceBeforeDeleteInCollections: [

      ],
      restrictUpdate: true,
      createSuccessMessage: "Payment added successfully",
      createErrorMessage: "Error while adding payment",
      updateSuccessMessage: "",
      updateErrorMessage: "",
      deleteSuccessMessage: "Payment deleted successfully",
      deleteErrorMessage: "Error while deleting payment",
      instantSearchConfig: {
        instantSearchLogic: {},
        searchStringDBKeys: ['eid'],
      },
      openEditDialogConfig: {
        detailedSearch: false,
        textChangeSearch: true,
        detailedSearchLogic: {},
        tableKeys: [
        ],
        detailedSearchBy: [
        ],
      },
      defaultEntries: [
      ],
      components: [
        {
          type: 'autocomplete',
          id: 'account',
          required: true,
          disabled: false,
          label: "Account",
          size: [12,12,12],//XS, MD, LG
          dataCollection: Collection_Accounts,
          dataLogic: {
            name: { $nin: ['Cash In Hand', 'Accounts Receivable', 'Inventory', 'Accounts Payable', 'Sales', 'Purchase', 'Discount On Sales', 'Cost Of Goods'] },
          },
          uniqueKey: 'eid',
          searchKeys: ['eid', 'name'],//Show specific keys in search
          data: [],// also Required if blank
          extraInformation: [
            {
                key: 'group_inputValue',
                label: 'Group',
            },
          ],
        },
        {
          type: 'textbox',
          id: 'note',
          required: false,
          disabled: false,
          label: "Note",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: false,
        },
        {
          type: 'textbox',
          id: 'amount',
          required: true,
          disabled: false,
          label: "Amount",
          size: [12,12,12],//XS, MD, LG
          password: false,
          readOnly: false,
          number: true,
          accountRelations: [
            {
              credit: { action: 'byAccountName', DBCollectionKey: 'Cash In Hand' },
              debit: { action: 'byDataKey', DBCollectionKey: 'account.id' },
              description: "byNoteEntry, Payment added using Account Payment",
            },
          ],
        },
      ],
    };

    return (
      <div>
        <Forms formAttr={formAttr} />
      </div>
    );
  }
}
