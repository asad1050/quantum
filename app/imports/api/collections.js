import { Mongo } from 'meteor/mongo';

export const Collection_Users = Meteor.users;
export const Apps = new Mongo.Collection('apps');
//--- Products
export const Collection_ProductCategories = new Mongo.Collection('productcategories');
export const Collection_ProductManufacturer = new Mongo.Collection('productmanufacturer');
export const Collection_ProductItems = new Mongo.Collection('productitems');

//--- Basic
export const Collection_Customers = new Mongo.Collection('customers');
export const Collection_Cities = new Mongo.Collection('cities');
export const Collection_Units = new Mongo.Collection('units');

//--- Inventory
export const Collection_Purchase = new Mongo.Collection('purchase');
export const Collection_PurchasePayment = new Mongo.Collection('purchasepayment');
export const Collection_PurchaseOrder = new Mongo.Collection('purchaseorder');
export const Collection_ReceivePurchaseOrder = new Mongo.Collection('receviepurchaseorder');
export const Collection_Suppliers = new Mongo.Collection('suppliers');

//--- Sales
export const Collection_Stockout = new Mongo.Collection('stockout');
export const Collection_GeneralSales = new Mongo.Collection('generalsales');

//---Settings
export const Collection_GeneralSettings = new Mongo.Collection('generalsettings');

//---Users
export const Collection_Roles = new Mongo.Collection('roles');

//--- Accounting
export const Collection_AccountGroups = new Mongo.Collection('accountgroups');
export const Collection_Accounts = new Mongo.Collection('accounts');
export const Collection_AccountCategories = new Mongo.Collection('accountcategories');
export const Collection_Transactions = new Mongo.Collection('transactions');
export const Collection_AccountReception = new Mongo.Collection('accountreception');
export const Collection_AccountPayment = new Mongo.Collection('accountpayment');

//--- Dental Lab specific
export const Collection_WorkTypes = new Mongo.Collection('worktypes');
export const Collection_CaseTypes = new Mongo.Collection('casetypes');
export const Collection_CaseShades = new Mongo.Collection('caseshades');
export const Collection_Cases = new Mongo.Collection('cases');

//--- Property Developer specific
export const Collection_PropertyPlots = new Mongo.Collection('propertyplots');
export const Collection_PropertySales = new Mongo.Collection('propertysales');
export const Collection_RecordPlotPayment = new Mongo.Collection('recordplotpayment');
export const Collection_PropertyPurchase = new Mongo.Collection('propertypurchase');
export const Collection_PlotReturn = new Mongo.Collection('plotreturn');
