import {
  Collection_ProductCategories,
  Apps,
  Collection_ProductItems,
  Collection_Suppliers,
  Collection_PurchaseOrder,
  Collection_ReceivePurchaseOrder,
  Collection_ProductManufacturer,
  Collection_Purchase,
  Collection_Customers,
  Collection_Cities,
  Collection_WorkTypes,
  Collection_AccountGroups,
  Collection_Accounts,
  Collection_Transactions,
  Collection_AccountCategories,
  Collection_PurchasePayment,
  Collection_Stockout,
  Collection_GeneralSettings,
  Collection_CaseTypes,
  Collection_CaseShades,
  Collection_Cases,
  Collection_AccountReception,
  Collection_AccountPayment,
  Collection_Roles,
  Collection_Users,
  Collection_GeneralSales,
  Collection_Units,
  Collection_PropertyPlots,
  Collection_PropertySales,
  Collection_RecordPlotPayment,
  Collection_PropertyPurchase,
  Collection_PlotReturn,
  } from './collections';

export default function collectionPointer(collectionName){
  switch (collectionName) {
    case Apps._name:
      return Apps;
      break;
    case Collection_ProductCategories._name:
      return Collection_ProductCategories;
      break;
    case Collection_ProductItems._name:
      return Collection_ProductItems;
      break;
    case Collection_Suppliers._name:
      return Collection_Suppliers;
      break;
    case Collection_PurchaseOrder._name:
      return Collection_PurchaseOrder;
      break;
    case Collection_ReceivePurchaseOrder._name:
      return Collection_ReceivePurchaseOrder;
      break;
    case Collection_ProductManufacturer._name:
      return Collection_ProductManufacturer;
      break;
    case Collection_Purchase._name:
      return Collection_Purchase;
      break;
    case Collection_Customers._name:
      return Collection_Customers;
      break;
    case Collection_Cities._name:
      return Collection_Cities;
      break;
    case Collection_WorkTypes._name:
      return Collection_WorkTypes;
      break;
    case Collection_AccountGroups._name:
      return Collection_AccountGroups;
      break;
    case Collection_Accounts._name:
      return Collection_Accounts;
      break;
    case Collection_Transactions._name:
      return Collection_Transactions;
    case Collection_AccountCategories._name:
      return Collection_AccountCategories;
      break;
    case Collection_PurchasePayment._name:
      return Collection_PurchasePayment;
      break;
    case Collection_Stockout._name:
      return Collection_Stockout;
      break;
    case Collection_GeneralSettings._name:
      return Collection_GeneralSettings;
      break;
    case Collection_CaseTypes._name:
      return Collection_CaseTypes;
      break;
    case Collection_CaseShades._name:
      return Collection_CaseShades;
      break;
    case Collection_Cases._name:
      return Collection_Cases;
      break;
    case Collection_AccountReception._name:
      return Collection_AccountReception;
      break;
    case Collection_AccountPayment._name:
      return Collection_AccountPayment;
      break;
    case Collection_Roles._name:
      return Collection_Roles;
      break;
    case Collection_Users._name:
      return Collection_Users;
      break;
    case Collection_GeneralSales._name:
      return Collection_GeneralSales;
      break;
    case Collection_Units._name:
      return Collection_Units;
      break;
    case Collection_PropertyPlots._name:
      return Collection_PropertyPlots;
      break;
    case Collection_PropertySales._name:
      return Collection_PropertySales;
      break;
    case Collection_RecordPlotPayment._name:
        return Collection_RecordPlotPayment;
        break;
    case Collection_PropertyPurchase._name:
        return Collection_PropertyPurchase;
        break;
    case Collection_PlotReturn._name:
        return Collection_PlotReturn;
        break;
      
    default:
      
  }
}
