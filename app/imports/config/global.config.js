import React from 'react';

import Page_DashboardCards from '../ui/pages/Page_DashboardCards';
import Page_ProductCategories from '../ui/uiCores/products/Page_ProductCategories';
import Page_ProductManufacturer from '../ui/uiCores/products/Page_ProductManufacturer';
import Page_ProductItems from '../ui/uiCores/products/Page_ProductItems';

import Page_Purchase from '../ui/uiCores/inventory/Page_Purchase';
import Page_PurchasePayment from '../ui/uiCores/inventory/Page_PurchasePayment';
import Page_PurchaseOrder from '../ui/uiCores/inventory/Page_PurchaseOrder';
import Page_ReceivePurchaseOrder from '../ui/uiCores/inventory/Page_ReceivePurchaseOrder';
import Page_Suppliers from '../ui/uiCores/inventory/Page_Suppliers';

//--- Sales
import Page_Stockout from '../ui/uiCores/sales/Page_Stockout';
import Page_GeneralSales from '../ui/uiCores/sales/Page_GeneralSales';

import Page_Customers from '../ui/uiCores/basic/Page_Customers';
import Page_Units from '../ui/uiCores/basic/Page_Units';

import Page_GeneralSettings from '../ui/uiCores/settings/Page_GeneralSettings';
import Page_Cities from '../ui/uiCores/settings/Page_Cities';

import Page_Roles from '../ui/uiCores/users/Page_Roles';
import Page_Users from '../ui/uiCores/users/Page_Users';
import Page_ChangePassword from '../ui/uiCores/users/Page_ChangePassword';

import Page_Accounts from '../ui/uiCores/accounts/Page_Accounts';
import Page_AccountCategories from '../ui/uiCores/accounts/Page_AccountCategories';
import Page_AccountReception from '../ui/uiCores/accounts/Page_AccountReception';
import Page_AccountPayment from '../ui/uiCores/accounts/Page_AccountPayment';

import Page_AccountLedger from '../ui/uiCores/reports/Page_AccountLedger';
import Page_IncomeStatement from '../ui/uiCores/reports/Page_IncomeStatement';
import Page_StockReport from '../ui/uiCores/reports/Page_StockReport';
import Page_BillingReport from '../ui/uiCores/reports/Page_BillingReport';


//--- Dental Lab Spcific
import Page_WorkTypes from '../ui/uiCores/dentallab/Page_WorkTypes';
import Page_CaseTypes from '../ui/uiCores/dentallab/Page_CaseTypes';
import Page_CaseShades from '../ui/uiCores/dentallab/Page_CaseShades';
import Page_Cases from '../ui/uiCores/dentallab/Page_Cases';
import Page_CasePayment from '../ui/uiCores/dentallab/Page_CasePayment';

//--- Property Developer specific
import Page_PropertyPlots from '../ui/uiCores/propertydeveloper/Page_PropertyPlots';
import Page_PropertySale from '../ui/uiCores/propertydeveloper/Page_PropertySale';
import Page_RecordPlotPayment from '../ui/uiCores/propertydeveloper/Page_RecordPlotPayment';
import Page_PropertyPurchase from '../ui/uiCores/propertydeveloper/Page_PropertyPurchase';
import Page_PlotReturn from '../ui/uiCores/propertydeveloper/Page_PlotReturn';

import {
  FaDatabase, FaColumns, FaListAlt, FaUsers, FaCogs, FaComment, FaBoxes, FaTh, FaBox,
  FaWarehouse, FaTruckLoading, FaDolly, FaToolbox, FaCartPlus, FaCodeBranch,
  FaNetworkWired, FaTooth, FaFileInvoiceDollar, FaIdCard, FaMoneyCheckAlt, FaCashRegister,
  FaOpencart, FaTools, FaGripHorizontal, FaTeethOpen, FaCubes, FaArchive, FaLayerGroup,
  FaPlusCircle, FaMinusCircle, FaFolder, FaHandHoldingUsd, FaUsersCog, FaUserFriends, FaMapMarkedAlt, FaEdit, FaCity,
  FaCheckCircle, FaMoneyBillWave, FaExternalLinkAlt,
 } from 'react-icons/fa';
import { MdDashboard } from "react-icons/md";

export default GlobalConfig = function () {
  //--- List of softwares
  let softwareList = {
    dentalLabMS: 'dentallabmanagementsystem',
    saleAndInventory: 'salesandinventory',
    propertyDeveloperMS: 'propertydeveloperms',
  };

  let software = sessionStorage.getItem("apptype");
  let plan = sessionStorage.getItem("appplan");
  
  
  //--- default config
  let config = {
    pages: [
      {
        name: 'Dashboard',
        id:'dashboard_menu',
        url: '/dashboard',
        menu: { text: 'Dashboard', icon: <MdDashboard /> },
        component: <Page_DashboardCards />,
        privateRoute: true,
        dashboard: true,
  
      },
    ],
  };

  //--- Changes depending upon software
  if (software == softwareList.dentalLabMS){
    //--- Appname
    config.appName = 'Quantum Dental Lab Management System';
    //--- Adding baisc dental lab pages
    config.pages.push({
      subPages: [
        {
          name: 'Cases',
          id:'cases_submenu',
          url: '/cases',
          menu: { text: 'Cases', icon: <FaArchive /> },
          component: <Page_Cases />,
          privateRoute: true,
          dashboard: true,
          type: 'form',
        },
        {
          name: 'CasePayment',
          id:'casepayment_submenu',
          url: '/casepayment',
          menu: { text: 'Payment', icon: <FaMoneyCheckAlt /> },
          component: <Page_CasePayment />,
          privateRoute: true,
          dashboard: true,
          type: 'save',
        },
        {
          name: 'CaseTypes',
          id:'casetypes_submenu',
          url: '/casetypes',
          menu: { text: 'Case Types', icon: <FaGripHorizontal /> },
          component: <Page_CaseTypes />,
          privateRoute: true,
          dashboard: true,
          type: 'form',
        },
        {
          name: 'CaseShades',
          id:'caseshades_submenu',
          url: '/caseshades',
          menu: { text: 'Case Shades', icon: <FaTeethOpen /> },
          component: <Page_CaseShades />,
          privateRoute: true,
          dashboard: true,
          type: 'form',
        },
        {
          name: 'WorkTypes',
          id:'worktypes_submenu',
          url: '/worktypes',
          menu: { text: 'Work Types', icon: <FaLayerGroup /> },
          component: <Page_WorkTypes />,
          privateRoute: true,
          dashboard: true,
          type: 'form',
        },
      ],
      name: 'DentalLab',
      id:'dentallaboptions_menu',
      url: '',
      menu: { text: 'Dental Lab', icon: <FaTooth /> },
      privateRoute: true,
      dashboard: true,

    });
    //--- Adding stockout page
    config.pages.push({
      name: 'stockout',
      id:'stockout_menu',
      url: '/stockout',
      menu: { text: 'Stock Out', icon: <FaOpencart /> },
      component: <Page_Stockout />,
      privateRoute: true,
      dashboard: true,
      type: 'save',
    },
    {
        subPages: [
          {
            name: 'Categories',
            id:'productcategories_submenu',
            url: '/productcategories',
            menu: { text: 'Categories', icon: <FaTh /> },
            component: <Page_ProductCategories />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'Manufacturer',
            id:'productmanufacturer_submenu',
            url: '/productmanufacturer',
            menu: { text: 'Manufacturer', icon: <FaToolbox /> },
            component: <Page_ProductManufacturer />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'Items',
            id:'productitems_submenu',
            url: '/productitems',
            menu: { text: 'Items', icon: <FaBox /> },
            component: <Page_ProductItems />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
        ],
        name: 'Products',
        id:'productsmanagement_menu',
        url: '',
        menu: { text: 'Products', icon: <FaBoxes /> },
        privateRoute: true,
        dashboard: true,
    
      },{
        subPages: [
          {
            name: 'Purchase',
            id:'purchaseorder_submenu',
            url: '/purchase',
            menu: { text: 'Purchase', icon: <FaCartPlus /> },
            component: <Page_Purchase />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'Purchase Payment',
            id:'purchasepayment_submenu',
            url: '/purchasepayment',
            menu: { text: 'Payment', icon: <FaMoneyCheckAlt /> },
            component: <Page_PurchasePayment />,
            privateRoute: true,
            dashboard: true,
            type: 'save',
          },
        ],
        name: 'Inventory',
        id:'inventorymanagement_menu',
        url: '',
        menu: { text: 'Inventory', icon: <FaWarehouse /> },
        privateRoute: true,
        dashboard: true,
    
      },
      {
        subPages: [
          {
            name: 'IncomeStatement',
            id:'incomestatement_submenu',
            url: '/incomestatement',
            menu: { text: 'Income Statement', icon: <FaHandHoldingUsd /> },
            component: <Page_IncomeStatement />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
          {
            name: 'StockReport',
            id:'stockreport_submenu',
            url: '/stockreport',
            menu: { text: 'Stock', icon: <FaBoxes /> },
            component: <Page_StockReport />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
          {
            name: 'BillingReport',
            id:'billingreport_submenu',
            url: '/billingreport',
            menu: { text: 'Billing', icon: <FaFileInvoiceDollar /> },
            component: <Page_BillingReport />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
        ],
        name: 'Reports',
        id:'reports_menu',
        url: '',
        menu: { text: 'Reports', icon: <FaFolder /> },
        privateRoute: true,
        dashboard: true,
    
      },
      );
  }else if (software == softwareList.saleAndInventory){
    //--- Appname
    config.appName = 'Quantum Sale and Inventory Manager';
    //--- Adding Sales Pages
    config.pages.push({
      subPages: [
        {
          name: 'GeneralSales',
          id:'generalsales_submenu',
          url: '/generalsales',
          menu: { text: 'General Sales', icon: <FaCashRegister /> },
          component: <Page_GeneralSales />,
          privateRoute: true,
          dashboard: true,
          type: 'form',
        },
        
      ],
      name: 'Sales',
      id:'sales_menu',
      url: '',
      menu: { text: 'Sales', icon: <FaOpencart /> },
      privateRoute: true,
      dashboard: true,

    },{
        subPages: [
          {
            name: 'Categories',
            id:'productcategories_submenu',
            url: '/productcategories',
            menu: { text: 'Categories', icon: <FaTh /> },
            component: <Page_ProductCategories />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'Manufacturer',
            id:'productmanufacturer_submenu',
            url: '/productmanufacturer',
            menu: { text: 'Manufacturer', icon: <FaToolbox /> },
            component: <Page_ProductManufacturer />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'Items',
            id:'productitems_submenu',
            url: '/productitems',
            menu: { text: 'Items', icon: <FaBox /> },
            component: <Page_ProductItems />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
        ],
        name: 'Products',
        id:'productsmanagement_menu',
        url: '',
        menu: { text: 'Products', icon: <FaBoxes /> },
        privateRoute: true,
        dashboard: true,
    
      },{
        subPages: [
          {
            name: 'Purchase',
            id:'purchaseorder_submenu',
            url: '/purchase',
            menu: { text: 'Purchase', icon: <FaCartPlus /> },
            component: <Page_Purchase />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'Purchase Payment',
            id:'purchasepayment_submenu',
            url: '/purchasepayment',
            menu: { text: 'Payment', icon: <FaMoneyCheckAlt /> },
            component: <Page_PurchasePayment />,
            privateRoute: true,
            dashboard: true,
            type: 'save',
          },
        ],
        name: 'Inventory',
        id:'inventorymanagement_menu',
        url: '',
        menu: { text: 'Inventory', icon: <FaWarehouse /> },
        privateRoute: true,
        dashboard: true,
    
      },
      {
        subPages: [
          {
            name: 'IncomeStatement',
            id:'incomestatement_submenu',
            url: '/incomestatement',
            menu: { text: 'Income Statement', icon: <FaHandHoldingUsd /> },
            component: <Page_IncomeStatement />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
          {
            name: 'StockReport',
            id:'stockreport_submenu',
            url: '/stockreport',
            menu: { text: 'Stock', icon: <FaBoxes /> },
            component: <Page_StockReport />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
          {
            name: 'BillingReport',
            id:'billingreport_submenu',
            url: '/billingreport',
            menu: { text: 'Billing', icon: <FaFileInvoiceDollar /> },
            component: <Page_BillingReport />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
        ],
        name: 'Reports',
        id:'reports_menu',
        url: '',
        menu: { text: 'Reports', icon: <FaFolder /> },
        privateRoute: true,
        dashboard: true,
    
      },
      );
  }else if (software == softwareList.propertyDeveloperMS){
    //--- Appname
    config.appName = 'Quantum Property Developers MS';
    //--- Adding Units Page
    config.pages.push(
    {
        name: 'Units',
        id:'units_menu',
        url: '/units',
        menu: { text: 'Units', icon: <FaCodeBranch /> },
        component: <Page_Units />,
        privateRoute: true,
        dashboard: true,
        adminAccessOnly: true,
        type: 'form',
    },
    );
    //--- adding basic property developer functions
    config.pages.push({
        subPages: [
          {
            name: 'PropertyPlots',
            id:'propertyplots_submenu',
            url: '/propertyplots',
            menu: { text: 'Plots', icon: <FaTh /> },
            component: <Page_PropertyPlots />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'PropertySale',
            id:'propertysale_submenu',
            url: '/propertysale',
            menu: { text: 'Booking/Sale', icon: <FaCheckCircle /> },
            component: <Page_PropertySale />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'RecordPlotPayment',
            id:'recordplotpayment_submenu',
            url: '/recordplotpayment',
            menu: { text: 'Record Payment', icon: <FaMoneyBillWave /> },
            component: <Page_RecordPlotPayment />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'PlotReturn',
            id:'plotreturn_submenu',
            url: '/plotreturn',
            menu: { text: 'Plot Return', icon: <FaExternalLinkAlt /> },
            component: <Page_PlotReturn />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
          {
            name: 'PropertyPurchase',
            id:'propertypurchase_submenu',
            url: '/propertypurchase',
            menu: { text: 'Purchase', icon: <FaMoneyCheckAlt /> },
            component: <Page_PropertyPurchase />,
            privateRoute: true,
            dashboard: true,
            type: 'form',
          },
        ],
        name: 'Real Estate',
        id:'realestate_menu',
        url: '',
        menu: { text: 'Real Estate', icon: <FaCity /> },
        privateRoute: true,
        dashboard: true,
  
      },
      {
        subPages: [
          {
            name: 'IncomeStatement',
            id:'incomestatement_submenu',
            url: '/incomestatement',
            menu: { text: 'Income Statement', icon: <FaHandHoldingUsd /> },
            component: <Page_IncomeStatement />,
            privateRoute: true,
            dashboard: true,
            type: 'report',
          },
        ],
        name: 'Reports',
        id:'reports_menu',
        url: '',
        menu: { text: 'Reports', icon: <FaFolder /> },
        privateRoute: true,
        dashboard: true,
    
      },);
  }


  //--- Adding basic pages
  config.pages.push(
  
  {
    subPages: [
      {
        name: 'Accounts',
        id:'accounts_submenu',
        url: '/accounts',
        menu: { text: 'Accounts', icon: <FaIdCard /> },
        component: <Page_Accounts />,
        privateRoute: true,
        dashboard: true,
        type: 'form',
      },
      {
        name: 'AccountCategories',
        id:'accountscategories_submenu',
        url: '/accountscategories',
        menu: { text: 'Categories', icon: <FaTh /> },
        component: <Page_AccountCategories />,
        privateRoute: true,
        dashboard: true,
        type: 'form',
      },
      {
        name: 'AccountReception',
        id:'accountreception_submenu',
        url: '/accountreception',
        menu: { text: 'Receive', icon: <FaPlusCircle /> },
        component: <Page_AccountReception />,
        privateRoute: true,
        dashboard: true,
        type: 'save',
      },
      {
        name: 'AccountPayment',
        id:'accountpayment_submenu',
        url: '/accountpayment',
        menu: { text: 'Pay', icon: <FaMinusCircle /> },
        component: <Page_AccountPayment />,
        privateRoute: true,
        dashboard: true,
        type: 'save',
      },
      {
        name: 'AccountLedger',
        id:'accountledger_submenu',
        url: '/accountledger',
        menu: { text: 'Ledger', icon: <FaFileInvoiceDollar /> },
        component: <Page_AccountLedger />,
        privateRoute: true,
        dashboard: true,
        type: 'report',
      },
    ],
    name: 'Accounts',
    id:'accountsmanagement_menu',
    url: '',
    menu: { text: 'Accounts', icon: <FaFileInvoiceDollar /> },
    privateRoute: true,
    dashboard: true,

  },
  {
    name: 'Customers',
    id:'customers_menu',
    url: '/customers',
    menu: { text: 'Customers', icon: <FaUsers /> },
    component: <Page_Customers />,
    privateRoute: true,
    dashboard: true,
    type: 'form',

  },
  {
    name: 'Suppliers',
    id:'suppliers_menu',
    url: '/suppliers',
    menu: { text: 'Suppliers', icon: <FaTruckLoading /> },
    component: <Page_Suppliers />,
    privateRoute: true,
    dashboard: true,
    type: 'form',

  },
  
  {
    subPages: [
      {
        name: 'Roles',
        id:'roles_submenu',
        url: '/roles',
        menu: { text: 'Roles', icon: <FaUsersCog /> },
        component: <Page_Roles />,
        privateRoute: true,
        dashboard: true,
        type: 'form',
      },
      {
        name: 'Users',
        id:'users_submenu',
        url: '/users',
        menu: { text: 'Users', icon: <FaUserFriends /> },
        component: <Page_Users />,
        privateRoute: true,
        dashboard: true,
        type: 'form',
      },
      {
        name: 'ChangePassword',
        id:'changepassword_submenu',
        url: '/changepassword',
        menu: { text: 'ChangePassword', icon: <FaEdit /> },
        component: <Page_ChangePassword />,
        privateRoute: true,
        dashboard: true,
        type: 'form',
      },
    ],
    name: 'Users',
    id:'users_menu',
    url: '',
    menu: { text: 'Users', icon: <FaUsers /> },
    privateRoute: true,
    dashboard: true,

  },
  {
    subPages: [
      {
        name: 'GeneralSettings',
        id:'generalsettings_submenu',
        url: '/generalsettings',
        menu: { text: 'General', icon: <FaTools /> },
        component: <Page_GeneralSettings />,
        privateRoute: true,
        dashboard: true,
        type: 'save',
      },
      {
        name: 'Cities',
        id:'cities_submenu',
        url: '/cities',
        menu: { text: 'Cities', icon: <FaMapMarkedAlt /> },
        component: <Page_Cities />,
        privateRoute: true,
        dashboard: true,
        type: 'form',
      },
    ],
    name: 'Settings',
    id:'settings_menu',
    url: '',
    menu: { text: 'Settings', icon: <FaCogs /> },
    privateRoute: true,
    dashboard: true,

  });


  return config;
}
