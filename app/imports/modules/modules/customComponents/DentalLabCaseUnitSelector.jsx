import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Autocomplete from 'react-autocomplete';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';


import { FaBuromobelexperte } from 'react-icons/fa';


const DentalLabCaseUnitSelectorStyle = styled.div`
    .containerStyle{
        min-width: 320px;
    }
`

export default class DentalLabCaseUnitSelector extends Component {

    constructor(props){
        super(props)

        this.units = {
            left: [
                11,12,13,14,15,16,17,18,48,47,46,45,44,43,42,41
            ],
            right: [
                21,22,23,24,25,26,27,28,38,37,36,35,34,33,32,31
            ],
        };
        this.unitMargins = {
            left: [
                '73%','53%','39%','24%','12%','4%','0%','0%','0%','0%','4%','12%','24%','39%','53%','73%',
            ],
            right: [
                '0%','20%','35%','49%','60%','68%','73%','73%','73%','73%','68%','60%','49%','35%','20%','0%',
            ],
        };

        this.state = {
            selectorOpened: false,
        };

        this.units.left.map((unit, key) => {
            this.state[unit] = false;
        });
        this.units.right.map((unit, key) => {
            this.state[unit] = false;
        });
    }

    handleCheckboxChange(event){
        let target = event.currentTarget;
        let id = $(target).prop('id');
        id = id.replace("unit", "");
        
        this.setState({[id]: !this.state[id]});
      }
    
    handleSelectorClose(){
        this.setState({ selectorOpened: false });
    }

    handleSelectorOpen(){
        this.units.left.map((unit, key) => {
            this.setState({ [unit]: false });
        });
        this.units.right.map((unit, key) => {
            this.setState({ [unit]: false });
        });
        this.setState({ selectorOpened: true });
    }

    handleSelectorOk(){
        var data = [];
        this.units.left.map((unit, key) => {
            if (this.state[unit] == true){
                data.push(unit);
            }
        });
        this.units.right.map((unit, key) => {
            if (this.state[unit] == true){
                data.push(unit);
            }
        });
        this.props.onOk(data, this.props.form);
        this.setState({ selectorOpened: false });
    }

    render() {

        const units = this.units;
        const unitMargins = this.unitMargins;
        

        return (
        <DentalLabCaseUnitSelectorStyle>
            <Button variant="outlined" fullWidth color="primary" onClick={this.handleSelectorOpen.bind(this)}>
                Open Unit selector
            </Button>
            <Dialog
                open={this.state.selectorOpened}
                onClose={this.handleSelectorClose.bind(this)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Select Units for Case"}</DialogTitle>
                <DialogContent style={{ minWidth: '330px' }} >
                  <Grid container>
                        <Grid item xs={6} md={6} lg={6} >
                            {units.left.map((unit, key) => {
                                if (unit == 18) {
                                    return (
                                        <div style={{ paddingLeft: unitMargins.left[key] }} key={key} >
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                    checked={this.state[unit]}
                                                    id={"unit" + unit}
                                                    color={"primary"}
                                                    style={{ padding: 0 }}
                                                    onChange={this.handleCheckboxChange.bind(this)}
                                                />
                                                }
                                                label={unit}
                                            />
                                            
                                            <br/>
                                            <br/>
                                            <Divider/>
                                            <br/>
                                        </div>
                                    );
                                } else {
                                    return (
                                        <div style={{ paddingLeft: unitMargins.left[key] }} key={key} >
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                    checked={this.state[unit]}
                                                    id={"unit" + unit}
                                                    color={"primary"}
                                                    style={{ padding: 0 }}
                                                    onChange={this.handleCheckboxChange.bind(this)}
                                                />
                                                }
                                                label={unit}
                                            />
                                            
                                            <br/>
                                        </div>
                                    );
                                }
                            })}
                            
                            
                        </Grid>
                        <Grid item xs={6} md={6} lg={6} >
                            {units.right.map((unit, key) => {
                                if (unit == 28) {
                                    return (
                                        <div>
                                        <div style={{ paddingLeft: unitMargins.right[key] }} key={key} >
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                    checked={false}
                                                    id={"unit" + unit}
                                                    checked={this.state[unit]}
                                                    color={"primary"}
                                                    style={{ padding: 0 }}
                                                    onChange={this.handleCheckboxChange.bind(this)}
                                                />
                                                }
                                                label={unit}
                                            />
                                            
                                            <br/>
                                        </div>
                                        <div>
                                        <br/>
                                            <Divider/>
                                            <br/>
                                        </div>
                                        </div>
                                    );
                                } else {
                                    return (
                                        <div style={{ paddingLeft: unitMargins.right[key] }} key={key} >
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                    checked={false}
                                                    id={"unit" + unit}
                                                    checked={this.state[unit]}
                                                    color={"primary"}
                                                    style={{ padding: 0 }}
                                                    onChange={this.handleCheckboxChange.bind(this)}
                                                />
                                                }
                                                label={unit}
                                            />
                                            
                                            <br/>
                                        </div>
                                    );
                                }
                            })}
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                <Button onClick={this.handleSelectorClose.bind(this)} color="primary">
                    Close
                </Button>
                <Button onClick={this.handleSelectorOk.bind(this)} color="primary" autoFocus>
                    Ok
                </Button>
                </DialogActions>
            </Dialog>
        </DentalLabCaseUnitSelectorStyle>
        );
    }
}
