import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Autocomplete from 'react-autocomplete';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import InputBase from '@material-ui/core/InputBase';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';


import { FaTrash } from 'react-icons/fa';


const SearchDialogStyles = styled.div`
.single-row-table-paper {
  overflow: auto;
}
.multi-row-table-paper {
  overflow: auto;
}
.container-paper{
  margin: 10px;
}
.container{
  padding: 16px 16px !important;
}
.grid-item-search{
  padding: 0px 0px 8px 8px;
}
.editByIdInput{
  width: 74%;
}
.rightButton{
  width: 74%;
}
`

class SearchDialogContainer extends Component {

  constructor(props){
		super(props)
		this.state = {
      open: false,
    };
    let components = this.props.config.detailedSearchBy;
    components.forEach(function (component) {
      if (component.type == 'checkbox'){
        this.state[component.id + '_search'] = false;
      }else if (component.type == 'datebox'){
        if (component.dateRange){
          this.state[component.id + '_search_datefrom'] = new Date();
          const today = new Date()
          const tomorrow = new Date(today)
          tomorrow.setDate(tomorrow.getDate() + 1)
          this.state[component.id + '_search_dateto'] = tomorrow;
        }else{
          this.state[component.id + '_search'] = new Date();
        }
      }else if (component.type == 'selectbox'){
        if (typeof component.dataCollection !== 'undefined'){
          this.state[component.id + '_search'] = "";
        }else{
          if (component.data.length > 0){
            this.state[component.id + '_search'] = component.data[0].value;
          }else{
            this.state[component.id + '_search'] = "";
          }
        }
      }else if (component.type == 'autocomplete'){
        this.state[component.id + '_search'] = {};
        this.state[component.id + '_search_inputValue'] = "";
      }
    }, this);
	}

  handleCheckboxChange(event){
    let target = event.currentTarget;
    let id = $(target).prop('id');
    this.setState({[id]: !this.state[id]});
  }

  handleDateboxChange(id, date){
    $('#' + id).parent().removeClass('Mui-error');
    this.setState({[id]: date});
  }

  handleSelectboxChange(id, event){
    this.setState({[id]: event.target.value});
  }

  handleEditInputKeyUp(event){
    let keyCode = (event.keyCode ? event.keyCode : event.which);
    let target = event.currentTarget;
    if (keyCode == '13'){
      
    }
  }

  handleSearch(event){
    let self = this;
    let components = this.props.config.detailedSearchBy;
    let collection = this.props.collection;

    //Create Data Scheme
    let dataScheme = {};
    components.forEach(function (component) {
      if (component.type == 'checkbox'){
        dataScheme[component.id] = this.state[component.id + '_search'];
      }else if (component.type == 'selectbox'){
        if ($('#' + component.id + '_search').val() && $('#' + component.id + '_search').val().trim() !== ''){
          if (component.dataCollection){
            dataScheme[component.id] = this.state[component.id + '_search'];
            dataScheme[component.id + '_collection'] = component.dataCollection._name;
          }else{
            dataScheme[component.id] = this.state[component.id + '_search'];
          }
        }
      }else if (component.type == 'autocomplete'){
        if (Object.keys(this.state[component.id + '_search']).length !== 0){
          if (this.state[component.id + '_search']._id){
            dataScheme[component.id + '.id'] = this.state[component.id + '_search']._id;
          }else{
            dataScheme[component.id + '.data.name'] = this.state[component.id + '_search'].name;
          }
        }
      }else if (component.type == 'datebox'){
        if (component.dateRange){
          if ($('#' + component.id + '_search_datefrom').val() && $('#' + component.id + '_search_datefrom').val().trim() !== '' && $('#' + component.id + '_search_dateto').val() && $('#' + component.id + '_search_dateto').val().trim() !== ''){
            let datefrom = $('#' + component.id + '_search_datefrom').val().trim();
            let dateto = $('#' + component.id + '_search_dateto').val().trim();
            datefrom = datefrom.split('/');
            datefrom = datefrom[1] + '/' + datefrom[0] + '/' + datefrom[2];
            datefrom = new Date(datefrom).toISOString();
            dateto = dateto.split('/');
            dateto = dateto[1] + '/' + dateto[0] + '/' + dateto[2];
            dateto = new Date(dateto).toISOString();
            dataScheme[component.id] = { $gte : datefrom, $lt: dateto }
          }
        }else{
          if ($('#' + component.id + '_search').val() && $('#' + component.id + '_search').val().trim() !== ''){
            let date = $('#' + component.id + '_search').val().trim();
            date = date.split('/');
            date = date[1] + '/' + date[0] + '/' + date[2];
            date = new Date(date).toISOString();
            dataScheme[component.id] = date;
          }
        }
      }else{
        if ($('#' + component.id + '_search').val() && $('#' + component.id + '_search').val().trim() !== ''){
          dataScheme[component.id] = $('#' + component.id + '_search').val().trim();
        }
      }
    }, this);
    this.props.handleChangeSearchLogic(dataScheme, this.props.parent);
    this.initiateTableEvents();

  }

  initiateTableEvents(){
    let self = this;
    setTimeout(function () {
      //Row Click Event
      let tableId = '#' + self.props.collection._name + '_search_table';
      $('#' + self.props.collection._name + '_search_table').find('.tableRow').unbind().click(function (event) {
        let target = event.target;
        if ($('tableRow').hasClass('tableRow') == false) target = $(target).parent();
        if ($(target).hasClass('Mui-selected')){
          //Select Entry
          let selectedData = $(target).data('entry');
          self.props.handleDataSelection(selectedData, self.props.parent);
          self.props.handleCloseEditDialogByRef(self.props.parent);
          self.props.handleChangeSearchLogic({}, self.props.parent);
        }else{
          $(tableId).find('.tableRow').removeClass('Mui-selected');
          $(target).addClass('Mui-selected');
        }
      });
      //Keyboard events
      $(window).unbind().on('keydown', function (event) {
        if (event.key == 'ArrowDown'){
          event.preventDefault();
          let table = $(tableId);
          let rows = table.find('.tableRow');
          let selectedRowIndex = 0;
          for (var i=0;i<rows.length;i++){
            if ($(rows[i]).hasClass('Mui-selected')){
              selectedRowIndex = i;
            }
          }
          if ((selectedRowIndex + 1) < rows.length){
            $(rows).removeClass('Mui-selected');
            $(rows[selectedRowIndex + 1]).addClass('Mui-selected');
          }
        }else if(event.key == 'ArrowUp'){
          event.preventDefault();
          let table = $(tableId);
          let rows = table.find('.tableRow');
          let selectedRowIndex = 0;
          for (var i=0;i<rows.length;i++){
            if ($(rows[i]).hasClass('Mui-selected')){
              selectedRowIndex = i;
            }
          }
          if ((selectedRowIndex - 1) > -1){
            $(rows).removeClass('Mui-selected');
            $(rows[selectedRowIndex - 1]).addClass('Mui-selected');
          }
        }else if(event.key == 'Enter'){
          event.preventDefault();
          self.handleTableSelection();
        }
      });
    }, 1000);
  }

  componentDidMount(){

  }

  getTextboxType(component){
    if (component.password){
      return 'password';
    }else if(component.number){
      return 'number';
    }else{
      return 'text';
    }
  }

  formatDate(date){
    if (date){
      return (new Date(date).toDateString());
    }else{
      return '';
    }

  }

  handleTableSelection(){
    swal.fire({
      title: 'Filling Data into Form',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        this.props.handleCloseEditDialogByRef(this.props.parent);
        swal.showLoading();
      },
    });
    let tableId = '#' + this.props.collection._name + '_search_table';
    let selectedData = $(tableId).find('.tableRow.Mui-selected').data('entry');
    this.props.handleDataSelection(selectedData, this.props.parent);
    
    this.props.handleChangeSearchLogic({}, this.props.parent);
    
  }


  render() {

    return (
      <SearchDialogStyles>
        <Dialog open={this.props.open} onClose={this.props.handleCloseEditDialogByRef.bind(this.props.parent)} aria-labelledby="form-dialog-title">
          <DialogTitle>Search {this.props.collection._name}</DialogTitle>
          <DialogContent>
            <Grid container spacing={2} >
              {this.props.config.detailedSearchBy.map(function (component, parentKey) {
                if (component.type == 'textbox'){
                  return(
                    <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                        <TextField
                          id={component.id + '_search'}
                          label={component.label}
                          disabled={component.disabled}
                          type={this.getTextboxType(component)}
                          margin="normal"
                          fullWidth
                          InputProps={{
                            readOnly: component.readOnly ? true : false,
                          }}
                          variant={this.props.inputVarient}
                        />

                    </Grid>
                  );
                }else if (component.type == 'checkbox'){
                  return(
                    <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                      <FormGroup row>
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={this.state[component.id + '_search']}
                              onChange={this.handleCheckboxChange.bind(this)}
                              id={component.id + '_search'}
                              disabled={component.disabled}
                              value={component.id + '_search'}
                              color={component.color}
                            />
                          }
                          label={component.label}
                        />
                      </FormGroup>
                    </Grid>
                  );
                }else if (component.type == 'datebox'){
                  if (component.dateRange){
                    return(
                      <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                        <Grid container spacing={2}>
                          <Grid item xs={6} md={6} lg={6} className="grid-item-search">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                              <Grid container justify="space-around">
                                <KeyboardDatePicker
                                  variant="inline"
                                  format={component.format}
                                  disabled={component.disabled}
                                  margin="normal"
                                  fullWidth
                                  id={component.id + '_search_datefrom'}
                                  label={component.label + ' from'}
                                  value={this.state[component.id + '_search_datefrom']}
                                  onChange={this.handleDateboxChange.bind(this, component.id + '_search_datefrom')}
                                  KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                  }}
                                  variant={this.props.inputVarient}
                                />
                              </Grid>
                            </MuiPickersUtilsProvider>
                          </Grid>
                          <Grid item xs={6} md={6} lg={6} className="grid-item-search">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                              <Grid container justify="space-around">
                                <KeyboardDatePicker
                                  variant="inline"
                                  format={component.format}
                                  disabled={component.disabled}
                                  margin="normal"
                                  fullWidth
                                  id={component.id + '_search_dateto'}
                                  label={component.label + ' to'}
                                  value={this.state[component.id + '_search_dateto']}
                                  onChange={this.handleDateboxChange.bind(this, component.id + '_search_dateto')}
                                  KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                  }}
                                  variant={this.props.inputVarient}
                                />
                              </Grid>
                            </MuiPickersUtilsProvider>
                          </Grid>
                        </Grid>
                      </Grid>
                    );
                  }else{
                    return(
                      <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justify="space-around">
                          <KeyboardDatePicker
                            variant="inline"
                            format={component.format}
                            disabled={component.disabled}
                            margin="normal"
                            fullWidth
                            id={component.id + '_search'}
                            label={component.label}
                            value={this.state[component.id + '_search']}
                            onChange={this.handleDateboxChange.bind(this, component.id + '_search')}
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                            variant={this.props.inputVarient}
                          />
                        </Grid>
                      </MuiPickersUtilsProvider>
                      </Grid>
                    );
                  }
                }else if (component.type == 'selectbox'){
                  return(
                    <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                      <FormControl fullWidth variant={this.props.inputVarient} disabled={component.disabled}>
                        <InputLabel id={component.id + '_search' + '-label'}>{component.label}</InputLabel>
                        <Select
                          labelid={component.id + '_search' + '-label'}
                          id={component.id + '_search'}
                          value={this.state[component.id + '_search']}
                          onChange={this.handleSelectboxChange.bind(this, component.id + '_search')}
                          fullWidth
                        >
                          { (typeof component.dataCollection !== 'undefined' && this.props.componentData[component.id + '_search']) ?
                            this.props.componentData[component.id + '_search'].map(function (entity, key) {
                              return (
                                <MenuItem value={entity[component.dataCollectionValue]} key={key} >{entity[component.dataCollectionLabel]}</MenuItem>
                              );
                            }, this)
                            :
                            component.data.map(function (entity, key) {
                            return (
                              <MenuItem value={entity.value} key={key} >{entity.label}</MenuItem>
                            );
                          }, this)}
                        </Select>
                      </FormControl>
                    </Grid>
                  );
                }else if (component.type == 'autocomplete'){
                  return(
                    <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                      <Typography variant="caption" display="block" gutterBottom>
                        {component.label}
                      </Typography>
                      <Autocomplete
                        id={component.id}
                        items={(typeof component.dataCollection !== 'undefined') ? this.props.componentData[component.id] : component.data }
                        shouldItemRender={(item, value) => {
                          let searchStringDBKeys = component.searchKeys;
                          let searchString = "";
                          for (var i=0;i<searchStringDBKeys.length;i++){
                            if (i!== 0) searchString += " ";
                            searchString += item[searchStringDBKeys[i]];
                          }
                          return (
                            searchString.toLowerCase().indexOf(value.toLowerCase()) > -1
                          );
                        }}
                        getItemValue={item => {
                          let searchStringDBKeys = component.searchKeys;
                          let searchString = "";
                          for (var i=0;i<searchStringDBKeys.length;i++){
                            if (i!== 0) searchString += " ";
                            searchString += item[searchStringDBKeys[i]];
                          }
                          return searchString;
                        }}
                        renderItem={(item, highlighted) => {
                          let searchStringDBKeys = component.searchKeys;
                          let searchString = "";
                          for (var i=0;i<searchStringDBKeys.length;i++){
                            if (i!== 0) searchString += " ";
                            searchString += item[searchStringDBKeys[i]];
                          }
                          return (
                            <MenuItem
                              key={item[component.uniqueKey]}
                              style={{ backgroundColor: highlighted ? '#eee' : 'white'}}
                            >
                            {searchString}
                            </MenuItem>
                          );
                        }
                        }
                        value={this.state[component.id + '_search_inputValue']}
                        onChange={e => this.setState({[component.id + '_search_inputValue']: event.target.value})}
                        onSelect={(value, item) => {
                          let data = item;
                          this.setState({[component.id + '_search']: item});
                          this.setState({[component.id + '_search_inputValue']: value});
                        }}
                        renderInput={params =>{
                          return <input {...params} id={component.id} className="inputComponent component-autocomplete focusNextWhenPressedEnter" label="" disabled={component.disabled} required={component.required} variant={this.props.inputVarient}/>
                        }}
                        wrapperStyle={{
                          display: 'inline-block',
                          width: '100%',
                        }}
                        menuStyle={{
                          borderRadius: '3px',
                          boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                          background: 'rgba(255, 255, 255, 0.9)',
                          padding: '2px 0',
                          fontSize: '90%',
                          position: 'fixed',
                          overflow: 'auto',
                          maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                          zIndex: '9999999',
                        }}
                      />
                  </Grid>
                  );
                }
              }, this)}
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
              <Button variant="outlined" className="rightButton" color="primary" id="btn-save-text-type" onClick={this.handleSearch.bind(this)} >
                Search
              </Button>
            </Grid>
            <br/>
            <hr/>
            <br/>
            <Paper>
              <Table size="small" id={this.props.collection._name + '_search_table'}>
                <TableHead>
                  <TableRow>
                    {this.props.config.tableKeys.map(function (column, key) {
                      return (
                        <TableCell key={key} component="th" scope="row">{column.label}</TableCell>
                      );
                    }, this)}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.props.collectionData.map(function (data, key) {
                    return (
                      <TableRow data-entry={JSON.stringify(data)} className={(key == 0) ? "Mui-selected tableRow" : "tableRow"} key={key}>
                        {this.props.config.tableKeys.map(function (column, childKey) {
                          if (column.formatDate){
                            return (
                              <TableCell key={childKey}>{this.formatDate(data[column.dbName])}</TableCell>
                            );
                          }else{
                            if (typeof data[column.dbName] == 'object' && Object.keys(data[column.dbName]) && Object.keys(data[column.dbName]).length > 0){
                              return (
                                <TableCell key={childKey}>{data[column.dbName].value}</TableCell>
                              );
                            }else{
                              return (
                                <TableCell key={childKey}>{data[column.dbName]}</TableCell>
                              );
                            }

                          }
                        }, this)}
                      </TableRow>
                    );
                  }, this)}
                </TableBody>
              </Table>
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.handleCloseEditDialogByRef.bind(this.props.parent)} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleTableSelection.bind(this)} color="primary">
              Select
            </Button>
          </DialogActions>
        </Dialog>
      </SearchDialogStyles>
    );
  }
}

export default SearchDialog = withTracker((props) => {
  let searchLogic = props.searchLogic;
  if (typeof searchLogic == 'undefined') searchLogic = {};
  let components = props.config.detailedSearchBy;
  let componentData = {};
  components.forEach(function (component) {
     if (component.type == 'selectbox' || component.type == 'autocomplete'){
       if (typeof component.dataCollection !== 'undefined'){
         componentData[component.id] = component.dataCollection.find({ appid: sessionStorage.getItem("appid"), appunit: sessionStorage.getItem("appunit") }).fetch();
       }
     }
  }, this);
  let collectionData = [];
  if (Object.keys(searchLogic).length > 0){
    searchLogic.appid = sessionStorage.getItem("appid");
    searchLogic.appunit = sessionStorage.getItem("appunit");
    collectionData = props.collection.find(searchLogic).fetch();
  }
  return {
    collectionData,
    componentData,
  };
})(SearchDialogContainer);
