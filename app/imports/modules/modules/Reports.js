import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import _ from 'underscore';

import {Collection_Roles} from '../../api/collections';

import Autocomplete from 'react-autocomplete';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import InputBase from '@material-ui/core/InputBase';
import Chip from '@material-ui/core/Chip';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import { getDataValueByKey } from '../../models/supporters';
import { FaTrash } from 'react-icons/fa';
import GlobalConfig from '../../config/global.config';


const ReportStyles = styled.div`
.single-row-table-paper {
  overflow: auto;
}
.multi-row-table-paper {
  overflow: auto;
}
.container-paper{
  margin: 10px;
}
.container{
  padding: 16px 16px !important;
}
.grid-item-search{
  padding: 0px 0px 8px 8px;
}
.editByIdInput{
  width: 74%;
}
.rightButton{
  width: 74%;
}
.component-autocomplete{
  width: 100%;
  border: 0;
  outline: 0;
  background: transparent;
  border-bottom: 1px solid rgba(0, 0, 0, 0.42);
}
.autocomplete-style{
  margin-top: 21px;
}
.tableWrapper{
  overflow-y: auto;
}
`

class ReportsContainer extends Component {

  constructor(props){
    super(props)
    this.newDataFetchedForSumField = false;
    this.extraPrintDetails = "";
		this.state = {
      open: false,
      customReportData : [],
    };
    let components = this.props.reportAttr.reportSearchBy;
    components.forEach(function (component) {
      if (component.type == 'checkbox'){
        this.state[component.id + '_search'] = false;
      }else if (component.type == 'datebox'){
        if (component.dateRange){
          this.state[component.id + '_search_datefrom'] = new Date();
          const today = new Date()
          const tomorrow = new Date(today)
          tomorrow.setDate(tomorrow.getDate() + 1)
          this.state[component.id + '_search_dateto'] = tomorrow;
        }else{
          this.state[component.id + '_search'] = new Date();
        }
      }else if (component.type == 'autocomplete'){
        this.state[component.id + '_search'] = {};
        this.state[component.id + '_search_inputValue'] = "";
      }else if (component.type == 'selectbox'){
        if (typeof component.dataCollection !== 'undefined'){
          this.state[component.id + '_search'] = "";
        }else{
          if (component.data.length > 0){
            this.state[component.id + '_search'] = component.data[0].value;
          }else{
            this.state[component.id + '_search'] = "";
          }
        }
      }
    }, this);
	}

  handleCheckboxChange(event){
    let target = event.currentTarget;
    let id = $(target).prop('id');
    this.setState({[id]: !this.state[id]});
  }

  handleDateboxChange(id, date){
    $('#' + id).parent().removeClass('Mui-error');
    this.setState({[id]: date});
  }

  handleSelectboxChange(id, event){
    this.setState({[id]: event.target.value});
  }

  handleEditInputKeyUp(event){
    let keyCode = (event.keyCode ? event.keyCode : event.which);
    let target = event.currentTarget;
    if (keyCode == '13'){
    }
  }

  handleSearch(event){
    if (this.checkForPermission() == false) return;
    let self = this;
    let components = this.props.reportAttr.reportSearchBy;
    let collection = this.props.reportAttr.collection;

    //Check for blank required values
    var cancelSubmition = false;
    components.forEach(function (component) {
      if (component.required){
        if (component.type == 'autocomplete'){
          if (Object.keys(this.state[component.id + '_search']).length == 0){
            cancelSubmition = true;
            $('#' + component.id + '_search').parent().addClass('Mui-error');
          }
        }else{
          if (this.state[component.id + '_search'].trim && this.state[component.id + '_search'].trim() == ''){
            cancelSubmition = true;
            $('#' + component.id + '_search').parent().addClass('Mui-error');
          }
        }
      }
    }, this);
    if (cancelSubmition){
      Bert.alert( 'Please fill up all required fields', 'warning', 'growl-top-right' );
      return;
    }

    //Create Data Scheme
    let dataScheme = {};
    components.forEach(function (component) {
      if (component.type == 'checkbox'){
        dataScheme[component.id] = this.state[component.id + '_search'];
      }else if (component.type == 'selectbox'){
        if ($('#' + component.id + '_search').val().trim() !== ''){
          if (component.dataCollection){
            dataScheme[component.id] = this.state[component.id + '_search'];
            dataScheme[component.id + '_collection'] = component.dataCollection._name;
          }else{
            dataScheme[component.id] = this.state[component.id + '_search'];
          }
        }
      }else if (component.type == 'datebox'){
        if (component.dateRange){
          if ($('#' + component.id + '_search_datefrom').val().trim() !== '' && $('#' + component.id + '_search_dateto').val().trim() !== ''){
            let datefrom = $('#' + component.id + '_search_datefrom').val().trim();
            let dateto = $('#' + component.id + '_search_dateto').val().trim();
            datefrom = datefrom.split('/');
            datefrom = datefrom[1] + '/' + datefrom[0] + '/' + datefrom[2];
            datefrom = new Date(datefrom).toISOString();
            dateto = dateto.split('/');
            dateto = dateto[1] + '/' + dateto[0] + '/' + dateto[2];
            dateto = new Date(dateto).toISOString();
            dataScheme[component.id] = { $gte : datefrom, $lt: dateto }
          }
        }else{
          if ($('#' + component.id + '_search').val().trim() !== ''){
            let date = $('#' + component.id + '_search').val().trim();
            date = date.split('/');
            date = date[1] + '/' + date[0] + '/' + date[2];
            date = new Date(date).toISOString();
            dataScheme[component.id] = date;
          }
        }
      }else if (component.type == 'autocomplete'){
        if (Object.keys(this.state[component.id + '_search']).length !== 0){
          if (this.props.reportAttr.customReportLogic){
            dataScheme[component.id] = this.state[component.id + '_search'];
          }else{
            if (this.state[component.id + '_search']._id){
              dataScheme[component.id] = this.state[component.id + '_search']._id;
            }
          }
        }
      }else{
        if ($('#' + component.id + '_search').val().trim() !== ''){
          dataScheme[component.id] = $('#' + component.id + '_search').val().trim();
        }
      }
    }, this);
    if (this.props.reportAttr.extraPrintDetails){
      this.extraPrintDetails = this.props.reportAttr.extraPrintDetails(dataScheme, this);
    }
    if (this.props.reportAttr.customReportLogic){
      var customReportLogicData = this.props.reportAttr.customReportLogic(dataScheme, this);
      if (customReportLogicData && customReportLogicData.length > 0) this.newDataFetchedForSumField = true;
      this.setState({ customReportData: customReportLogicData });
      return;
    }

    this.props.reportAttr.baseFunctions.handleChangeSearchLogic(dataScheme);
    this.initiateTableEvents();

  }

  initiateTableEvents(){
    let self = this;
    
  }

  componentDidMount(){

  }

  getTextboxType(component){
    if (component.password){
      return 'password';
    }else if(component.number){
      return 'number';
    }else{
      return 'text';
    }
  }

  formatDate(date){
    if (date && new Date(date).toDateString() !== 'Invalid Date'){
      // return (new Date(date).toDateString());
      var parsedDate = new Date(date);
      var dd = parsedDate.getDate();
      var mm = parsedDate.getMonth()+1; 
      var yyyy = parsedDate.getFullYear();
      return (dd + '/' + mm + '/' +yyyy);
    }else if(new Date(date).toDateString() == 'Invalid Date'){
      return date;
    }else{
      return '';
    }

  }

  subCollectionField(collection, selectionKey, valueKey, rawValue){
    var subColData = collection.findOne({ [selectionKey]: rawValue, appid: sessionStorage.getItem("appid"), appunit: sessionStorage.getItem("appunit") });
    if (subColData) return collection.findOne({ [selectionKey]: rawValue, appid: sessionStorage.getItem("appid"), appunit: sessionStorage.getItem("appunit") })[valueKey];
    return "";
  }

  sumField(collectionData){
    if (this.newDataFetchedForSumField){
      if (collectionData && collectionData.length > 0){
        let reportTableKeys = this.props.reportAttr.reportTableKeys;
        var sumRow = {};
        var sumTitleRow = {};
    
        //--- Check for sum
        reportTableKeys.forEach(function (tableKey) {
          sumRow[tableKey.dbName] = "";
          sumTitleRow[tableKey.dbName] = "";
          if (tableKey.sumField && tableKey.sumField.sumField){
            sumTitleRow[tableKey.dbName] = tableKey.sumField.fieldLabel;
            var totalSum = 0;
            collectionData.forEach((colData)=>{
              if (colData[tableKey.dbName] !== ""){
                totalSum += parseFloat(colData[tableKey.dbName]);
              }
            });
            sumRow[tableKey.dbName] = totalSum;
          }
        }, this);
    
        collectionData.push(sumTitleRow);
        collectionData.push(sumRow);
      }
      this.newDataFetchedForSumField = false;
    }
    
    return collectionData;
  }

  printTable(){
    var divToPrint=document.getElementById(this.props.reportAttr.collection._name + '_search_table');
    var newPrintWindow= window.open("");

    var style = "<style>";
    style = style + "table {width: 100%; font: 17px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
    style = style + "padding: 2px 3px; text-align: center;}";
    style = style + "p { font: Calibri; }";
    style = style + "</style>";

    newPrintWindow.document.write(style);
    //--- Header -- Title
    newPrintWindow.document.write('<h3 style="text-align: center;font: 22px Calibri;" >' + this.props.reportAttr.title + '<h3>');
    //--- Extra Details
    if  (this.extraPrintDetails && this.extraPrintDetails.trim() !== ""){
      newPrintWindow.document.write(this.extraPrintDetails);
      newPrintWindow.document.write('</br>');
      newPrintWindow.document.write('</br>');
    }
    //--- Content
    newPrintWindow.document.write(divToPrint.outerHTML);

    //---Footer
    newPrintWindow.document.write('<p style="font: 12px Calibri;position:absolute;bottom:0;" >' + GlobalConfig().appName + ' | Proudly powered by Evarosoft (evarosoft.com) </p>');
    newPrintWindow.print();
    newPrintWindow.close();
  }

  checkForPermission(){
    let allowed = false;
    allowed = (this.props.rolePermission && (this.props.rolePermission.allAccess || (this.props.rolePermission[this.props.reportAttr.id.replace('Report_', '') + '_access'])));
    if (allowed){
      allowed = true;
    }else{
      allowed = false;
    }
    
    return allowed;
  }

  render() {
  

    return (
      <ReportStyles>
      <Paper className="container-paper">
      <Container maxWidth="lg" className="container">
        <Grid container >
          <Grid item xs={12} md={6} ls={6} className="grid-item">
            <Typography variant="h5">
              {this.props.reportAttr.title}
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={2} >
          {this.props.reportAttr.reportSearchBy.map(function (component, parentKey) {
            if (component.type == 'textbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                    <TextField
                      id={component.id + '_search'}
                      label={component.label}
                      disabled={component.disabled}
                      type={this.getTextboxType(component)}
                      margin="normal"
                      fullWidth
                      InputProps={{
                        readOnly: component.readOnly ? true : false,
                      }}
                      variant={this.props.reportAttr.inputVarient}
                    />

                </Grid>
              );
            }else if (component.type == 'checkbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state[component.id + '_search']}
                          onChange={this.handleCheckboxChange.bind(this)}
                          id={component.id + '_search'}
                          disabled={component.disabled}
                          value={component.id + '_search'}
                          color={component.color}
                        />
                      }
                      label={component.label}
                    />
                  </FormGroup>
                </Grid>
              );
            }else if (component.type == 'datebox'){
              if (component.dateRange){
                return(
                  <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                    <Grid container spacing={2}>
                      <Grid item xs={6} md={6} lg={6} className="grid-item-search">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <Grid container justify="space-around">
                            <KeyboardDatePicker
                              variant="inline"
                              format={component.format}
                              disabled={component.disabled}
                              margin="normal"
                              fullWidth
                              id={component.id + '_search_datefrom'}
                              label={component.label}
                              value={this.state[component.id + '_search_datefrom']}
                              onChange={this.handleDateboxChange.bind(this, component.id + '_search_datefrom')}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}
                              variant={this.props.reportAttr.inputVarient}
                            />
                          </Grid>
                        </MuiPickersUtilsProvider>
                      </Grid>
                      <Grid item xs={6} md={6} lg={6} className="grid-item-search">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <Grid container justify="space-around">
                            <KeyboardDatePicker
                              variant="inline"
                              format={component.format}
                              disabled={component.disabled}
                              margin="normal"
                              fullWidth
                              id={component.id + '_search_dateto'}
                              label={component.label}
                              value={this.state[component.id + '_search_dateto']}
                              onChange={this.handleDateboxChange.bind(this, component.id + '_search_dateto')}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}
                              variant={this.props.reportAttr.inputVarient}
                            />
                          </Grid>
                        </MuiPickersUtilsProvider>
                      </Grid>
                    </Grid>
                  </Grid>
                );
              }else{
                return(
                  <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container justify="space-around">
                      <KeyboardDatePicker
                        variant="inline"
                        format={component.format}
                        disabled={component.disabled}
                        margin="normal"
                        fullWidth
                        id={component.id + '_search'}
                        label={component.label}
                        value={this.state[component.id + '_search']}
                        onChange={this.handleDateboxChange.bind(this, component.id + '_search')}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                        variant={this.props.reportAttr.inputVarient}
                      />
                    </Grid>
                  </MuiPickersUtilsProvider>
                  </Grid>
                );
              }
            }else if (component.type == 'selectbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item-search">
                  <FormControl fullWidth variant={this.props.reportAttr.inputVarient} disabled={component.disabled}>
                    <InputLabel id={component.id + '_search' + '-label'}>{component.label}</InputLabel>
                    <Select
                      labelid={component.id + '_search' + '-label'}
                      id={component.id + '_search'}
                      value={this.state[component.id + '_search']}
                      onChange={this.handleSelectboxChange.bind(this, component.id + '_search')}
                      fullWidth
                    >
                      { (typeof component.dataCollection !== 'undefined' && this.props.componentData[component.id + '_search']) ?
                        this.props.componentData[component.id + '_search'].map(function (entity, key) {
                          return (
                            <MenuItem value={entity[component.dataCollectionValue]} key={key} >{entity[component.dataCollectionLabel]}</MenuItem>
                          );
                        }, this)
                        :
                        component.data.map(function (entity, key) {
                        return (
                          <MenuItem value={entity.value} key={key} >{entity.label}</MenuItem>
                        );
                      }, this)}
                    </Select>
                  </FormControl>
                </Grid>
              );
            }else if (component.type == 'autocomplete'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item autocomplete-style">
                  <Typography variant="caption" display="block" gutterBottom>
                    {component.label} {(component.required ? ' *' : '')}
                  </Typography>
                  <Autocomplete
                    id={component.id}
                    items={(typeof component.dataCollection !== 'undefined') ? this.props.componentData[component.id + '_search'] : component.data }
                    shouldItemRender={(item, value) => {
                      let searchStringDBKeys = component.searchKeys;
                      let searchString = "";
                      for (var i=0;i<searchStringDBKeys.length;i++){
                        if (i!== 0) searchString += " ";
                        if (searchStringDBKeys[i].indexOf('.') >= 0){
                          let searchStringDBKeysSplit = searchStringDBKeys[i].split('.');
                          let searchStringText = item;
                          searchStringDBKeysSplit.forEach((searchStringKey, j) => {
                            searchStringText = searchStringText[searchStringDBKeysSplit[j]];
                          });
                          searchString += searchStringText;
                        }else{
                          searchString += item[searchStringDBKeys[i]];
                        }
                      }
                      return (
                        searchString.toLowerCase().indexOf(value.toLowerCase()) > -1
                      );
                    }}
                    getItemValue={item => {
                      let searchStringDBKeys = component.searchKeys;
                      let searchString = "";
                      for (var i=0;i<searchStringDBKeys.length;i++){
                        if (i!== 0) searchString += " ";
                        if (searchStringDBKeys[i].indexOf('.') >= 0){
                          let searchStringDBKeysSplit = searchStringDBKeys[i].split('.');
                          let searchStringText = item;
                          searchStringDBKeysSplit.forEach((searchStringKey, j) => {
                            searchStringText = searchStringText[searchStringDBKeysSplit[j]];
                          });
                          searchString += searchStringText;
                        }else{
                          searchString += item[searchStringDBKeys[i]];
                        }
                      }
                      return searchString;
                    }}
                    renderItem={(item, highlighted) => {
                      let searchStringDBKeys = component.searchKeys;
                      let searchString = "";
                      for (var i=0;i<searchStringDBKeys.length;i++){
                        if (i!== 0) searchString += " ";
                        if (searchStringDBKeys[i].indexOf('.') >= 0){
                          let searchStringDBKeysSplit = searchStringDBKeys[i].split('.');
                          let searchStringText = item;
                          searchStringDBKeysSplit.forEach((searchStringKey, j) => {
                            searchStringText = searchStringText[searchStringDBKeysSplit[j]];
                          });
                          searchString += searchStringText;
                        }else{
                          searchString += item[searchStringDBKeys[i]];
                        }
                      }
                      return (
                        <MenuItem
                          key={item[component.uniqueKey]}
                          style={{ backgroundColor: highlighted ? '#eee' : 'white'}}
                        >
                        <Grid container className="autocomplete-extrainfocontainer" >
                          <Grid >
                            {searchString}
                          </Grid>
                          <Grid className="autocomplete-extrainforow" >
                            {
                              (component.extraInformation && component.extraInformation.length > 0) ?
                              component.extraInformation.map((info, j) => {
                                  if (item && typeof item[info.key] !== 'undefined'){
                                    return (
                                      <Chip color="primary" size="small" label={info.label + ': ' + item[info.key]} key={j} className="autocomplete-extrainfochip" />
                                    );
                                  }
                                })
                              :
                              ""
                            }
                          </Grid>
                        </Grid>
                        </MenuItem>
                      );
                    }
                    }
                    value={this.state[component.id + '_search_inputValue']}
                    onChange={(e) => {
                      this.setState({[component.id + '_search_inputValue']: event.target.value});
                      if (event.target.value == ""){
                        this.setState({[component.id + '_search']: ""});
                        this.setState({[component.id + '_search_inputValue']: ""});
                        if (component.onSelectCallback){
                          component.onSelectCallback(item, this);
                        }
                      }
                    }}
                    onSelect={(value, item) => {
                      let data = item;
                      this.setState({[component.id + '_search']: item});
                      this.setState({[component.id + '_search_inputValue']: value});
                      if (component.onSelectCallback){
                        component.onSelectCallback(item, this);
                      }
                    }}
                    renderInput={params =>{
                      return <input {...params} id={component.id} className="inputComponent component-autocomplete focusNextWhenPressedEnter" label="" disabled={component.disabled} required={component.required}  variant={this.props.reportAttr.inputVarient}/>
                    }}
                    wrapperStyle={{
                      display: 'inline-block',
                      width: '100%',
                    }}
                    menuStyle={{
                      borderRadius: '3px',
                      boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                      background: 'rgba(255, 255, 255, 0.9)',
                      padding: '2px 0',
                      fontSize: '90%',
                      position: 'fixed',
                      overflow: 'auto',
                      maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                      zIndex: '9999999',
                    }}
                  />
              </Grid>
              );
            }
          }, this)}
        </Grid>
        <Grid container >
          <Grid item xs={12} md={9} lg={9}>
            <Button variant="outlined" className="rightButton" color="primary" id="btn-save-text-type" fullWidth onClick={this.handleSearch.bind(this)} >
              Search
            </Button>
          </Grid>
          <Grid item xs={12} md={3} lg={3}>
            <Button variant="outlined" className="rightButton" color="default" id="btn-print-text-type" fullWidth onClick={this.printTable.bind(this)} >
              Print
            </Button>
          </Grid>
        </Grid>
        <br/>
        <hr/>
        <br/>
        <Paper className="tableWrapper" >
          {(this.props.reportAttr.customReportLogic) ? (
            <Table size="small" id={this.props.reportAttr.collection._name + '_search_table'}>
              <TableHead>
                <TableRow>
                  {this.props.reportAttr.reportTableKeys.map(function (column, key) {
                    return (
                      <TableCell key={key} component="th" scope="row">{column.label}</TableCell>
                    );
                  }, this)}
                </TableRow>
              </TableHead>
              <TableBody>
                {
                this.sumField(this.state.customReportData).map(function (data, key) {
                  return (
                    <TableRow data-entry={JSON.stringify(data)} className={(key == 0) ? "Mui-selected tableRow" : "tableRow"} key={key}>
                      {this.props.reportAttr.reportTableKeys.map(function (column, childKey) {
                        if (column.formatDate){
                          return (
                            <TableCell key={childKey}>{this.formatDate(getDataValueByKey(data, column.dbName))}</TableCell>
                          );
                        }else if (column.subCollectionField){

                          return (
                            <TableCell key={childKey}>{this.subCollectionField(column.subCollectionField.collection, column.subCollectionField.selectionKey, column.subCollectionField.valueKey, data[column.dbName])}</TableCell>
                          );
                        }else{
                          return (
                            <TableCell key={childKey} dangerouslySetInnerHTML={{__html: getDataValueByKey(data, column.dbName)}} />
                          );
                        }
                      }, this)}
                    </TableRow>
                  );
                }, this)}
              </TableBody>
            </Table>
          ):(
            <Table size="small" id={this.props.reportAttr.collection._name + '_search_table'}>
              <TableHead>
                <TableRow>
                  {this.props.reportAttr.reportTableKeys.map(function (column, key) {
                    return (
                      <TableCell key={key} component="th" scope="row">{column.label}</TableCell>
                    );
                  }, this)}
                </TableRow>
              </TableHead>
              <TableBody>
                {this.props.collectionData.map(function (data, key) {
                  return (
                    <TableRow data-entry={JSON.stringify(data)} className={(key == 0) ? "Mui-selected tableRow" : "tableRow"} key={key}>
                      {this.props.reportAttr.reportTableKeys.map(function (column, childKey) {
                        if (column.formatDate){
                          return (
                            <TableCell key={childKey}>{this.formatDate(getDataValueByKey(data, column.dbName))}</TableCell>
                          );
                        }else if (column.subCollectionField){

                          return (
                            <TableCell key={childKey}>{this.subCollectionField(column.subCollectionField.collection, column.subCollectionField.selectionKey, column.subCollectionField.valueKey, data[column.dbName])}</TableCell>
                          );
                        }else{
                          return (
                            <TableCell key={childKey}>{ getDataValueByKey(data, column.dbName)}</TableCell>
                          );
                        }
                      }, this)}
                    </TableRow>
                  );
                }, this)}
              </TableBody>
            </Table>
          )}
        </Paper>
        </Container>
        </Paper>
      </ReportStyles>
    );
  }
}

export default Reports = withTracker((props) => {
  let searchLogic = props.searchLogic;
  if (typeof searchLogic == 'undefined') searchLogic = {};
  let components = props.reportAttr.reportSearchBy;
  let reportTableKeys = props.reportAttr.reportTableKeys;
  let componentData = {};
  components.forEach(function (component) {
     if (component.type == 'selectbox' || component.type == 'autocomplete'){
      if (typeof component.dataCollection !== 'undefined'){
        let dataLogic = {};
        if (component.dataLogic) dataLogic = component.dataLogic;
        dataLogic.appid = sessionStorage.getItem("appid");
        dataLogic.appunit = sessionStorage.getItem("appunit");
        componentData[component.id + '_search'] = component.dataCollection.find(dataLogic).fetch();
      }
    }
  }, this);
  
  let collectionData = [];
  
  
  if (searchLogic !== false && Object.keys(searchLogic).length > 0){
      searchLogic.appid = sessionStorage.getItem("appid");
      searchLogic.appunit = sessionStorage.getItem("appunit");
    collectionData = props.reportAttr.collection.find(searchLogic).fetch();
  }else if (searchLogic !== false && props.reportAttr.allowBlankSearch){
      searchLogic.appid = sessionStorage.getItem("appid");
      searchLogic.appunit = sessionStorage.getItem("appunit");
    collectionData = props.reportAttr.collection.find(searchLogic).fetch();
  }
  
  var sumRow = {};
  var sumTitleRow = {};


  let hasSumField = false;
  //--- Check for sum
  reportTableKeys.forEach(function (tableKey) {
    sumRow[tableKey.dbName] = "";
    sumTitleRow[tableKey.dbName] = "";
    if (tableKey.sumField && tableKey.sumField.sumField){
      hasSumField = true;
      sumTitleRow[tableKey.dbName] = tableKey.sumField.fieldLabel;
      var totalSum = 0;
      collectionData.forEach((colData)=>{
        totalSum += parseFloat(colData[tableKey.dbName]);
      });
      sumRow[tableKey.dbName] = totalSum;
    }
  }, this);

  if (hasSumField){
    collectionData.push(sumTitleRow);
    collectionData.push(sumRow);
  }
  


  //--- Role permissions
  let rolePermission = {};
  if (Meteor.user()){
    if (Meteor.user().profile.role == 'evarosoft_owner'){
      rolePermission.allAccess = true;
    }else if((Collection_Roles.findOne({rolename: 'admin'}) && Meteor.user().profile.role == Collection_Roles.findOne({rolename: 'admin'})._id) || Meteor.user().profile.role == 'owner'){
      rolePermission.allAccess = true;
    }else{
      rolePermission = Collection_Roles.findOne(Meteor.user().profile.role);
    }
  }
  return {
    collectionData,
    componentData,
    rolePermission,
  };
})(ReportsContainer);
