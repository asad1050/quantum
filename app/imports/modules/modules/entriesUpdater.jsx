import React, { Component, forwardRef } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import _ from 'lodash';
import swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import {Collection_Roles} from '../../api/collections';

import Autocomplete from 'react-autocomplete';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import InputBase from '@material-ui/core/InputBase';
import Chip from '@material-ui/core/Chip';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

import { FaTrash, FaPen, FaSearch } from 'react-icons/fa';

import { getDataValueByKey } from '../../models/supporters';

const Styles = styled.div`
  .single-row-table-paper {
    overflow: auto;
  }
  .multi-row-table-paper {
    overflow: auto;
  }
  .container-paper{
    margin: 10px;
  }
  .container{
    padding: 16px 16px !important;
  }
  .grid-item{
    padding: 0px 0px 8px 8px;
  }
  .editByIdInput{
    width: 74%;
  }
  .rightButton{
    width: 74%;
  }
  .searchInputContainer{
    padding: 0px 10px;
  }
  .instant_search_item_input{
    width: 100%;
    border: 0;
    font-size: 14px;
    margin-top: 9px;
  }
  .component-autocomplete{
    width: 100%;
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid rgba(0, 0, 0, 0.42);
  }
  .autocomplete-style{
    margin-top: 21px;
  }
`

class EntriesUpdaterContainer extends Component {

  constructor(props){
    super(props)
    this.dataFetched = false;
    this.state = { changeState: false };
    let components = this.props.entriesUpdaterAttr.components;
    components.forEach(function (component) {
      if (component.type == 'checkbox'){
        this.state[component.id] = false;
      }else if (component.type == 'datebox'){
        this.state[component.id] = new Date();
      }else if (component.type == 'radiobox'){
        if (component.options.length > 0){
          this.state[component.id] = component.options[0].value;
        }else{
          this.state[component.id] = "";
        }
      }else if (component.type == 'selectbox'){
        if (typeof component.dataCollection !== 'undefined'){
          this.state[component.id] = "";
          this.state[component.id + '_inputValue'] = "";
        }else{
          if (component.data.length > 0){
            this.state[component.id] = component.data[0].value;
            this.state[component.id + '_inputValue'] = component.data[0].value;
          }else{
            this.state[component.id] = "";
            this.state[component.id + '_inputValue'] = "";
          }
          this.state[component.id + '_collection'] = component.dataCollection;
        }
      }else{
        this.state[component.id] = "";
      }
    }, this);
    this.dataTimeout = false;
  }

  componentDidUpdate(prevProps) {
    let data = this.props.entriesUpdaterData;
    
    window.clearTimeout( this.dataTimeout );
    this.dataTimeout = setTimeout(() => {
      this.dataFetched = true;
      this.setState({ changeState: false });
    }, 1000);
    
    if (this.dataFetched == false){
      if (data && Object.keys(data).length > 0){
          for (var i=0;i<Object.keys(data).length;i++){
            var dataEntity = data[Object.keys(data)[i]];
            var dataKey = Object.keys(data)[i];
            if (Object.keys(dataEntity) && Object.keys(dataEntity)[0] == 'data'){
              this.setState({ [dataKey]: dataEntity.data });
              this.setState({ [dataKey + '_inputValue']: dataEntity.value });
            }else if (dataEntity && dataEntity.length > 0 && typeof dataEntity == 'object'){
              if (dataKey.indexOf('_simple') >= 0){
                  this.setState({ [dataKey.replace('_simple', '')]: dataEntity });
              }else{
                  this.setState({ [dataKey + '_storeable']: dataEntity });
              }
            }else{
                
              this.setState({ [dataKey]: dataEntity });
            }
          }

          this.dataFetched = true;
      }
      let components = this.props.entriesUpdaterAttr.components;
      if (components.length > 0){
      $('#' + components[0].id).focus();
      }

    }
    
  }
  
  initiateFormInputEvents(){
    let self = this;
    $('.inputComponent').unbind().keyup((event) => {
      let target = event.currentTarget;
      if (event.key == 'Enter'){
        //--- Enter
        event.preventDefault();
        if ($(target).hasClass('focusNextWhenPressedEnter')){
          //--- Table Inputs
          let found = false;
          let alteration = 3;
          while (alteration > 0){
            //--- For Table inputs
            let input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().find('input');
            if (alteration == 2){
              input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().find('input');
            }else if (alteration == 1){
              input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().next().find('input');
            }
            if (input.length > 0){
              input.focus();
              found = true;
              break;
            }else{
              input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().find('button.MuiIconButton-root[title="Save"]');
              if (alteration == 2){
                input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().find('button.MuiIconButton-root[title="Save"]');
              }else if (alteration == 1){
                input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().next().find('button.MuiIconButton-root[title="Save"]');
              }
              input.focus();
              if (input.length == 0){
                alteration--;
              }else{
                found = true;
                break;
              }
            }
          }

          if (found == false){
            //--- Form inputs
            let alteration = 3;
            while (alteration > 0){
              let input = $(target).closest('div.grid-item').next().find('.inputComponent').find('.MuiSelect-select.MuiSelect-selectMenu');
              if (alteration == 2){
                input = $(target).closest('div.grid-item').next().next().find('.inputComponent').find('.MuiSelect-select.MuiSelect-selectMenu');
              }else if (alteration == 1){
                input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent').find('.MuiSelect-select.MuiSelect-selectMenu');
              }
              if (input.length > 0){
                input.focus();
                break;
              }else{
                input = $(target).closest('div.grid-item').next().find('.inputComponent').find('input:enabled');
                if (alteration == 2){
                  input = $(target).closest('div.grid-item').next().next().find('.inputComponent').find('input:enabled');
                }else if (alteration == 1){
                  input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent').find('input:enabled');
                }
                if (input.length > 0){
                  input.focus();
                  break;
                }else{
                  input = $(target).closest('div.grid-item').next().find('.inputComponent').find('button:enabled');
                  if (alteration == 2){
                    input = $(target).closest('div.grid-item').next().next().find('.inputComponent').find('button:enabled');
                  }else if (alteration == 1){
                    input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent').find('button:enabled');
                  }
                  if (input.length > 0){
                    input.focus();
                    break;
                  }else{
                    input = $(target).closest('div.grid-item').next().find('.inputComponent:enabled');
                    if (alteration == 2){
                      input = $(target).closest('div.grid-item').next().next().find('.inputComponent:enabled');
                    }else if (alteration == 1){
                      input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent:enabled');
                    }
                    if (input.length > 0){
                      if (input.length <= 1){
                        input.focus();
                      }else{
                        $(input[0]).focus();
                      }
                      break;
                    }else{
                      input = $(target).closest('div.grid-item').next().find('button[title="Add"]');
                      if (alteration == 2){
                        input = $(target).closest('div.grid-item').next().next().find('button[title="Add"]');
                      }else if (alteration == 1){
                        input = $(target).closest('div.grid-item').next().next().next().find('button[title="Add"]');
                      }
                      input.focus();
                      if (input.length == 0){
                        alteration--;
                      }else{
                        break;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }else if (event.key == 'F9'){
        //--- F9
        event.preventDefault();
        if ($(target).hasClass('openDateWhenF12Pressed')){
          $(target).find('button.MuiButtonBase-root').click();
        }
      }else if (event.key == 'ArrowRight'){
        //--- Right Arrow Key
        event.preventDefault();
        if ($(target).hasClass('moveNextWithArrowRightKey')){

          $(target).next().focus();
        }
      }else if (event.key == 'ArrowLeft'){
        //--- Left Arrow Key
        event.preventDefault();
        if ($(target).hasClass('moveBackWithArrowLeftKey')){

          $(target).prev().focus();
        }
      }
      //--- Form input logic check
      if ($(event.target).prop('type') == 'number'){
        let formComponentLogics = self.props.formAttr.formComponentLogics;
        if (formComponentLogics && formComponentLogics.length > 0){
          formComponentLogics.map((logic, i)=>{
            if ($(target).find('input').prop('id') == logic.entry1 || $(target).find('input').prop('id') == logic.entry2){
              let logicResult = 0;
              let entry1 = $('#'+logic.entry1).val();
              let entry2 = $('#'+logic.entry2).val();
              if (!isNaN(entry1) && !isNaN(entry2)){
                switch (logic.operand) {
                  case '+':
                    logicResult = parseFloat(entry1) + parseFloat(entry2);
                    break;
                  case '-':
                    logicResult = parseFloat(entry1) - parseFloat(entry2);
                    break;
                  case '*':
                    logicResult = parseFloat(entry1) * parseFloat(entry2);
                    break;
                  case '/':
                    logicResult = parseFloat(entry1) / parseFloat(entry2);
                    break;
                  case '%':
                    if (entry1 !== "" && entry2 !== ""){
                      let diffrentialValue = parseFloat(entry1) - parseFloat(entry2);
                      logicResult = diffrentialValue / parseFloat(entry1);
                      logicResult = logicResult * 100;
                    }
                    break;
                  case '100-%':
                    if (entry1 !== "" && entry2 !== ""){
                      let diffrentialValue = parseFloat(entry1) - parseFloat(entry2);
                      logicResult = diffrentialValue / parseFloat(entry1);
                      logicResult = logicResult * 100;
                      logicResult = 100 - logicResult;
                    }
                  case '%*':
                    if (entry1 !== "" && entry2 !== ""){
                      entry2 = entry2 / 100;
                      logicResult = entry1 * entry2;
                    }
                    break;
                  default:
                }
                logicResult = logicResult.toFixed(2);
                if (!isNaN(logicResult) && logic.result !== $(event.target).prop('id')){
                  self.setState({[logic.result]: logicResult});
                }else{
                  self.setState({[logic.result]: 0});
                }
              }
            }
          });
        }
      }

    });

    $('button[title="Add"]').unbind().click((event) => {
      setTimeout(() => {
        $($('table').find('input')[0]).focus();
        $('table').find('input').addClass('inputComponent focusNextWhenPressedEnter');
        setTimeout(()=>{
          this.initiateFormInputEvents();
        }, 100);
      }, 100);
    });
  }

  componentDidMount(){
    this.initiateFormInputEvents();
    let self = this;
    this.dataFetched = false;
    this.setState({ changeState: true });
    $(window).unbind().on('keydown', function (event) {

      self.includeFormWindowEvents(event);
    });

  }

  componentWillUnmount(){
    $(window).unbind();
  }

  includeFormWindowEvents(event){
    if (event.key == 's'){
        if (event.originalEvent.ctrlKey){
          event.preventDefault();
          this.handleUpdate();
        }
    }
  }

  handleCheckboxChange(event){
    let target = event.currentTarget;
    let id = $(target).prop('id');
    this.setState({[id]: !this.state[id]});
  }

  handleRadioboxChange(id, event){
    let target = event.currentTarget;
    this.setState({[id]: $(target).val()});
  }

  handleDateboxChange(id, date){
    $('#' + id).parent().removeClass('Mui-error');
    this.setState({[id]: date});
  }

  handleSelectboxChange(id, object, event){
    this.setState({[id]: event.props.value});
    this.setState({[id + '_inputValue']: event.props.children});
  }

  handleKeyChange(id, event){
    this.setState({[id]: event.target.value});
  }

  handleEditInputKeyUp(event){
    let keyCode = (event.keyCode ? event.keyCode : event.which);
    let target = event.currentTarget;
    if (keyCode == '13'){

    }
  }

  checkForPermission(){
    let allowed = false;
    allowed = (this.props.rolePermission && (this.props.rolePermission.allAccess || (this.props.rolePermission[this.props.entriesUpdaterAttr.id.replace('Form_', '') + '_access'])));
    if (allowed){
      allowed = true;
    }else{
      allowed = false;
    }
    
    return allowed;
  }

  handleUpdate(event){
    if (this.checkForPermission() == false) return;
    if (this.props.entriesUpdaterAttr.restrictUpdate) return ;
    let self = this;
    let components = this.props.entriesUpdaterAttr.components;
    let collection = this.props.entriesUpdaterAttr.collection;
    //Check for blank required values
    var cancelSubmition = false;
    components.forEach(function (component) {
      if (component.required){
        if (this.state[component.id].trim && this.state[component.id].trim() == ''){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
        }
      }
    }, this);
    if (cancelSubmition){
      Bert.alert( 'Please fill up all required fields', 'warning', 'growl-top-right' );
      return;
    }

    
    let dataScheme = {};
    //---Identifier
    dataScheme[this.props.entriesUpdaterAttr.idKey] = this.props.entriesUpdaterAttr.idValue;
    //Create Data Scheme
    components.forEach(function (component) {
      if (component.type == 'selectbox'){
        let selectBoxData = {};
        if (component.dataCollection){
          dataScheme[component.id] = this.state[component.id];
          dataScheme[component.id + '_collection'] = component.dataCollection._name;
          dataScheme[component.id + '_inputValue'] = this.state[component.id + '_inputValue'];
        }else{
          dataScheme[component.id] = this.state[component.id];
          dataScheme[component.id + '_inputValue'] = this.state[component.id + '_inputValue'];
        }

      }else if (component.type == 'datebox'){
        let date = $('#' + component.id).val().trim();
        date = date.split('/');
        date = date[1] + '/' + date[0] + '/' + date[2];
        date = new Date(date).toISOString();
        dataScheme[component.id] = date;
      }else if (component.type == 'textbox'){
        if (component.number && this.state[component.id] == ''){
          dataScheme[component.id] = 0;
        }else{
          dataScheme[component.id] = this.state[component.id];
        }
      }else{
        dataScheme[component.id] = this.state[component.id];
      }
    }, this);

    if (this.props.entriesUpdaterAttr.onSubmit) {
      this.props.entriesUpdaterAttr.onSubmit(dataScheme, this);
      return;
    }

    let options = {};
    options.appid = sessionStorage.getItem("appid");
    options.appname = sessionStorage.getItem("appname");
    options.apptype = sessionStorage.getItem("apptype");
    options.appplan = sessionStorage.getItem("appplan");

    if (this.props.entriesUpdaterData && Object.keys(this.props.entriesUpdaterData).length > 0){
      swal.fire({
        title: 'Updating Field',
        onBeforeOpen: () => {
          swal.showLoading();
          Meteor.call('form.update', collection._name, { appid: sessionStorage.getItem("appid") }, dataScheme, options, this.props.entriesUpdaterAttr.id, (err, res) => {
            if (err){
              if (typeof self.props.entriesUpdaterAttr.updateErrorMessage !== 'undefined' && self.props.entriesUpdaterAttr.updateErrorMessage !== ''){
                swal.fire({
                  type: 'error',
                  title: 'Oops...',
                  text: self.props.entriesUpdaterAttr.updateErrorMessage + ', Reason: ' + err.error ,
                }).then(()=>{
                  setTimeout(()=>{
                    if (components.length > 0){
                      $('#' + components[0].id).focus();
                    }
                  }, 400);
                });
              }else{
                swal.close();
              }
            }else{
              if (typeof self.props.entriesUpdaterAttr.updateSuccessMessage !== 'undefined' && self.props.entriesUpdaterAttr.updateSuccessMessage !== ''){
                if (collection._name == 'generalsettings') {
                  sessionStorage.setItem("settings", JSON.stringify(dataScheme));
                }
                swal.fire({
                  type: 'success',
                  title: 'Success',
                  text: self.props.entriesUpdaterAttr.updateSuccessMessage,
                  focusConfirm: true,
                }).then(()=>{
                  setTimeout(()=>{
                    if (components.length > 0){
                      $('#' + components[0].id).focus();
                    }
                  }, 400);
                });
              }else{
                swal.close();
              }
              this.dataFetched = false;
            }
          });
        },
      });
    }else{
      swal.fire({
        title: 'Updating Field',
        onBeforeOpen: () => {
          swal.showLoading();
          Meteor.call('form.create', collection._name, dataScheme, dataScheme, options, this.props.entriesUpdaterAttr.id, (err, res) => {
            if (err){
              if (typeof self.props.entriesUpdaterAttr.updateErrorMessage !== 'undefined' && self.props.entriesUpdaterAttr.updateErrorMessage !== ''){
                swal.fire({
                  type: 'error',
                  title: 'Oops...',
                  text: self.props.entriesUpdaterAttr.updateErrorMessage + ', Reason: ' + err.error ,
                }).then(()=>{
                  setTimeout(()=>{
                    if (components.length > 0){
                      $('#' + components[0].id).focus();
                    }
                  }, 400);
                });
              }else{
                swal.close();
              }
            }else{
              if (typeof self.props.entriesUpdaterAttr.updateSuccessMessage !== 'undefined' && self.props.entriesUpdaterAttr.updateSuccessMessage !== ''){
                swal.fire({
                  type: 'success',
                  title: 'Success',
                  text: self.props.entriesUpdaterAttr.updateSuccessMessage,
                  focusConfirm: true,
                }).then(()=>{
                  setTimeout(()=>{
                    if (components.length > 0){
                      $('#' + components[0].id).focus();
                    }
                  }, 400);
                });
              }else{
                swal.close();
              }
              this.dataFetched = false;
            }
          });
        },
      });
    }
  }

  resetFormInputs(skipFocus){
    let components = this.props.entriesUpdaterAttr.components;
    this.setState({ mode: 'new', selectedEntry: {} });
    components.forEach(function (component) {
      if (component.type == 'textbox'){
        this.setState({[component.id] : ''});
      }else if(component.type == 'checkbox'){
        this.setState({[component.id] : false});
      }else if(component.type == 'autocomplete'){
        this.setState({[component.id] : []});
        this.setState({[component.id + '_inputValue'] : ""});
      }else if(component.type == 'multiselecttable'){
        this.setState({[component.id] : []});
        this.setState({[component.id + '_storeable'] : []});
      }else if (component.type == 'selectbox'){
        if (typeof component.dataCollection !== 'undefined'){
          this.state[component.id] = "";
          this.state[component.id + '_inputValue'] = "";
        }else{
          if (component.data.length > 0){
            this.state[component.id] = component.data[0].value;
            this.state[component.id + '_inputValue'] = component.data[0].value;
          }else{
            this.state[component.id] = "";
            this.state[component.id + '_inputValue'] = "";
          }
          this.state[component.id + '_collection'] = component.dataCollection;
        }
      }
    }, this);
    $('.inputComponent').parent().removeClass('Mui-error');
    if (typeof skipFocus == 'undefined' || skipFocus == false){
      if (components.length > 0){
        $('#' + components[0].id).focus();
      }
    }
  }

  getTextboxType(component){
    if (component.password){
      return 'password';
    }else if(component.number){
      return 'number';
    }else{
      return 'text';
    }
  }

  render() {
    let self = this;
    let components = this.props.entriesUpdaterAttr.components;
    let collection = this.props.entriesUpdaterAttr.collection;
    let apps = this.props.apps;
    return (
      <Styles>
        <Paper className="container-paper">
        <Container maxWidth="lg" className="container">
        <Grid container >
          <Grid item xs={12} md={12} ls={12} className="grid-item">
            <Typography variant="h5">
              {this.props.entriesUpdaterAttr.title}
            </Typography>
          </Grid>
        </Grid>
        <Grid container >
          {components.map(function (component, parentKey) {
            if (component.type == 'blank'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                </Grid>
              );
            }else if (component.type == 'text'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                    <Typography id={component.id} variant={component.tag}>
                      {component.label}
                    </Typography>
                </Grid>
              );
            }else if (component.type == 'textbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                    <TextField
                      id={component.id}
                      label={component.label}
                      disabled={component.disabled}
                      type={this.getTextboxType(component)}
                      margin="normal"
                      className="inputComponent component-textbox focusNextWhenPressedEnter"
                      fullWidth
                      value={this.state[component.id]}
                      onChange={this.handleKeyChange.bind(this, component.id)}
                      required={component.required}
                      InputProps={{
                        readOnly: component.readOnly ? true : false,
                      }}
                      variant={this.props.entriesUpdaterAttr.inputVarient}
                      defaultValue={this.props.entriesUpdaterAttr.defaultValue}
                    />

                </Grid>
              );
            }else if (component.type == 'checkbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state[component.id]}
                          onChange={this.handleCheckboxChange.bind(this)}
                          id={component.id}
                          disabled={component.disabled}
                          value={component.id}
                          color={component.color}
                          className="inputComponent component-checkbox focusNextWhenPressedEnter"
                        />
                      }
                      label={component.label}
                    />
                  </FormGroup>
                </Grid>
              );
            }else if (component.type == 'datebox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Grid container justify="space-around">
                    <KeyboardDatePicker
                      variant="inline"
                      format={component.format}
                      required={component.required}
                      disabled={component.disabled}
                      margin="normal"
                      className="inputComponent component-datebox focusNextWhenPressedEnter openDateWhenF12Pressed"
                      fullWidth
                      id={component.id}
                      label={component.label}
                      value={this.state[component.id]}
                      onChange={this.handleDateboxChange.bind(this, component.id)}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                      variant={this.props.entriesUpdaterAttr.inputVarient}
                    />
                  </Grid>
                </MuiPickersUtilsProvider>
                </Grid>
              );
            }else if (component.type == 'radiobox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                <FormControl component="fieldset">
                  <FormLabel component="legend">{component.label}</FormLabel>
                  <RadioGroup aria-label="gender" id={component.id} required={component.required} name="gender1" value={this.state[component.id]} onChange={this.handleRadioboxChange.bind(this, component.id)}>
                  {component.options.map(function (option, key) {
                    return (
                      <FormControlLabel className="inputComponent component-radiobox focusNextWhenPressedEnter" disabled={component.disabled} value={option.value} control={<Radio />} label={option.label} key={key} />
                    );
                  }, this)}
                  </RadioGroup>
                </FormControl>
                </Grid>
              );
            }else if (component.type == 'selectbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                  <FormControl fullWidth required={component.required} variant={this.props.entriesUpdaterAttr.inputVarient} disabled={component.disabled}>
                    <InputLabel id={component.id + '-label'}>{component.label}</InputLabel>
                    <Select
                      labelid={component.id + '-label'}
                      id={component.id}
                      value={this.state[component.id]}
                      onChange={this.handleSelectboxChange.bind(this, component.id)}
                      fullWidth
                      className="inputComponent component-selectbox focusNextWhenPressedEnter"
                    >
                      { (typeof component.dataCollection !== 'undefined' && this.props.componentData[component.id]) ?
                        this.props.componentData[component.id].map(function (entity, key) {
                          return (
                            <MenuItem value={entity[component.dataCollectionValue]} key={key} >{entity[component.dataCollectionLabel]}</MenuItem>
                          );
                        }, this)
                        :
                        component.data.map(function (entity, key) {
                        return (
                          <MenuItem value={entity.value} key={key} >{entity.label}</MenuItem>
                        );
                      }, this)}
                    </Select>
                  </FormControl>
                </Grid>
              );
            }

          }, this)}

        <Grid item xs={12} md={12} lg={12}>
        {(this.checkForPermission()) ? 
          <Button fullWidth className="inputComponent" variant="outlined" color="primary" id="btn-update-text-type" onClick={this.handleUpdate.bind(this)} >
            Update
          </Button>
        :
          <div></div>
          }  
            </Grid>
        </Grid>

        </Container>
        </Paper>
      </Styles>
    );
  }
}

export default EntriesUpdater = withTracker((props) => {
  let components = props.entriesUpdaterAttr.components;
  let entriesUpdaterData;
  if (props.entriesUpdaterAttr.collectionLogic) props.entriesUpdaterAttr.collectionLogic.appid = sessionStorage.getItem("appid");
  if (props.entriesUpdaterAttr.collectionLogic) props.entriesUpdaterAttr.collectionLogic.appunit = sessionStorage.getItem("appunit");
  if (props.entriesUpdaterAttr.collection && (typeof props.entriesUpdaterAttr.onSubmit == 'undefined' || props.entriesUpdaterAttr.onSubmit == false)) entriesUpdaterData = props.entriesUpdaterAttr.collection.findOne(props.entriesUpdaterAttr.collectionLogic);
  //--- Role permissions
  let rolePermission = {};
  if (Meteor.user()){
    if (Meteor.user().profile.role == 'evarosoft_owner'){
      rolePermission.allAccess = true;
    }else if((Collection_Roles.findOne({rolename: 'admin'}) && Meteor.user().profile.role == Collection_Roles.findOne({rolename: 'admin'})._id) || Meteor.user().profile.role == 'owner'){
      rolePermission.allAccess = true;
    }else{
      rolePermission = Collection_Roles.findOne(Meteor.user().profile.role);
    }
  }
  return {
    entriesUpdaterData,
    rolePermission,
  };
})(EntriesUpdaterContainer);
