import React, { Component, forwardRef } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import _ from 'underscore';
import swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import {Collection_Roles} from '../../api/collections';

import Autocomplete from 'react-autocomplete';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import InputBase from '@material-ui/core/InputBase';
import Chip from '@material-ui/core/Chip';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

import { FaTrash, FaPen, FaSearch } from 'react-icons/fa';

import SearchDialog from './SearchDialog';
import { getDataValueByKey } from '../../models/supporters';

const Styles = styled.div`
  .single-row-table-paper {
    overflow: auto;
  }
  .multi-row-table-paper {
    overflow: auto;
  }
  .container-paper{
    margin: 10px;
  }
  .container{
    padding: 16px 16px !important;
  }
  .grid-item{
    padding: 0px 0px 8px 8px;
  }
  .editByIdInput{
    width: 74%;
  }
  .rightButton{
    width: 74%;
  }
  .searchInputContainer{
    padding: 0px 10px;
  }
  .instant_search_item_input{
    width: 100%;
    border: 0;
    font-size: 14px;
    margin-top: 9px;
  }
  .component-autocomplete{
    width: 100%;
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid rgba(0, 0, 0, 0.42);
  }
  .autocomplete-style{
    margin-top: 21px;
  }
`

class FormsContainer extends Component {

  constructor(props){
		super(props)
		this.state = {
      openEditDialog: false,
      searchLogic: {},
      selectedEntry: {},
      mode: 'new',
      instantSearchInputState: {},
      instantSearchInputValue: "",
      autoPrint: false,
      formProcessing: false,
    };
    
    let components = this.props.formAttr.components;
    components.forEach(function (component) {
      if (component.type == 'checkbox'){
        this.state[component.id] = false;
      }else if (component.type == 'datebox'){
        if (typeof component.defaultValue !== 'undefined' && component.defaultValue !== false) {
          this.state[component.id] = component.defaultValue;
        } else {
          this.state[component.id] = new Date();
        }
      }else if (component.type == 'radiobox'){
        if (component.options.length > 0){
          this.state[component.id] = component.options[0].value;
        }else{
          this.state[component.id] = "";
        }
      }else if (component.type == 'selectbox'){
        if (typeof component.dataCollection !== 'undefined'){
          this.state[component.id] = "";
          this.state[component.id + '_inputValue'] = "";
        }else{
          if (component.data.length > 0){
            this.state[component.id] = component.data[0].value;
            this.state[component.id + '_inputValue'] = component.data[0].value;
          }else{
            this.state[component.id] = "";
            this.state[component.id + '_inputValue'] = "";
          }
          this.state[component.id + '_collection'] = component.dataCollection;
        }
      }else if (component.type == 'autocomplete'){
        this.state[component.id] = {};
        this.state[component.id + '_inputValue'] = "";
      }else if (component.type == 'multiselecttable'){
        this.state[component.id] = [];
        this.state[component.id + '_storeable'] = [];
        component.columns.forEach((column, key) => {
          if (column.fieldType == 'custom'){
            if (column.component.type == 'autocomplete'){
              if (typeof column.component.dataCollection !== 'undefined'){
                this.state[column.component.id] = {};
                this.state[column.component.id + '_inputValue'] = "";
              }
            }
          }
        });
      }else{
        this.state[component.id] = "";
      }
    }, this);
	}

  componentDidMount(){
    this.initiateFormInputEvents();

    let self = this;
    $(window).unbind().on('keydown', function (event) {

      self.includeFormWindowEvents(event);
    });
    this.resetFormInputs();
    if (localStorage.getItem(this.props.formAttr.id + 'print_checked') == "true" || localStorage.getItem(this.props.formAttr.id + 'print_checked') == true) {
      this.setState({ autoPrint: true });
    } else {
      this.setState({ autoPrint: false });
    }
  }

  componentWillUnmount(){
    $(window).unbind();
  }

  initiateFormInputEvents(){
    let self = this;
    $('.inputComponent').unbind().keyup((event) => {
      let target = event.currentTarget;
      if (event.key == 'Enter'){
        //--- Enter
        event.preventDefault();
        if ($(target).hasClass('focusNextWhenPressedEnter')){
          //--- Table Inputs
          let found = false;
          let alteration = 3;
          while (alteration > 0){
            //--- For Table inputs
            let input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().find('input');
            if (alteration == 2){
              input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().find('input');
            }else if (alteration == 1){
              input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().next().find('input');
            }
            if (input.length > 0){
              input.focus();
              found = true;
              break;
            }else{
              input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().find('button.MuiIconButton-root[title="Save"]');
              if (alteration == 2){
                input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().find('button.MuiIconButton-root[title="Save"]');
              }else if (alteration == 1){
                input = $(target).closest('td.MuiTableCell-root.MuiTableCell-body').next().next().next().find('button.MuiIconButton-root[title="Save"]');
              }
              input.focus();
              if (input.length == 0){
                alteration--;
              }else{
                found = true;
                break;
              }
            }
          }

          if (found == false){
            //--- Form inputs
            let alteration = 3;
            while (alteration > 0){
              let input = $(target).closest('div.grid-item').next().find('.inputComponent').find('.MuiSelect-select.MuiSelect-selectMenu');
              if (alteration == 2){
                input = $(target).closest('div.grid-item').next().next().find('.inputComponent').find('.MuiSelect-select.MuiSelect-selectMenu');
              }else if (alteration == 1){
                input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent').find('.MuiSelect-select.MuiSelect-selectMenu');
              }
              if (input.length > 0){
                input.focus();
                break;
              }else{
                input = $(target).closest('div.grid-item').next().find('.inputComponent').find('input:enabled');
                if (alteration == 2){
                  input = $(target).closest('div.grid-item').next().next().find('.inputComponent').find('input:enabled');
                }else if (alteration == 1){
                  input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent').find('input:enabled');
                }
                if (input.length > 0){
                  input.focus();
                  break;
                }else{
                  input = $(target).closest('div.grid-item').next().find('.inputComponent').find('button:enabled');
                  if (alteration == 2){
                    input = $(target).closest('div.grid-item').next().next().find('.inputComponent').find('button:enabled');
                  }else if (alteration == 1){
                    input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent').find('button:enabled');
                  }
                  if (input.length > 0){
                    input.focus();
                    break;
                  }else{
                    input = $(target).closest('div.grid-item').next().find('.inputComponent:enabled');
                    if (alteration == 2){
                      input = $(target).closest('div.grid-item').next().next().find('.inputComponent:enabled');
                    }else if (alteration == 1){
                      input = $(target).closest('div.grid-item').next().next().next().find('.inputComponent:enabled');
                    }
                    if (input.length > 0){
                      if (input.length <= 1){
                        input.focus();
                      }else{
                        $(input[0]).focus();
                      }
                      break;
                    }else{
                      input = $(target).closest('div.grid-item').next().find('button[title="Add"]');
                      if (alteration == 2){
                        input = $(target).closest('div.grid-item').next().next().find('button[title="Add"]');
                      }else if (alteration == 1){
                        input = $(target).closest('div.grid-item').next().next().next().find('button[title="Add"]');
                      }
                      input.focus();
                      if (input.length == 0){
                        alteration--;
                      }else{
                        break;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }else if (event.key == 'F9'){
        //--- F9
        event.preventDefault();
        if ($(target).hasClass('openDateWhenF12Pressed')){
          $(target).find('button.MuiButtonBase-root').click();
        }
      }else if (event.key == 'ArrowRight'){
        //--- Right Arrow Key
        event.preventDefault();
        if ($(target).hasClass('moveNextWithArrowRightKey')){

          $(target).next().focus();
        }
      }else if (event.key == 'ArrowLeft'){
        //--- Left Arrow Key
        event.preventDefault();
        if ($(target).hasClass('moveBackWithArrowLeftKey')){

          $(target).prev().focus();
        }
      }
      //--- Form input logic check
      if ($(event.target).prop('type') == 'number'){
        let formComponentLogics = self.props.formAttr.formComponentLogics;
        if (formComponentLogics && formComponentLogics.length > 0){
          formComponentLogics.map((logic, i)=>{
            if ($(target).find('input').prop('id') == logic.entry1 || $(target).find('input').prop('id') == logic.entry2){
              let logicResult = 0;
              let entry1 = $('#'+logic.entry1).val();
              let entry2 = $('#'+logic.entry2).val();
              if (!isNaN(entry1) && !isNaN(entry2)){
                switch (logic.operand) {
                  case '+':
                    logicResult = parseFloat(entry1) + parseFloat(entry2);
                    break;
                  case '-':
                    logicResult = parseFloat(entry1) - parseFloat(entry2);
                    break;
                  case '*':
                    logicResult = parseFloat(entry1) * parseFloat(entry2);
                    break;
                  case '/':
                    logicResult = parseFloat(entry1) / parseFloat(entry2);
                    break;
                  case '%':
                    if (entry1 !== "" && entry2 !== ""){
                      let diffrentialValue = parseFloat(entry1) - parseFloat(entry2);
                      logicResult = diffrentialValue / parseFloat(entry1);
                      logicResult = logicResult * 100;
                    }
                    break;
                  case '100-%':
                    if (entry1 !== "" && entry2 !== ""){
                      let diffrentialValue = parseFloat(entry1) - parseFloat(entry2);
                      logicResult = diffrentialValue / parseFloat(entry1);
                      logicResult = logicResult * 100;
                      logicResult = 100 - logicResult;
                    }
                    break;
                  case '%*':
                    if (entry1 !== "" && entry2 !== ""){
                      entry2 = entry2 / 100;
                      logicResult = entry1 * entry2;
                    }
                    break;
                  default:
                }
                logicResult = logicResult.toFixed(2);
                if (!isNaN(logicResult) && logic.result !== $(event.target).prop('id')){
                  self.setState({[logic.result]: logicResult});
                }else{
                  self.setState({[logic.result]: 0});
                }
              }
            }
          });
        }
      }

    });

    $('button[title="Add"]').unbind().click((event) => {
      setTimeout(() => {
        $($('table').find('input')[0]).focus();
        $('table').find('input').addClass('inputComponent focusNextWhenPressedEnter');
        $('table').find('.component-datebox').find('input').val('');
        setTimeout(()=>{
          this.initiateFormInputEvents();
        }, 100);
      }, 100);
    });
  }

  handleCheckboxChange(event){
    let target = event.currentTarget;
    let id = $(target).prop('id');
    this.setState({[id]: !this.state[id]});
  }

  handleRadioboxChange(id, event){
    let target = event.currentTarget;
    this.setState({[id]: $(target).val()});
  }

  handleDateboxChange(id, date){
    $('#' + id).parent().removeClass('Mui-error');
    this.setState({[id]: date});
  }

  handleSelectboxChange(id, object, event){
    this.setState({[id]: event.props.value});
    this.setState({[id + '_inputValue']: event.props.children});
  }

  handleKeyChange(id, event){
    let target = event.currentTarget;
    this.setState({[id]: event.target.value});
  }

  handleEditInputKeyUp(event){
    let keyCode = (event.keyCode ? event.keyCode : event.which);
    let target = event.currentTarget;
    if (keyCode == '13'){

    }
  }

  handleCreate(event){
    if (this.state.formProcessing) return;
    if (this.checkForPermission('create') == false) return;
    let self = this;
    let components = this.props.formAttr.components;
    let collection = this.props.formAttr.collection;
    let extraIds = this.props.formAttr.extraIds;
    let formEntryLogics = this.props.formAttr.formEntryLogics;
    let defaultEntries = this.props.formAttr.defaultEntries;
    //Check for blank required values
    var cancelSubmition = false;
    components.forEach(function (component) {
      if (component.required){
        if (component.type == 'autocomplete'){
          if (Object.keys(this.state[component.id]).length == 0){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
          }
        }else if (component.type == 'multiselecttable'){
          if (this.state[component.id].length == 0){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
          }
        }else{
          if (this.state[component.id].trim && this.state[component.id].trim() == ''){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
          }
        }
      }
    }, this);
    if (cancelSubmition){
      Bert.alert( 'Please fill up all required fields', 'warning', 'growl-top-right' );
      return;
    }

    let componentDBLogicalRelation = [];
    //Create Data Scheme
    let dataScheme = {};
    components.forEach(function (component) {
      if (component.type == 'autocomplete'){
        let autoCompleteData = {};
        if (component.dataCollection){
          autoCompleteData = {
            data: this.state[component.id],
            value: this.state[component.id + '_inputValue'],
            id: this.state[component.id]._id,
            collectionName: component.dataCollection._name,
          }
        }else{
          autoCompleteData = {
            data: this.state[component.id],
            value: this.state[component.id + '_inputValue'],
          }
        }
        dataScheme[component.id] = autoCompleteData;
      }else if (component.type == 'multiselecttable'){
        let tableDBLogicalRelations = [];
        component.columns.forEach((column, key) => {
          if (column.DBLogicalRelations || column.accountRelations){
            let relationObject = {
              name: column.field,
            };
            if (column.DBLogicalRelations) relationObject['DBLogicalRelations'] = column.DBLogicalRelations;
            if (column.accountRelations) relationObject['accountRelations'] = column.accountRelations;
            
            tableDBLogicalRelations.push(relationObject);
          }
        });
        dataScheme[component.id] = this.state[component.id + '_storeable'];
        dataScheme[component.id + '_simple'] = this.state[component.id];
        componentDBLogicalRelation.push({
          key: component.id,
          type: component.type,
          tableDBLogicalRelations: tableDBLogicalRelations,
        });

      }else if (component.type == 'selectbox'){
        if (component.dataCollection){
          dataScheme[component.id] = this.state[component.id];
          dataScheme[component.id + '_collection'] = component.dataCollection._name;
          dataScheme[component.id + '_inputValue'] = this.state[component.id + '_inputValue'];
        }else{
          dataScheme[component.id] = this.state[component.id];
          dataScheme[component.id + '_inputValue'] = this.state[component.id + '_inputValue'];
        }

      }else if (component.type == 'datebox'){
        let date = $('#' + component.id).val().trim();
        if (date !== '') {
          date = date.split('/');
          date = date[1] + '/' + date[0] + '/' + date[2];
          date = new Date(date).toISOString();
        }
        dataScheme[component.id] = date;
      }else if (component.type == 'textbox'){
        let relationObject = {
          name: component.id,
        };
        if (component.DBLogicalRelations) relationObject['DBLogicalRelations'] = component.DBLogicalRelations;
        if (component.accountRelations) relationObject['accountRelations'] = component.accountRelations;
        if (component.number && this.state[component.id] == ''){
          dataScheme[component.id] = 0;
        }else{
          dataScheme[component.id] = this.state[component.id];
        }
        componentDBLogicalRelation.push({
          key: component.id,
          type: component.type,
          logicalRelations: relationObject,
        });
      }else{
        dataScheme[component.id] = this.state[component.id];
      }
    }, this);

    //Default Values
    if (defaultEntries && defaultEntries.length > 0){
      defaultEntries.forEach(function (defaultEntry) {
        dataScheme[defaultEntry.name] = defaultEntry.value;
      }, this);
    }

    let logicErrorReason = "";
    //--- Check for form Logics
    if (formEntryLogics && formEntryLogics.length > 0){
      formEntryLogics.forEach((logic, i) => {
        if (logic.operand && logic.entry1 && logic.entry2 && dataScheme[logic.entry1] && dataScheme[logic.entry2]){
          if (logicErrorReason !== "") logicErrorReason += ', ';
          switch (logic.operand) {
            case '<':
              if (parseFloat(dataScheme[logic.entry1]) >= parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be less then " + logic.entry2;
              break;
            case '>':
              if (parseFloat(dataScheme[logic.entry1]) <= parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be greater then " + logic.entry2;
              break;
            case '=':
            case '==':
              if (isNaN()){
                if (dataScheme[logic.entry1] !== dataScheme[logic.entry2]) cancelSubmition = true;
                logicErrorReason += logic.entry1 + " should be equal to " + logic.entry2;
              }else{
                if (parseFloat(dataScheme[logic.entry1]) !== parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
                logicErrorReason += logic.entry1 + " should be equal to " + logic.entry2;
              }
              break;
            case '<=':
              if (parseFloat(dataScheme[logic.entry1]) > parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be less then equal to " + logic.entry2;
              break;
            case '>=':
              if (parseFloat(dataScheme[logic.entry1]) < parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be greater then equal to " + logic.entry2;
              break;
            default:

          }
        }
      });
    }

    if (cancelSubmition){
      Bert.alert( logicErrorReason, 'warning', 'growl-top-right' );
      return;
    }
    
    //Options
    let options = {};
    options.appid = sessionStorage.getItem("appid");
    options.appname = sessionStorage.getItem("appname");
    options.apptype = sessionStorage.getItem("apptype");
    options.appplan = sessionStorage.getItem("appplan");
    if (this.props.formAttr.accountConfig && Object.keys(this.props.formAttr.accountConfig).length > 0) options.accountConfig = this.props.formAttr.accountConfig;
    if (extraIds && extraIds.length > 0) options.extraIds = extraIds;
    if (this.props.formAttr.uniqueKeys && this.props.formAttr.uniqueKeys.length > 0) options.uniqueKeys = this.props.formAttr.uniqueKeys;
    if (this.props.formAttr.changeOtherCollectionDataOnCreation && this.props.formAttr.changeOtherCollectionDataOnCreation.length > 0) options.changeOtherCollectionDataOnCreation = this.props.formAttr.changeOtherCollectionDataOnCreation;
    if (this.props.formAttr.userForm) options.userForm = true;
    if (componentDBLogicalRelation && componentDBLogicalRelation.length > 0) options.componentDBLogicalRelation = componentDBLogicalRelation;
    swal.fire({
      title: 'Creating Field',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        swal.showLoading();
        this.setState({ formProcessing: true });
        Meteor.call('form.create', collection._name, dataScheme, options, this.props.formAttr.id, (err, res) => {
          if (err){
            if (typeof self.props.formAttr.createErrorMessage !== 'undefined' && self.props.formAttr.createErrorMessage !== ''){
              swal.fire({
                type: 'error',
                title: 'Oops...',
                text: self.props.formAttr.createErrorMessage + ', Reason: ' + err.error ,
              }).then(()=>{
                setTimeout(()=>{
                  if (components.length > 0){
                    $('#' + components[0].id).focus();
                  }
                }, 400);
              });
            }else{
              swal.close();
            }
          }else{
            if (typeof self.props.formAttr.createSuccessMessage !== 'undefined' && self.props.formAttr.createSuccessMessage !== ''){
              if (self.checkForPermission('print') && (self.props.formAttr.printReceipt || self.props.formAttr.printInvoice) && self.state.autoPrint) {
                self.handlePrint(res);
              }
              swal.fire({
                type: 'success',
                title: 'Success',
                text: self.props.formAttr.createSuccessMessage,
              }).then(() => {
                setTimeout(()=>{
                  if (components.length > 0){
                    $('#' + components[0].id).focus();
                  }
                }, 400);
              });
            }else{
              swal.close();
            }

            this.resetFormInputs(true);
          }
          this.setState({ formProcessing: false });
        });
      },
    });
  }

  handleNew(){
    let components = this.props.formAttr.components;
    this.resetFormInputs();
  }

  resetFormInputs(skipFocus){
    let components = this.props.formAttr.components;
    this.setState({ mode: 'new', selectedEntry: {} });
    components.forEach(function (component) {
      if (component.type == 'textbox'){
        this.setState({[component.id] : ''});
      }else if(component.type == 'checkbox'){
        this.setState({[component.id] : false});
      }else if(component.type == 'autocomplete'){
        this.setState({[component.id] : []});
        this.setState({[component.id + '_inputValue'] : ""});
      }else if(component.type == 'multiselecttable'){
        this.setState({[component.id] : []});
        this.setState({[component.id + '_storeable'] : []});
      }else if (component.type == 'selectbox'){
        if (typeof component.dataCollection !== 'undefined'){
          this.state[component.id] = "";
          this.state[component.id + '_inputValue'] = "";
        }else{
          if (component.data.length > 0){
            this.state[component.id] = component.data[0].value;
            this.state[component.id + '_inputValue'] = component.data[0].value;
          }else{
            this.state[component.id] = "";
            this.state[component.id + '_inputValue'] = "";
          }
          this.state[component.id + '_collection'] = component.dataCollection;
        }
      }
    }, this);
    $('.inputComponent').parent().removeClass('Mui-error');
    if (typeof skipFocus == 'undefined' || skipFocus == false){
      if (components.length > 0){
        $('#' + components[0].id).focus();
      }
    }
  }

  handleUpdate(event){
    if (this.props.formAttr.restrictUpdate) return ;
    if (this.state.formProcessing) return;
    if (this.checkForPermission('update') == false) return;
    let self = this;
    let components = this.props.formAttr.components;
    let collection = this.props.formAttr.collection;
    let extraIds = this.props.formAttr.extraIds;
    let formEntryLogics = this.props.formAttr.formEntryLogics;
    //Check for blank required values
    var cancelSubmition = false;
    components.forEach(function (component) {
      if (component.required){
        if (component.type == 'autocomplete'){
          if (Object.keys(this.state[component.id]).length == 0){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
          }
        }else if (component.type == 'multiselecttable'){
          if (this.state[component.id].length == 0){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
          }
        }else{
          if (this.state[component.id].trim && this.state[component.id].trim() == ''){
            cancelSubmition = true;
            $('#' + component.id).parent().addClass('Mui-error');
          }
        }
      }
    }, this);
    if (cancelSubmition){
      Bert.alert( 'Please fill up all required fields', 'warning', 'growl-top-right' );
      return;
    }

    let componentDBLogicalRelation = [];
    //Create Data Scheme
    let dataScheme = _.clone(this.state.selectedEntry);
    components.forEach(function (component) {
      if (component.type == 'autocomplete'){
        let autoCompleteData = {};
        if (component.dataCollection){
          autoCompleteData = {
            data: this.state[component.id],
            value: this.state[component.id + '_inputValue'],
            id: this.state[component.id]._id,
            collectionName: component.dataCollection._name,
          }
        }else{
          autoCompleteData = {
            data: this.state[component.id],
            value: this.state[component.id + '_inputValue'],
          }
        }
        dataScheme[component.id] = autoCompleteData;
      }else if (component.type == 'multiselecttable'){
        let tableDBLogicalRelations = [];
        component.columns.forEach((column, key) => {
          if (column.DBLogicalRelations || column.accountRelations){
            let relationObject = {
              name: column.field,
            };
            if (column.DBLogicalRelations) relationObject['DBLogicalRelations'] = column.DBLogicalRelations;
            if (column.accountRelations) relationObject['accountRelations'] = column.accountRelations;
            
            tableDBLogicalRelations.push(relationObject);
          }
        });
        dataScheme[component.id] = this.state[component.id + '_storeable'];
        dataScheme[component.id + '_simple'] = this.state[component.id];
        componentDBLogicalRelation.push({
          key: component.id,
          type: component.type,
          tableDBLogicalRelations: tableDBLogicalRelations,
        });
      }else if (component.type == 'selectbox'){
        let selectBoxData = {};
        if (component.dataCollection){
          dataScheme[component.id] = this.state[component.id];
          dataScheme[component.id + '_collection'] = component.dataCollection._name;
          dataScheme[component.id + '_inputValue'] = this.state[component.id + '_inputValue'];
        }else{
          dataScheme[component.id] = this.state[component.id];
          dataScheme[component.id + '_inputValue'] = this.state[component.id + '_inputValue'];
        }

      }else if (component.type == 'datebox'){
        let date = $('#' + component.id).val().trim();
        if (date !== '') {
          date = date.split('/');
          date = date[1] + '/' + date[0] + '/' + date[2];
          date = new Date(date).toISOString();
        }
        dataScheme[component.id] = date;
      }else if (component.type == 'textbox'){
        let relationObject = {
          name: component.id,
        };
        if (component.DBLogicalRelations) relationObject['DBLogicalRelations'] = component.DBLogicalRelations;
        if (component.accountRelations) relationObject['accountRelations'] = component.accountRelations;
        if (component.number && this.state[component.id] == ''){
          dataScheme[component.id] = 0;
        }else{
          dataScheme[component.id] = this.state[component.id];
        }
        componentDBLogicalRelation.push({
          key: component.id,
          type: component.type,
          logicalRelations: relationObject,
        });
      }else{
        dataScheme[component.id] = this.state[component.id];
      }
    }, this);

    let logicErrorReason = "";
    //--- Check for form Logics
    if (formEntryLogics && formEntryLogics.length > 0){
      formEntryLogics.forEach((logic, i) => {
        if (logic.operand && logic.entry1 && logic.entry2 && dataScheme[logic.entry1] && dataScheme[logic.entry2]){
          if (logicErrorReason !== "") logicErrorReason += ', ';
          switch (logic.operand) {
            case '<':
              if (parseFloat(dataScheme[logic.entry1]) >= parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be less then " + logic.entry2;
              break;
            case '>':
              if (parseFloat(dataScheme[logic.entry1]) <= parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be greater then " + logic.entry2;
              break;
            case '=':
            case '==':
              if (isNaN()){
                if (dataScheme[logic.entry1] !== dataScheme[logic.entry2]) cancelSubmition = true;
                logicErrorReason += logic.entry1 + " should be equal to " + logic.entry2;
              }else{
                if (parseFloat(dataScheme[logic.entry1]) !== parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
                logicErrorReason += logic.entry1 + " should be equal to " + logic.entry2;
              }
              break;
            case '<=':
              if (parseFloat(dataScheme[logic.entry1]) > parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be less then equal to " + logic.entry2;
              break;
            case '>=':
              if (parseFloat(dataScheme[logic.entry1]) < parseFloat(dataScheme[logic.entry2])) cancelSubmition = true;
              logicErrorReason += logic.entry1 + " should be greater then equal to " + logic.entry2;
              break;
            default:

          }
        }
      });
    }

    if (cancelSubmition){
      Bert.alert( logicErrorReason, 'warning', 'growl-top-right' );
      return;
    }

    //--- options
    let options = {};
    options.appid = sessionStorage.getItem("appid");
    options.appname = sessionStorage.getItem("appname");
    options.apptype = sessionStorage.getItem("apptype");
    options.appplan = sessionStorage.getItem("appplan");
    if (this.props.formAttr.userForm) options.userForm = true;
    if (this.props.formAttr.accountConfig && Object.keys(this.props.formAttr.accountConfig).length > 0) options.accountConfig = this.props.formAttr.accountConfig;
    if (this.props.formAttr.uniqueKeys && this.props.formAttr.uniqueKeys.length > 0) options.uniqueKeys = this.props.formAttr.uniqueKeys;
    if (componentDBLogicalRelation && componentDBLogicalRelation.length > 0) options.componentDBLogicalRelation = componentDBLogicalRelation;
    swal.fire({
      title: 'Updating Field',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        swal.showLoading();
        this.setState({ formProcessing: true });
        Meteor.call('form.update', collection._name, this.state.selectedEntry._id, dataScheme, options, this.props.formAttr.id, (err, res) => {
          if (err){
            if (typeof self.props.formAttr.updateErrorMessage !== 'undefined' && self.props.formAttr.updateErrorMessage !== ''){
              swal.fire({
                type: 'error',
                title: 'Oops...',
                text: self.props.formAttr.updateErrorMessage + ', Reason: ' + err.error ,
              }).then(()=>{
                setTimeout(()=>{
                  if (components.length > 0){
                    $('#' + components[0].id).focus();
                  }
                }, 400);
              });
            }else{
              swal.close();
            }
          }else{
            if (typeof self.props.formAttr.updateSuccessMessage !== 'undefined' && self.props.formAttr.updateSuccessMessage !== ''){
              swal.fire({
                type: 'success',
                title: 'Success',
                text: self.props.formAttr.updateSuccessMessage,
                focusConfirm: true,
              }).then(()=>{
                setTimeout(()=>{
                  if (components.length > 0){
                    $('#' + components[0].id).focus();
                  }
                }, 400);
              });
            }else{
              swal.close();
            }

            this.resetFormInputs(true);
          }
          this.setState({ formProcessing: false });
        });
      },
    });
  }

  handleDelete(event){
    if (this.state.formProcessing) return;
    if (this.checkForPermission('delete') == false) return;
    let self = this;
    let components = this.props.formAttr.components;
    let collection = this.props.formAttr.collection;
    let extraIds = this.props.formAttr.extraIds;
    let deleteCheck = this.props.formAttr.chechForExistanceBeforeDeleteInCollections;
    let options = {};
    options.appid = sessionStorage.getItem("appid");
    options.appname = sessionStorage.getItem("appname");
    options.apptype = sessionStorage.getItem("apptype");
    options.appplan = sessionStorage.getItem("appplan");
    if (this.props.formAttr.accountConfig && Object.keys(this.props.formAttr.accountConfig).length > 0) options.accountConfig = this.props.formAttr.accountConfig;
    if (deleteCheck && deleteCheck.length > 0){
      options.deleteCheck = deleteCheck;
    }


    let componentDBLogicalRelation = [];
    //Create Data Scheme
    components.forEach(function (component) {
      if (component.type == 'autocomplete'){

      }else if (component.type == 'multiselecttable'){
        let tableDBLogicalRelations = [];
        component.columns.forEach((column, key) => {
          if (column.DBLogicalRelations || column.accountRelations){
            let relationObject = {
              name: column.field,
            };
            if (column.DBLogicalRelations) relationObject['DBLogicalRelations'] = column.DBLogicalRelations;
            if (column.accountRelations) relationObject['accountRelations'] = column.accountRelations;
            
            tableDBLogicalRelations.push(relationObject);
          }
        });
        componentDBLogicalRelation.push({
          key: component.id,
          type: component.type,
          tableDBLogicalRelations: tableDBLogicalRelations,
        });
      }else if (component.type == 'selectbox'){

      }else if (component.type == 'datebox'){

      }else if (component.type == 'textbox'){
        let relationObject = {
          name: component.id,
        };
        if (component.DBLogicalRelations) relationObject['DBLogicalRelations'] = component.DBLogicalRelations;
        if (component.accountRelations) relationObject['accountRelations'] = component.accountRelations;
        componentDBLogicalRelation.push({
          key: component.id,
          type: component.type,
          logicalRelations: relationObject,
        });
      }else{

      }
    }, this);

    if (componentDBLogicalRelation && componentDBLogicalRelation.length > 0) options.componentDBLogicalRelation = componentDBLogicalRelation;
    if (this.props.formAttr.changeOtherCollectionDataOnDelete && this.props.formAttr.changeOtherCollectionDataOnDelete.length > 0) options.changeOtherCollectionDataOnDelete = this.props.formAttr.changeOtherCollectionDataOnDelete;
    if (this.props.formAttr.userForm) options.userForm = true;

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        swal.fire({
          title: 'Deleting Field',
          allowOutsideClick: false,
          allowEscapeKey: false,
          onBeforeOpen: () => {
            swal.showLoading();
            this.setState({ formProcessing: true });
            Meteor.call('form.delete', collection._name, this.state.selectedEntry._id, options, this.props.formAttr.id, (err, res) => {
              if (err){
                if (typeof self.props.formAttr.updateErrorMessage !== 'undefined' && self.props.formAttr.updateErrorMessage !== ''){
                  swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: self.props.formAttr.updateErrorMessage + ', Reason: ' + err.error ,
                  }).then(()=>{
                    setTimeout(()=>{
                      if (components.length > 0){
                        $('#' + components[0].id).focus();
                      }
                    }, 400);
                  });
                }else{
                  swal.close();
                }
              }else{
                if (typeof self.props.formAttr.updateSuccessMessage !== 'undefined' && self.props.formAttr.updateSuccessMessage !== ''){
                  swal.fire({
                    type: 'success',
                    title: 'Success',
                    text: self.props.formAttr.updateSuccessMessage,
                  }).then(()=>{
                    setTimeout(()=>{
                      if (components.length > 0){
                        $('#' + components[0].id).focus();
                      }
                    }, 400);
                  });
                }else{
                  swal.close();
                }

                this.resetFormInputs(true);
              }
              this.setState({ formProcessing: false });
            });
          },
        });
      }
    });
  }

  handlePrint(res){
    if ((typeof this.props.formAttr.printReceipt == 'undefined' || this.props.formAttr.printReceipt == false) && (typeof this.props.formAttr.printInvoice == 'undefined' || this.props.formAttr.printInvoice == false)) {
      if (window.debudMode) console.log('Form configuration doe not allow for printing');
      return;
    }
    var divToPrint=document.getElementById(this.props.formAttr.collection._name + '_receipt');
    var newPrintWindow= window.open("");
    var data = false;
    if (res) data = res;
    if (this.props.formAttr.printReceipt) {
      var printContent = this.props.formAttr.printReceipt(this, res);
    } else if (this.props.formAttr.printInvoice) {
      var printContent = this.props.formAttr.printInvoice(this, res);
    }
    //--- adding credit in footer
    printContent += '<hr><div style="width: 100%;text-align: center;margin-top: -8px;" ><span style="font-size:13px;" >QUANTUM by Evarosoft (www.evarosoft.com)</span></div>';
    newPrintWindow.document.write(printContent);
    newPrintWindow.print();
    newPrintWindow.close();
  }

  getTextboxType(component){
    if (component.password){
      return 'password';
    }else if(component.number){
      return 'number';
    }else{
      return 'text';
    }
  }
  handleOpenEditDialog(){
    if (this.props.formAttr.openEditDialogConfig.detailedSearch){
      this.setState({ openEditDialog: true });
      let self = this;
      let tableId = '#' + self.props.formAttr.collection._name + '_search_table';
    }
  }
  // handleCloseEditDialog(){
  //   this.setState({ openEditDialog: false });
  //   $('#' + this.props.formAttr.collection._name + '_search_table').find('.tableRow').unbind();
  //   let self = this;
  //   $(window).unbind().on('keydown', function (event) {
  //     self.includeFormWindowEvents(event);
  //   });
  // }
  handleCloseEditDialogByRef(parent){
    if (parent.target){
      parent = this;
    }
    parent.setState({ openEditDialog: false });
    $('#' + parent.props.formAttr.collection._name + '_search_table').find('.tableRow').unbind();
    let self = parent;
    $(window).unbind().on('keydown', function (event) {
      self.includeFormWindowEvents(event);
    });
  }
  handleChangeSearchLogic(searchLogic, parent){
    let detailedSearchLogic = parent.props.formAttr.openEditDialogConfig.detailedSearchLogic;
    if (detailedSearchLogic && Object.keys(detailedSearchLogic).length > 0 && Object.keys(searchLogic).length > 0){
      for (var i=0;i<Object.keys(detailedSearchLogic).length;i++){
        searchLogic[Object.keys(detailedSearchLogic)[i]] = detailedSearchLogic[Object.keys(detailedSearchLogic)[i]];
      }
    }
    parent.setState({ searchLogic: searchLogic });
    let components = parent.props.formAttr.components;
    if (components.length > 0){
      $('#' + components[0].id).focus();
    }
    swal.close();
  }
  handleDataSelection(data, parent){
    if (parent.props.formAttr.restrictUpdate) return ;
    if (window.debugMode) console.log("Detailed Search Fetched Data: ", data);
    if (data){
      parent.setState({ selectedEntry: data });
      for (var i=0;i<Object.keys(data).length;i++){
        var dataEntity = data[Object.keys(data)[i]];
        var dataKey = Object.keys(data)[i];
        if (Object.keys(dataEntity) && Object.keys(dataEntity)[0] == 'data'){
          parent.setState({ [dataKey]: dataEntity.data });
          parent.setState({ [dataKey + '_inputValue']: dataEntity.value });
        }else if (dataEntity && dataEntity.length > 0 && typeof dataEntity == 'object'){
          if (dataKey.indexOf('_simple') >= 0){
            parent.setState({ [dataKey.replace('_simple', '')]: dataEntity });
          }else{
            parent.setState({ [dataKey + '_storeable']: dataEntity });
          }
        }else{
          parent.setState({ [dataKey]: dataEntity });
        }
      }
      parent.setState({ mode: 'update' });
    }
    let components = parent.props.formAttr.components;
    if (components.length > 0){
      $('#' + components[0].id).focus();
    }
  }

  includeFormWindowEvents(event){
    if (this.state.mode == 'new'){
      if (event.key == 's'){
        if (event.originalEvent.ctrlKey){
          event.preventDefault();
          this.handleCreate();
        }
      }
    }else if (this.state.mode == 'update'){
      if (event.key == 's'){
        if (event.originalEvent.ctrlKey){
          event.preventDefault();
          this.handleUpdate();
        }
      }else if (event.key == 'd'){
        if (event.originalEvent.ctrlKey){
          event.preventDefault();
          this.handleDelete();
        }
      }
    }
    if (event.key == 'F12'){
      event.preventDefault();
      let searchInputId = '#' + this.props.formAttr.id + '_instant_search';
      if ($(searchInputId).is(':focus')){
        //Search input is already focused
        this.handleOpenEditDialog();
      }else{
        //Search input is not focused
        $(searchInputId).focus();
      }
    }else if (event.key == 'Escape'){
      event.preventDefault();
      this.handleNew();
    }
  }

  checkForPermission(type){
    let allowed = false;
    switch (type) {
      case 'create':
        allowed = (this.props.rolePermission && (this.props.rolePermission.allAccess || (this.props.rolePermission[this.props.formAttr.id.replace('Form_', '') + '_create'] || (this.props.formAttr.restrictUpdate && this.props.rolePermission[this.props.formAttr.id.replace('Form_', '') + '_access']))));
        break;
      case 'update':
        allowed = (this.props.rolePermission && (this.props.rolePermission.allAccess || this.props.rolePermission[this.props.formAttr.id.replace('Form_', '') + '_update']));
        break;
      case 'delete':
        allowed = (this.props.rolePermission && (this.props.rolePermission.allAccess || this.props.rolePermission[this.props.formAttr.id.replace('Form_', '') + '_delete']));
        break;
      case 'print':
        allowed = (this.props.rolePermission && (this.props.rolePermission.allAccess || this.props.rolePermission[this.props.formAttr.id.replace('Form_', '') + '_print']));
        break;
    
      default:
        break;
    }
    if (allowed){
      allowed = true;
    }else{
      allowed = false;
    }
    
    return allowed;
  }

  render() {
    let self = this;
    let components = this.props.formAttr.components;
    let collection = this.props.formAttr.collection;
    let apps = this.props.apps;
    return (
      <Styles>
        <Paper className="container-paper">
        <Container maxWidth="lg" className="container">
        <Grid container >
          <Grid item xs={12} md={6} ls={6} className="grid-item">
            <Typography variant="h5">
              {this.props.formAttr.title}
            </Typography>
          </Grid>
          {
            (this.props.formAttr.restrictUpdate) ? "" :
            <Grid item xs={12} md={6} ls={6} className="grid-item">


              <Paper>
                <Grid container className="searchInputContainer" >
                  {
                    (this.props.formAttr.openEditDialogConfig.detailedSearch) ?
                    <Grid item xs={2} md={2} ls={2} >
                      <IconButton onClick={this.handleOpenEditDialog.bind(this)} >
                        <FaSearch fontSize="medium" />
                      </IconButton>
                    </Grid>
                    :
                    <Grid item xs={2} md={2} ls={2} >
                      <IconButton >
                        <FaPen fontSize="medium" />
                      </IconButton>
                    </Grid>
                  }
                  {
                    (this.props.formAttr.openEditDialogConfig.detailedSearch) ?
                    <Grid item xs={10} md={10} ls={10} >
                      <InputBase
                        id={this.props.formAttr.id + '_instant_search'}
                        className="instant_search_item_input"
                        placeholder={"Search Entry by " + this.props.formAttr.searchKey}
                        value={this.state.instantSearchInputValue}
                        onChange={e => this.setState({ instantSearchInputValue: e.target.value })}
                        onKeyUp={(e)=>{
                          if (e.key == 'Enter'){
                            if (typeof this.props.formAttr.collection !== 'undefined' && this.state.instantSearchInputValue && this.state.instantSearchInputValue.trim() !== ''){
                              if (window.debugMode) console.log("Search Input Value: ", this.state.instantSearchInputValue);
                              var data = this.props.formAttr.collection.findOne({ eid: this.state.instantSearchInputValue, appid: sessionStorage.getItem("appid"), appunit: sessionStorage.getItem("appunit") });
                              if (window.debugMode) console.log('Search Data: ', data);
                              if (data){
                                this.setState({ selectedEntry: data });
                                for (var i=0;i<Object.keys(data).length;i++){
                                  var dataEntity = data[Object.keys(data)[i]];
                                  var dataKey = Object.keys(data)[i];
                                  if (Object.keys(dataEntity) && Object.keys(dataEntity)[0] == 'data'){
                                    this.setState({ [dataKey]: dataEntity.data });
                                    this.setState({ [dataKey + '_inputValue']: dataEntity.value });
                                  }else if (dataEntity && dataEntity.length > 0 && typeof dataEntity == 'object'){
                                    if (dataKey.indexOf('_simple') >= 0){
                                      this.setState({ [dataKey.replace('_simple', '')]: dataEntity });
                                    }else{
                                      this.setState({ [dataKey + '_storeable']: dataEntity });
                                    }
                                  }else{
                                    this.setState({ [dataKey]: dataEntity });
                                  }
                                }
                                this.setState({ mode: 'update' });
                                this.setState({ instantSearchInputValue: '' });
                              }
                              let components = this.props.formAttr.components;
                              if (components.length > 0){
                                $('#' + components[0].id).focus();
                              }
                            }
                          }
                        }}
                      />
                    </Grid>
                    :
                    <Grid item xs={10} md={10} ls={10} >
                      <Autocomplete
                        id={this.props.formAttr.id + '_instant_search'}
                        items={(typeof this.props.formAttr.collection !== 'undefined' && this.props.formInstantSearchData) ? this.props.formInstantSearchData : [] }
                        shouldItemRender={(item, value) => {
                          let instantSearchConfig = this.props.formAttr.instantSearchConfig;
                          let searchStringDBKeys = instantSearchConfig.searchStringDBKeys;
                          let searchString = "";
                          for (var i=0;i<searchStringDBKeys.length;i++){
                            if (i!== 0) searchString += " ";
                            searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                          }
                          return (
                            searchString.toLowerCase().indexOf(value.toLowerCase()) > -1
                          );
                        }}
                        getItemValue={item => {
                          let instantSearchConfig = this.props.formAttr.instantSearchConfig;
                          let searchStringDBKeys = instantSearchConfig.searchStringDBKeys;
                          let searchString = "";
                          for (var i=0;i<searchStringDBKeys.length;i++){
                            if (i!== 0) searchString += " ";
                            searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                          }
                          return searchString;
                        }}
                        renderItem={(item, highlighted) => {
                          let instantSearchConfig = this.props.formAttr.instantSearchConfig;
                          let searchStringDBKeys = instantSearchConfig.searchStringDBKeys;
                          let searchString = "";
                          for (var i=0;i<searchStringDBKeys.length;i++){
                            if (i!== 0) searchString += " ";
                            searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                          }
                          return (
                            <MenuItem
                              key={item._id}
                              style={{ backgroundColor: highlighted ? '#eee' : 'white'}}
                            >
                            {searchString}
                            </MenuItem>
                          );
                        }
                        }
                        value={this.state.instantSearchInputValue}
                        onChange={e => this.setState({ instantSearchInputValue: e.target.value })}
                        onSelect={(value, item) => {
                          let data = item;
                          this.setState({ selectedEntry: data });
                          for (var i=0;i<Object.keys(data).length;i++){
                            var dataEntity = data[Object.keys(data)[i]];
                            var dataKey = Object.keys(data)[i];
                            if (Object.keys(dataEntity) && Object.keys(dataEntity)[0] == 'data'){
                              this.setState({ [dataKey]: dataEntity.data });
                              this.setState({ [dataKey + '_inputValue']: dataEntity.value });
                            }else if (dataEntity && dataEntity.length > 0 && typeof dataEntity == 'object'){
                              if (dataKey.indexOf('_simple') >= 0){
                                this.setState({ [dataKey.replace('_simple', '')]: dataEntity });
                              }else{
                                this.setState({ [dataKey + '_storeable']: dataEntity });
                              }
                            }else{
                              this.setState({ [dataKey]: dataEntity });
                            }
                          }
                          this.setState({ mode: 'update' });
                          this.setState({ instantSearchInputState: item });
                          let components = this.props.formAttr.components;
                          if (components.length > 0){
                            $('#' + components[0].id).focus();
                          }
                        }}
                        renderInput={params =>{
                          return <input {...params}  placeholder={"Search Entry By " + this.props.formAttr.title}  className="instant_search_item_input" id={this.props.formAttr.id + '_instant_search'} />
                        }}
                        wrapperStyle={{
                          display: 'inline-block',
                          width: '95%',
                        }}
                        menuStyle={{
                          borderRadius: '3px',
                          boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                          background: 'rgba(255, 255, 255, 0.9)',
                          padding: '2px 0',
                          fontSize: '90%',
                          position: 'fixed',
                          overflow: 'auto',
                          maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                          zIndex: '9999999',
                        }}
                      />
                    </Grid>
                  }
                </Grid>
              </Paper>
              <SearchDialog
                open={this.state.openEditDialog}
                parent={this}

                handleOpenEditDialog={this.handleOpenEditDialog}
                handleChangeSearchLogic={this.handleChangeSearchLogic}
                handleDataSelection={this.handleDataSelection}
                handleCloseEditDialogByRef={this.handleCloseEditDialogByRef}
                searchLogic={this.state.searchLogic}
                collection={this.props.formAttr.collection}
                config={this.props.formAttr.openEditDialogConfig}
                inputVarient={this.props.formAttr.inputVarient}
              />

            </Grid>
          }

        </Grid>
        <Grid container >
          {components.map(function (component, parentKey) {
            if (component.type == 'blank'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                </Grid>
              );
            }else if (component.type == 'text'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                    <Typography id={component.id} variant={component.tag}>
                      {component.label}
                    </Typography>
                </Grid>
              );
            }else if (component.type == 'custom'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                    {component.component(component, this)}
                </Grid>
              );
            }else if (component.type == 'textbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                    <TextField
                      id={component.id}
                      label={component.label}
                      disabled={component.disabled}
                      type={this.getTextboxType(component)}
                      margin="normal"
                      className="inputComponent component-textbox focusNextWhenPressedEnter"
                      fullWidth
                      value={this.state[component.id]}
                      onChange={this.handleKeyChange.bind(this, component.id)}
                      required={component.required}
                      InputProps={{
                        readOnly: component.readOnly ? true : false,
                      }}
                      variant={this.props.formAttr.inputVarient}
                      defaultValue={this.props.formAttr.defaultValue}
                    />

                </Grid>
              );
            }else if (component.type == 'checkbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state[component.id]}
                          onChange={this.handleCheckboxChange.bind(this)}
                          id={component.id}
                          disabled={component.disabled}
                          value={component.id}
                          color={component.color}
                          className="inputComponent component-checkbox focusNextWhenPressedEnter"
                        />
                      }
                      label={component.label}
                    />
                  </FormGroup>
                </Grid>
              );
            }else if (component.type == 'datebox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Grid container justify="space-around">
                    <KeyboardDatePicker
                      variant="inline"
                      format={component.format}
                      required={component.required}
                      disabled={component.disabled}
                      margin="normal"
                      className="inputComponent component-datebox focusNextWhenPressedEnter openDateWhenF12Pressed"
                      fullWidth
                      id={component.id}
                      label={component.label}
                      value={this.state[component.id]}
                      onChange={this.handleDateboxChange.bind(this, component.id)}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                      autoOk={true}
                      invalidDateMessage='Invalid Date Format/Empty'
                      variant={this.props.formAttr.inputVarient}
                    />
                  </Grid>
                </MuiPickersUtilsProvider>
                </Grid>
              );
            }else if (component.type == 'radiobox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                <FormControl component="fieldset">
                  <FormLabel component="legend">{component.label}</FormLabel>
                  <RadioGroup aria-label="gender" id={component.id} required={component.required} name="gender1" value={this.state[component.id]} onChange={this.handleRadioboxChange.bind(this, component.id)}>
                  {component.options.map(function (option, key) {
                    return (
                      <FormControlLabel className="inputComponent component-radiobox focusNextWhenPressedEnter" disabled={component.disabled} value={option.value} control={<Radio />} label={option.label} key={key} />
                    );
                  }, this)}
                  </RadioGroup>
                </FormControl>
                </Grid>
              );
            }else if (component.type == 'selectbox'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                  <FormControl fullWidth required={component.required} variant={this.props.formAttr.inputVarient} disabled={component.disabled}>
                    <InputLabel id={component.id + '-label'}>{component.label}</InputLabel>
                    <Select
                      labelid={component.id + '-label'}
                      id={component.id}
                      value={this.state[component.id]}
                      onChange={this.handleSelectboxChange.bind(this, component.id)}
                      fullWidth
                      className="inputComponent component-selectbox focusNextWhenPressedEnter"
                    >
                      { (typeof component.dataCollection !== 'undefined' && this.props.componentData[component.id]) ?
                        this.props.componentData[component.id].map(function (entity, key) {
                          return (
                            <MenuItem value={entity[component.dataCollectionValue]} key={key} >{entity[component.dataCollectionLabel]}</MenuItem>
                          );
                        }, this)
                        :
                        component.data.map(function (entity, key) {
                        return (
                          <MenuItem value={entity.value} key={key} >{entity.label}</MenuItem>
                        );
                      }, this)}
                    </Select>
                  </FormControl>
                </Grid>
              );
            }else if (component.type == 'autocomplete'){
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item autocomplete-style">
                  <Typography variant="caption" display="block" gutterBottom>
                    {component.label} {(component.required ? ' *' : '')}
                  </Typography>
                  <Autocomplete
                    id={component.id}
                    items={(typeof component.dataCollection !== 'undefined') ? this.props.componentData[component.id] : component.data }
                    shouldItemRender={(item, value) => {
                      let searchStringDBKeys = component.searchKeys;
                      let searchString = "";
                      for (var i=0;i<searchStringDBKeys.length;i++){
                        if (i!== 0) searchString += " ";
                        searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                      }
                      return (
                        searchString.toLowerCase().indexOf(value.toLowerCase()) > -1
                      );
                    }}
                    getItemValue={item => {
                      let searchStringDBKeys = component.searchKeys;
                      let searchString = "";
                      for (var i=0;i<searchStringDBKeys.length;i++){
                        if (i!== 0) searchString += " ";
                        searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                      }
                      return searchString;
                    }}
                    renderItem={(item, highlighted) => {
                      let searchStringDBKeys = component.searchKeys;
                      let searchString = "";
                      for (var i=0;i<searchStringDBKeys.length;i++){
                        if (i!== 0) searchString += " ";
                        searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                      }
                      return (
                        <MenuItem
                          key={item[component.uniqueKey]}
                          style={{ backgroundColor: highlighted ? '#eee' : 'white'}}
                        >
                        <Grid container className="autocomplete-extrainfocontainer" >
                          <Grid >
                            {searchString}
                          </Grid>
                          <Grid className="autocomplete-extrainforow" >
                            {
                              (component.extraInformation && component.extraInformation.length > 0) ?
                              component.extraInformation.map((info, j) => {
                                  if (item && typeof item[info.key] !== 'undefined'){
                                    return (
                                      <Chip color="primary" size="small" label={info.label + ': ' + item[info.key]} key={j} className="autocomplete-extrainfochip" />
                                    );
                                  }
                                })
                              :
                              ""
                            }
                          </Grid>
                        </Grid>
                        </MenuItem>
                      );
                    }
                    }
                    value={this.state[component.id + '_inputValue']}
                    onChange={(e) =>{ 
                      this.setState({[component.id + '_inputValue']: event.target.value});
                      if (event.target.value == ""){
                        this.setState({[component.id]: ""});
                        this.setState({[component.id + '_inputValue']: ""});
                        if (component.onSelectCallback){
                          component.onSelectCallback(item, this);
                        }
                      }
                    }}
                    onSelect={(value, item) => {
                      let data = item;
                      this.setState({[component.id]: item});
                      this.setState({[component.id + '_inputValue']: value});
                      if (component.onSelectCallback){
                        component.onSelectCallback(item, this);
                      }
                    }}
                    renderInput={params =>{
                      return <input {...params} id={component.id} className="inputComponent component-autocomplete focusNextWhenPressedEnter" label="" disabled={component.disabled} required={component.required} variant={this.props.formAttr.inputVarient}/>
                    }}
                    wrapperStyle={{
                      display: 'inline-block',
                      width: '100%',
                    }}
                    menuStyle={{
                      borderRadius: '3px',
                      boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                      background: 'rgba(255, 255, 255, 0.9)',
                      padding: '2px 0',
                      fontSize: '90%',
                      position: 'fixed',
                      overflow: 'auto',
                      maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                      zIndex: '9999999',
                    }}
                  />
              </Grid>
              );
            }else if (component.type == 'multiselecttable'){
              let tableColumns = [];
              component.columns.map((column, key) =>{
                if (column.fieldType == 'field'){
                  let field = {
                    title: column.title,
                    field: column.field,
                  };
                  if (typeof column.type !== 'undefined') field['type'] = column.type;
                  if (typeof column.readOnly !== 'undefined') field['readOnly'] = column.readOnly;
                  if (typeof column.editable !== 'undefined') field['editable'] = column.editable;
                  if (typeof column.emptyValue !== 'undefined') field['emptyValue'] = column.emptyValue;
                  if (typeof column.hidden !== 'undefined') field['hidden'] = column.hidden;
                  tableColumns.push(field);
                }else if(column.fieldType == 'custom'){
                  if (column.component.type == 'autocomplete'){
                    let subComponent = column.component;
                    let field = {
                      title: column.title, field: column.component.id,
                      editComponent: props => (
                        <Autocomplete
                          id={subComponent.id}
                          items={(typeof subComponent.dataCollection !== 'undefined') ? this.props.componentData[subComponent.id] : [] }
                          shouldItemRender={(item, value) => {
                            let searchStringDBKeys = subComponent.searchKeys;
                            let searchString = "";
                            for (var i=0;i<searchStringDBKeys.length;i++){
                              if (i!== 0) searchString += " ";
                              searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                            }
                            return (
                              searchString.toLowerCase().indexOf(value.toLowerCase()) > -1
                            );
                          }}
                          getItemValue={item => {
                            let searchStringDBKeys = subComponent.searchKeys;
                            let searchString = "";
                            for (var i=0;i<searchStringDBKeys.length;i++){
                              if (i!== 0) searchString += " ";
                              searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                            }
                            return searchString;
                          }}
                          renderItem={(item, highlighted) => {
                            let searchStringDBKeys = subComponent.searchKeys;
                            let searchString = "";
                            for (var i=0;i<searchStringDBKeys.length;i++){
                              if (i!== 0) searchString += " ";
                              searchString += getDataValueByKey(item, searchStringDBKeys[i]);
                            }
                            return (
                              <MenuItem
                                key={item[subComponent.uniqueKey]}
                                style={{ backgroundColor: highlighted ? '#eee' : 'white'}}
                              >
                              <Grid container className="autocomplete-extrainfocontainer" >
                                <Grid >
                                  {searchString}
                                </Grid>
                                <Grid className="autocomplete-extrainforow" >
                                  {
                                    (subComponent.extraInformation && subComponent.extraInformation.length > 0) ?
                                      subComponent.extraInformation.map((info, j) => {
                                        if (item && typeof item[info.key] !== 'undefined'){
                                          return (
                                            <Chip color="primary" size="small" label={info.label + ': ' + item[info.key]} key={j} className="autocomplete-extrainfochip" />
                                          );
                                        }
                                      })
                                    :
                                    ""
                                  }
                                </Grid>
                              </Grid>
                              </MenuItem>
                            );
                          }
                          }
                          value={props.value}
                          onChange={e => props.onChange(e.target.value)}
                          onSelect={(value, item) => {
                            props.onChange(value);
                            this.setState({[subComponent.id]: item});
                            this.setState({[subComponent.id + '_inputValue']: value});
                            setTimeout(()=>{
                              $('#'+subComponent.id).focus();
                              this.initiateFormInputEvents();
                            }, 100);
                          }}
                          renderInput={params =>{
                            return <input {...params} id={subComponent.id} className="inputComponent component-autocomplete focusNextWhenPressedEnter" label="" variant={this.props.formAttr.inputVarient}/>
                          }}
                          wrapperStyle={{
                            display: 'inline-block',
                            width: '100%',
                          }}
                          menuStyle={{
                            borderRadius: '3px',
                            boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                            background: 'rgba(255, 255, 255, 0.9)',
                            padding: '2px 0',
                            fontSize: '90%',
                            position: 'fixed',
                            overflow: 'auto',
                            maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                            zIndex: '9999999',
                          }}
                        />
                      )
                    };
                    tableColumns.push(field);
                  } else if (column.component.type == 'datebox'){
                    let subComponent = column.component;
                    let field = {
                      title: column.title, field: column.component.id,
                      editComponent: props => (
                        <Grid item className="grid-item">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <Grid container justify="space-around">
                                    <KeyboardDatePicker
                                        disableToolbar
                                        variant={this.props.formAttr.inputVarient}
                                        format={subComponent.format}
                                        margin="normal"
                                        id={subComponent.id}
                                        label={subComponent.label}
                                        value={props.value}
                                        onChange={(date) => {
                                            console.log(date);
                                            if (date == 'Invalid Date') {
                                                props.onChange(date);
                                            } else {
                                                props.onChange(date.toISOString());
                                            }
                                        }}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                        className="inputComponent component-datebox focusNextWhenPressedEnter openDateWhenF12Pressed"
                                        fullWidth
                                        autoOk={true}
                                        invalidDateMessage='Invalid Date Format/Empty'
                                    />
                                </Grid>
                            </MuiPickersUtilsProvider> 
                        </Grid>
                      )
                    };
                    tableColumns.push(field);
                  }

                }
              });
              let editableOptions = {};

              if (component.options.isCreatable){
                editableOptions['onRowAdd'] = newData => 
                new Promise(resolve => {
                  resolve();
                  let cancelSubmition = false;
                  let errorReason = "";
                  component.columns.map((column, key) =>{
                    //--- Map colums with autocomplete data && Conditon check
                    if(column.fieldType == 'custom'){
                      if (column.component.type == 'autocomplete' && column.component.mapTableColumns && column.component.mapTableColumns.length > 0){
                        column.component.mapTableColumns.forEach(mapLogic => {
                          var autoCompleteData = this.state[column.component.id];
                          newData[mapLogic.columnName] = getDataValueByKey(autoCompleteData, mapLogic.dataKey);
                        });
                      }
                      if (column.component.type == 'autocomplete' && column.component.conditions && column.component.conditions.length > 0){
                        column.component.conditions.forEach(condition => {
                          let keyValue1 = false;
                          let keyValue2 = false;
                          //--- Value 1
                          if (condition.conditionKey1.source == 'data'){
                            var autoCompleteData = this.state[column.component.id];
                            keyValue1 = getDataValueByKey(autoCompleteData, condition.conditionKey1.key);
                          }else if (condition.conditionKey1.source == 'column'){
                            keyValue1 = newData[condition.conditionKey1.key];
                          }
                          //--- Value 2
                          if (condition.conditionKey2.source == 'data'){
                            var autoCompleteData = this.state[column.component.id];
                            keyValue2 = getDataValueByKey(autoCompleteData, condition.conditionKey2.key);
                          }else if (condition.conditionKey2.source == 'column'){
                            keyValue2 = newData[condition.conditionKey2.key];
                          }
                          if (keyValue1 && keyValue2 && keyValue1 !== "" && keyValue2 !== "" && !isNaN(keyValue1) && !isNaN(keyValue2)){
                            if (condition.condition(parseFloat(keyValue1), parseFloat(keyValue2)) == false){
                              cancelSubmition = true;
                              errorReason = condition.errorMessage;
                            }
                          }
                          
                        });
                      }
                    }
                    
                    //--- Check for column logics
                    if(column.fieldType == 'field'){
                      if (typeof column.logic !== 'undefined' && Object.keys(column.logic).length > 0){
                        if (column.logic.operand && column.logic.entry1 && column.logic.entry2 && newData[column.logic.entry1] && newData[column.logic.entry2]){
                          switch (column.logic.operand) {
                            case '+':
                              newData[column.field] = parseFloat(newData[column.logic.entry1]) + parseFloat(newData[column.logic.entry2]);
                              break;
                            case '-':
                              newData[column.field] = parseFloat(newData[column.logic.entry1]) - parseFloat(newData[column.logic.entry2]);
                              break;
                            case '*':
                              newData[column.field] = parseFloat(newData[column.logic.entry1]) * parseFloat(newData[column.logic.entry2]);
                              break;
                            case '/':
                              newData[column.field] = parseFloat(newData[column.logic.entry1]) / parseFloat(newData[column.logic.entry2]);
                              break;
                            default:
                          }
                        }
                      }
                    }
                  });

                  let newDataStoreable = _.clone(newData);
                  component.columns.map((column, key) =>{
                    if(column.fieldType == 'custom'){
                      if (column.component.type == 'autocomplete'){
                        newDataStoreable[column.component.id] = {};
                        newDataStoreable[column.component.id].data = this.state[column.component.id];
                        newDataStoreable[column.component.id + '_id'] = this.state[column.component.id]._id;
                        newDataStoreable[column.component.id].collection = column.component.dataCollection._name;
                        newDataStoreable[column.component.id].value = this.state[column.component.id + '_inputValue'];
                        newData[column.component.id] = this.state[column.component.id + '_inputValue'];
                        this.setState({[column.component.id + '_inputValue']: ""});
                        this.setState({[column.component.id]: {}});
                      }
                    }
                  });

                  //--- Check for required data
                  component.columns.map((column, key) =>{
                    if(column.required){
                      if (column.fieldType == 'field'){
                        if (typeof newData[column.field] == 'undefined' || newData[column.field] == ""){
                          if (errorReason !== "") errorReason += ", ";
                          errorReason += column.field;
                          errorReason += " should not be empty";
                          cancelSubmition = true;
                        }
                      }else if(column.fieldType == 'custom'){
                        if (typeof newData[column.component.id] == 'undefined' || newData[column.component.id] == ""){
                          if (errorReason !== "") errorReason += ", ";
                          errorReason += column.component.id;
                          errorReason += " should not be empty";
                          cancelSubmition = true;
                        }
                      }
                    }
                  });
                  if (cancelSubmition){
                    Bert.alert( errorReason, 'danger', 'growl-top-right' );
                    return ;
                  }

                  let oldData = this.state[component.id];
                  let oldDataStoreable = this.state[component.id + '_storeable'];
                  console.log("oldDate: ", oldData);
                  console.log("newData: ", newData);
                  if (component.hasSumRow) {
                    oldData.pop();
                  }
                  oldData.push(newData);
                  oldDataStoreable.push(newDataStoreable);
                  console.log("oldDate: ", oldData);
                  console.log("newData: ", newData);

                  if (component.hasSumRow) {
                    //total sum columns
                    let totalSumRow = {};
                    component.columns.map((column, key) =>{
                        if (column.sumColumn){
                        let sumValue = 0;
                        oldData.map((rows, rkey)=>{
                            sumValue += parseFloat(rows[column.field]);
                        });
                        totalSumRow[column.field] = sumValue;
                        if (column.assignSumToComponent){
                            this.setState({[column.assignSumToComponent]: sumValue});
                        }
                        }else if (column.useColumnForTotalTitle){
                        totalSumRow[column.component.id] = 'Total';
                        }
                    });
                    if (totalSumRow && Object.keys(totalSumRow).length > 0){
                        oldData.push(totalSumRow);
                    }
                  }
                  

                  this.setState({[component.id]: oldData});
                  this.setState({[component.id + '_storeable']: oldDataStoreable});

                  $('button[title="Add"]').focus();
                });
              }

              if (component.options.isEditable) {
                editableOptions['onRowUpdate'] = (newData, recentData) =>
                new Promise(resolve => {
                  resolve();
                  if (recentData) {
                    let cancelSubmition = false;
                    let errorReason = "";
                    component.columns.map((column, key) =>{
                      //--- Check for column logics
                      if(column.fieldType == 'field'){
                        if (typeof column.logic !== 'undefined' && Object.keys(column.logic).length > 0){
                          if (column.logic.operand && column.logic.entry1 && column.logic.entry2 && newData[column.logic.entry1] && newData[column.logic.entry2]){
                            switch (column.logic.operand) {
                              case '+':
                                newData[column.field] = parseFloat(newData[column.logic.entry1]) + parseFloat(newData[column.logic.entry2]);
                                break;
                              case '-':
                                newData[column.field] = parseFloat(newData[column.logic.entry1]) - parseFloat(newData[column.logic.entry2]);
                                break;
                              case '*':
                                newData[column.field] = parseFloat(newData[column.logic.entry1]) * parseFloat(newData[column.logic.entry2]);
                                break;
                              case '/':
                                newData[column.field] = parseFloat(newData[column.logic.entry1]) / parseFloat(newData[column.logic.entry2]);
                                break;
                              default:
                            }
                          }
                        }
                      }
                    });

                    let newDataStoreable = _.clone(newData);
                    let oldDataStoreable = this.state[component.id + '_storeable'];
                    component.columns.map((column, key) =>{
                      if(column.fieldType == 'custom'){
                        if (column.component.type == 'autocomplete'){
                          if (this.state[column.component.id + '_inputValue'] !== "" && Object.keys(this.state[column.component.id]).length > 0){
                            newDataStoreable[column.component.id] = {};
                            newDataStoreable[column.component.id + '_id'] = this.state[column.component.id]._id;
                            newDataStoreable[column.component.id].data = this.state[column.component.id];
                            newDataStoreable[column.component.id].collection = column.component.dataCollection._name;
                            newDataStoreable[column.component.id].value = this.state[column.component.id + '_inputValue'];
                            newData[column.component.id] = this.state[column.component.id + '_inputValue'];
                          }else{
                            newData[column.component.id] = oldDataStoreable[recentData.tableData.id][column.component.id].value;
                            newDataStoreable[column.component.id] = oldDataStoreable[recentData.tableData.id][column.component.id];
                            newDataStoreable[column.component.id] = oldDataStoreable[recentData.tableData.id][column.component.id];
                            newDataStoreable[column.component.id + '_id'] = oldDataStoreable[recentData.tableData.id][column.component.id].data._id;
                          }
                          this.setState({[column.component.id + '_inputValue']: ""});
                          this.setState({[column.component.id]: {}});
                        }
                      }
                    });

                    
                    component.columns.map((column, key) =>{

                      //--- Condition check
                      if(column.fieldType == 'custom'){
                        if (column.component.type == 'autocomplete' && column.component.conditions && column.component.conditions.length > 0){
                          column.component.conditions.forEach(condition => {
                            let keyValue1 = false;
                            let keyValue2 = false;
                            //--- Value 1
                            if (condition.conditionKey1.source == 'data'){
                              if (this.state[column.component.id] && Object.keys(this.state[column.component.id]).length > 0){
                                var autoCompleteData = this.state[column.component.id];
                                keyValue1 = getDataValueByKey(autoCompleteData, condition.conditionKey1.key);
                              }else{
                                var autoCompleteData = newDataStoreable[column.component.id];
                                keyValue1 = getDataValueByKey(autoCompleteData, 'data.' + condition.conditionKey1.key);
                              }
                            }else if (condition.conditionKey1.source == 'column'){
                              keyValue1 = newData[condition.conditionKey1.key];
                            }
                            //--- Value 2
                            if (condition.conditionKey2.source == 'data'){
                              if (this.state[column.component.id] && Object.keys(this.state[column.component.id]).length > 0){
                                var autoCompleteData = this.state[column.component.id];
                                keyValue2 = getDataValueByKey(autoCompleteData, condition.conditionKey2.key);
                              }else{
                                var autoCompleteData = newDataStoreable[column.component.id];
                                keyValue2 = getDataValueByKey(autoCompleteData, 'data.' + condition.conditionKey2.key);
                              }
                            }else if (condition.conditionKey2.source == 'column'){
                              keyValue2 = newData[condition.conditionKey2.key];
                            }
                            if (keyValue1 && keyValue2 && keyValue1 !== "" && keyValue2 !== "" && !isNaN(keyValue1) && !isNaN(keyValue2)){
                              if (condition.condition(parseFloat(keyValue1), parseFloat(keyValue2)) == false){
                                cancelSubmition = true;
                                errorReason = condition.errorMessage;
                              }
                            }
                            
                          });
                        }
                      }

                      //--- Check for required data
                      if(column.required){
                        if (column.fieldType == 'field'){
                          if (typeof newData[column.field] == 'undefined' || newData[column.field] == ""){
                            if (errorReason !== "") errorReason += ", ";
                            errorReason += column.field;
                            errorReason += " should not be empty";
                            cancelSubmition = true;
                          }
                        }else if(column.fieldType == 'custom'){
                          if (typeof newData[column.component.id] == 'undefined' || newData[column.component.id] == ""){
                            if (errorReason !== "") errorReason += ", ";
                            errorReason += column.component.id;
                            errorReason += " should not be empty";
                            cancelSubmition = true;
                          }
                        }
                      }
                    });
                    if (cancelSubmition){
                      Bert.alert( errorReason, 'danger', 'growl-top-right' );
                      return ;
                    }
                    
                    let oldData = this.state[component.id];
                    if (component.hasSumRow) {
                        oldData.pop();
                    }
                    oldData[recentData.tableData.id] = newData;
                    oldDataStoreable[recentData.tableData.id] = newDataStoreable;

                    if (component.hasSumRow) {
                        //total sum columns
                        let totalSumRow = {};
                        component.columns.map((column, key) =>{
                        if (column.sumColumn){
                            let sumValue = 0;
                            oldData.map((rows, rkey)=>{
                            sumValue += parseFloat(rows[column.field]);
                            });
                            totalSumRow[column.field] = sumValue;
                            if (column.assignSumToComponent){
                            this.setState({[column.assignSumToComponent]: sumValue});
                            }
                        }else if (column.useColumnForTotalTitle){
                            totalSumRow[column.component.id] = 'Total';
                        }
                        });
                        if (totalSumRow && Object.keys(totalSumRow).length > 0){
                        oldData.push(totalSumRow);
                        }
                    }
                    
                    this.setState({[component.id]: oldData});
                    this.setState({[component.id + '_storeable']: oldDataStoreable});

                    $('button[title="Add"]').focus();
                  }
                });
              }
              if (component.options.isDeletable){
                editableOptions['onRowDelete'] = recentData =>
                new Promise(resolve => {
                  resolve();
                  let oldData = this.state[component.id];
                  let oldDataStoreable = this.state[component.id + '_storeable'];
                  if (component.hasSumRow) {
                    oldData.pop();
                    }
                  oldData.splice(recentData.tableData.id, 1);
                  oldDataStoreable.splice(recentData.tableData.id, 1);

                  if (component.hasSumRow) {
                    //total sum columns
                    let totalSumRow = {};
                    component.columns.map((column, key) =>{
                        if (column.sumColumn){
                        let sumValue = 0;
                        oldData.map((rows, rkey)=>{
                            sumValue += parseFloat(rows[column.field]);
                        });
                        totalSumRow[column.field] = sumValue;
                        if (column.assignSumToComponent){
                            this.setState({[column.assignSumToComponent]: sumValue});
                        }
                        }else if (column.useColumnForTotalTitle){
                        totalSumRow[column.field] = 'Total';
                        }
                    }, this);
                    if (totalSumRow && Object.keys(totalSumRow).length > 0){
                        oldData.push(totalSumRow);
                    }
                }
                  
                  

                  this.setState({[component.id]: oldData});
                  this.setState({[component.id + '_storeable']: oldDataStoreable});

                  $('button[title="Add"]').focus();
                });
              }
              return(
                <Grid item xs={component.size[0]} md={component.size[1]} lg={component.size[2]} key={parentKey} className="grid-item">
                  <MaterialTable
                    title={component.label}
                    id={component.id}
                    icons={tableIcons}
                    columns={tableColumns}
                    data={this.state[component.id]}
                    options={component.options}
                    editable={editableOptions}
                  />



              </Grid>
              );
            }



          }, this)}

          {(this.state.mode == 'new') ?
            <Grid item xs={12} md={12} lg={12}>
              {(this.checkForPermission('create')) ? 
              <Button fullWidth className="inputComponent" variant="outlined" color="primary" id="btn-save-text-type" onClick={this.handleCreate.bind(this)} >
                Save
              </Button>
            :
              <div></div>
              }
              {(this.checkForPermission('print') && (this.props.formAttr.printReceipt || this.props.formAttr.printInvoice)) ? 
              <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.autoPrint}
                        onChange={() => {
                          if (this.state.autoPrint) {
                            this.setState({ autoPrint: false });
                            localStorage.setItem(this.props.formAttr.id + 'print_checked', false);
                          } else {
                            this.setState({ autoPrint: true });
                            localStorage.setItem(this.props.formAttr.id + 'print_checked', true);
                          }
                        }}
                        id={this.props.formAttr.id + 'print_checked'}
                        value={this.state.autoPrint}
                        color={'primary'}
                        className="inputComponent component-checkbox focusNextWhenPressedEnter"
                      />
                    }
                    label={'Auto Print Receipt'}
                  />
              </FormGroup>
            :
              <div></div>
              }
            </Grid>
            :
            <Grid item xs={12} md={12} lg={12}>
              <Grid container>
                <Grid item xs={3} md={3} lg={3} >
                {(this.checkForPermission('update')) ? 
                  <Button fullWidth className="inputComponent moveNextWithArrowRightKey" variant="outlined" color="primary" id="btn-update-text-type" onClick={this.handleUpdate.bind(this)} >
                    Update
                  </Button>
                :
                  <div></div>
                  }
                </Grid>
                <Grid item xs={3} md={3} lg={3} >
                {(this.checkForPermission('delete')) ? 
                  <Button fullWidth className="inputComponent moveNextWithArrowRightKey moveBackWithArrowLeftKey" variant="outlined" color="secondary" id="btn-delete-text-type" onClick={this.handleDelete.bind(this)} >
                    Delete
                  </Button>
                :
                  <div></div>
                  }
                  
                </Grid>
                <Grid item xs={3} md={3} lg={3} >
                {(this.checkForPermission('print') && (this.props.formAttr.printReceipt || this.props.formAttr.printInvoice)) ? 
                  <Button fullWidth className="inputComponent moveNextWithArrowRightKey moveBackWithArrowLeftKey" variant="outlined" color="secondary" id="btn-print-text-type" onClick={this.handlePrint.bind(this)} >
                    Print
                  </Button>
                :
                  <div></div>
                  }
                  
                </Grid>
                <Grid item xs={3} md={3} lg={3} >
                  <Button fullWidth className="inputComponent moveBackWithArrowLeftKey" variant="outlined" color="default" id="btn-new-text-type" onClick={this.handleNew.bind(this)} >
                    New
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          }
        </Grid>

        </Container>
        </Paper>
      </Styles>
    );
  }
}

export default Forms = withTracker((props) => {
  let components = props.formAttr.components;
  let componentData = {};
  components.forEach(function (component) {
     if (component.type == 'selectbox' || component.type == 'autocomplete'){
       if (typeof component.dataCollection !== 'undefined'){
         let dataLogic = {};
         if (component.dataLogic) dataLogic = component.dataLogic;
         dataLogic.appid = sessionStorage.getItem("appid");
         if (typeof component.unitIndependent == 'undefined' || component.unitIndependent == null || component.unitIndependent == false) dataLogic.appunit = sessionStorage.getItem("appunit");
         componentData[component.id] = component.dataCollection.find(dataLogic).fetch();
       }
     }else if(component.type == 'multiselecttable'){
       component.columns.forEach((column, key) => {
         if (column.fieldType == 'custom'){
           if (typeof column.component.dataCollection !== 'undefined'){
             componentData[column.component.id] = column.component.dataCollection.find({ appid: sessionStorage.getItem("appid"), appunit: sessionStorage.getItem("appunit") }).fetch();
           }
         }
       });
     }
  }, this);
  if (props.formAttr.instantSearchConfig.instantSearchLogic) props.formAttr.instantSearchConfig.instantSearchLogic.appid = sessionStorage.getItem("appid");
  if (props.formAttr.instantSearchConfig.instantSearchLogic && (typeof props.formAttr.instantSearchConfig.unitIndependent == 'undefined' || props.formAttr.instantSearchConfig.unitIndependent == null || props.formAttr.instantSearchConfig.unitIndependent == false)) props.formAttr.instantSearchConfig.instantSearchLogic.appunit = sessionStorage.getItem("appunit");
  console.log(props.formAttr.instantSearchConfig.instantSearchLogic);
  formInstantSearchData = props.formAttr.collection.find(props.formAttr.instantSearchConfig.instantSearchLogic).fetch();
  if (props.formAttr.userForm){
    formInstantSearchData.map((data, key)=>{
      data.firstname = data.profile.firstName;
      data.lastname = data.profile.lastName;
      data.role = data.profile.role;
      data.email = data.profile.email;
    });
  }
  //--- Role permissions
  let rolePermission = {};
  if (Meteor.user()){
    if (Meteor.user().profile.role == 'evarosoft_owner'){
      rolePermission.allAccess = true;
    }else if((Collection_Roles.findOne({rolename: 'admin'}) && Meteor.user().profile.role == Collection_Roles.findOne({rolename: 'admin'})._id) || Meteor.user().profile.role == 'owner'){
      rolePermission.allAccess = true;
    }else{
      rolePermission = Collection_Roles.findOne(Meteor.user().profile.role);
    }
  }
  return {
    componentData,
    formInstantSearchData,
    rolePermission,
  };
})(FormsContainer);
