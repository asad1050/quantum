export function getDataValueByKey(data, key){
    let result = false;
    if (key && key.split('.').length > 0){
        //---Nested Logic
        result = data;
        var loc = key;
        loc = loc.split('.');
        loc.forEach(function(location){
            result = result[location];
        });
    }else{
        result = data[key];
    }
    return result;
}