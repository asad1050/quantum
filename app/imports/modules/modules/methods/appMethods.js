import { Apps } from '../../../api/collections';
import collectionPointer from '../../../api/collectionPointer';
import { createTransaction, createAccount, updateAccount, deleteAccount, reverseTransaction } from '../../../models/accountingModel';
import { getDataValueByKey } from './methodSupporters';

if (Meteor.isServer){
  Meteor.methods({
    //APP Methods
    'app.fetchData': (name) => {
      if (name == ''){
        throw new Meteor.Error("Please enter name of app");
      }
      console.log("Fetching Data of App: ", name);
      var appData = Apps.find({'appname': name}).fetch();
      if (appData) {
        var settings = collectionPointer('generalsettings').findOne({ appid: appData[0]._id });
        if (settings) { 
          appData[0].settings = settings;
        }
        return appData;
      } else {
        return false;
      }
    },

    'app.fetchUserData': (name) => {
        return Meteor.user();
    },

    'app.fetchMainUnit': (appid) => {
        return collectionPointer('units').findOne({ name: 'main', appid: appid  });
    },

    //FORM Methods
    'form.create': (collectionName, dataScheme, options, formName) => {
      if (dataScheme == {} || typeof dataScheme == 'undefined'){
        throw new Meteor.Error("Please fill all required fields");
      }
      
      //--- Check for authorization
      if (Meteor.userId() == null) throw new Meteor.Error("Unauthorized access");
      var userData = Meteor.user();
      if (userData.profile.appid !== options.appid) throw new Meteor.Error("App mismatch with user");
      var appPermission = false;
      var appData = Apps.findOne(options.appid);
      if (appData && Object.keys(appData).length > 0) {
          if (appData.appname == userData.profile.appname && appData.apptype == options.apptype && appData.appplan == options.appplan) {
            appPermission = true;
          }
      }
      if (appPermission == false) {
        throw new Meteor.Error("You front app details does not match with db app details, Forbidden!");
      }
      if (appData.active == false) {
        throw new Meteor.Error("You app is not active, please contact administrator.");
      }

      dataScheme['appid'] = userData.profile.appid;
      dataScheme['appname'] = userData.profile.appname;
      if (userData.profile.appunit) {
        dataScheme['appunit'] = userData.profile.appunit;
      } else {
        dataScheme['appunit'] = collectionPointer('units').findOne({ name: 'main', appid: userData.profile.appid  })._id;
      }
      options.appunit = dataScheme['appunit'];


      if (options){
        let extraIds = options.extraIds;
        //--- Extra extraIds
        if (extraIds && extraIds.length > 0){
          extraIds.forEach(function (extraId) {
            let count = false;
            let searchLogic = {};
            if (extraId.logic) searchLogic = extraId.logic;
            if (typeof searchLogic.appid == 'undefined') searchLogic.appid = options.appid;
            if (typeof searchLogic.appunit == 'undefined') searchLogic.appunit = options.appunit;
            let lastEntry = collectionPointer(collectionName).find(searchLogic, {limit: 1, sort: {index: -1}}).fetch();
            if (lastEntry && lastEntry.length > 0){
              let entryCount = lastEntry[0][extraId.keyName];
              let indexCount = lastEntry[0]['index'];
              if (entryCount){
                entryCount = entryCount.match(/\d+/)[0];
                entryCount = parseInt(entryCount);
                entryCount++;
                indexCount++;
  
                let newId = extraId.prefix + entryCount + extraId.postfix;
                dataScheme[extraId.keyName] = newId;
                dataScheme['index'] = indexCount;
                
              }
            }else{
              let entryCount = "1";
              let indexCount = 1;
              let newId = extraId.prefix + entryCount + extraId.postfix;
              dataScheme[extraId.keyName] = newId;
              dataScheme['index'] = indexCount;
            }
          }, this);
        }
      }
      

      //--- Check for Unique Keys
      let throwDuplicationError = false;
      let duplicateKeys = "";
      if (options && options.uniqueKeys && options.uniqueKeys.length > 0){
        options.uniqueKeys.map((key, i)=>{
          if (typeof dataScheme[key] !== 'undefined' && dataScheme[key] !== ""){
            if (collectionPointer(collectionName).find({[key]: dataScheme[key], appid: options.appid, appunit: options.appunit }).count() !== 0){
              if (duplicateKeys !== "") duplicateKeys += ", ";
              duplicateKeys += key;
              throwDuplicationError = true;
            }
          }
        });
        if (throwDuplicationError){
          throw new Meteor.Error("Value of following entries already exists: " + duplicateKeys);
        }
      }

        //--- If collection have some assiciated account
        if (options && options.accountConfig && Object.keys(options.accountConfig).length > 0){
            if (typeof options.accountConfig.nameKey == 'object') {
                var accountName = '';
                options.accountConfig.nameKey.forEach((dataKeyName, dataKeyNameIndex) => {
                    if (dataKeyNameIndex !== 0) accountName += ' ';
                    accountName += getDataValueByKey(dataScheme, dataKeyName);
                });
                var newAccountId = createAccount(accountName, options.accountConfig.accountCategoryByName, options.appid, options.appunit, dataScheme, collectionName);
            } else {
                var newAccountId = createAccount(getDataValueByKey(dataScheme, dataScheme[options.accountConfig.nameKey]), options.accountConfig.accountCategoryByName, options.appid, options.appunit, dataScheme, collectionName);
            }
            
            dataScheme['account'] = newAccountId;
        }
      
      var accountTransactions = [];
      //--- Apply Component DB Loical relations
      if (options && options.componentDBLogicalRelation && options.componentDBLogicalRelation.length > 0){
        options.componentDBLogicalRelation.forEach((component, i) => {
          if (component.type == 'multiselecttable'){
            component.tableDBLogicalRelations.forEach((columnRelation, j) => {
              if (columnRelation.DBLogicalRelations){
                //--- DB Logical relations
                columnRelation.DBLogicalRelations.forEach((relation, k) => {
                  switch (relation.action) {
                    case 'add':
                      dataScheme[component.key].forEach((rows, l) => {
                        var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(rows, relation.DBCollectionId));
                        var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                        relatedValue += parseFloat(getDataValueByKey(rows, columnRelation.name));
                        collectionPointer(relation.DBCollection).update(getDataValueByKey(rows, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      break;
                    case 'subtract':
                      dataScheme[component.key].forEach((rows, l) => {
                        var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(rows, relation.DBCollectionId));
                        var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                        relatedValue -= parseFloat(getDataValueByKey(rows, columnRelation.name));
                        collectionPointer(relation.DBCollection).update(getDataValueByKey(rows, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      break;
                    case 'replace':
                      dataScheme[component.key].forEach((rows, l) => {
                        collectionPointer(relation.DBCollection).update(getDataValueByKey(rows, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: getDataValueByKey(rows, columnRelation.name)} });
                      });
                      break;
                    default:

                  }
                });
              }
              if (columnRelation.accountRelations){
                //--- Accounts Relations
                columnRelation.accountRelations.forEach((relation, k) => {
                  dataScheme[component.key].forEach((rows, l) => {
                    var creditValue = 0;
                    var creditAccountId = "";
                    var debitValue = 0;
                    var debitAccountId = "";
                    if (relation.credit && Object.keys(relation.credit).length > 0){
                      switch (relation.credit.action) {
                        case 'byDataKey':
                          if (relation.credit.dataKey){
                            creditValue = parseFloat(getDataValueByKey(rows, relation.credit.dataKey));
                          }else{
                            creditValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                          }
                          
                          creditAccountId = getDataValueByKey(dataScheme, relation.credit.DBCollectionKey);
                          break;
                        case 'byAccountName':
                          if (relation.credit.dataKey){
                            creditValue = parseFloat(getDataValueByKey(rows, relation.credit.dataKey));
                          }else{
                            creditValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                          }
                          
                          creditAccountId = collectionPointer('accounts').findOne({ name: relation.credit.accountName, appid: options.appid, appunit: options.appunit })._id;
                          break;
                        default:
                      }
                    }

                    if (relation.debit && Object.keys(relation.debit).length > 0){
                      switch (relation.debit.action) {
                        case 'byDataKey':
                          if (relation.debit.dataKey){
                            debitValue = parseFloat(getDataValueByKey(rows, relation.debit.dataKey));
                          }else{
                            debitValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                          }
                          
                          debitAccountId = getDataValueByKey(dataScheme, relation.debit.DBCollectionKey);
                          break;
                        case 'byAccountName':
                          if (relation.debit.dataKey){
                            debitValue = parseFloat(getDataValueByKey(rows, relation.debit.dataKey));
                          }else{
                            debitValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                          }
                          debitAccountId = collectionPointer('accounts').findOne({ name: relation.debit.accountName, appid: options.appid, appunit: options.appunit })._id;
                          
                          break;
                        default:
                      }
                    }
                    
                    
                    if (relation.description.indexOf ('byNoteEntry') >= 0) {
                      if (dataScheme.note && dataScheme.note !== ""){
                        relation.description = dataScheme.note;
                      } else {
                        relation.description = relation.description.split(',')[1];
                      }
                    }

                    var additionalField = false;
                    if (relation.includeInTransactions && getDataValueByKey(dataScheme, relation.includeInTransactions)) {
                      additionalField = getDataValueByKey(dataScheme, relation.includeInTransactions);
                    }
                    
                    var transactionId = createTransaction(debitAccountId, creditAccountId, relation.description, new Date().toISOString(), creditValue, options.appid, options.appunit, additionalField, formName);
                    accountTransactions.push(transactionId);


                  });
                });
              }

            });

          }else if (component.type == 'textbox'){
            let columnRelation = component.logicalRelations;
            if (columnRelation.DBLogicalRelations){
              //--- DB Logical relations
              columnRelation.DBLogicalRelations.forEach((relation, k) => {
                switch (relation.action) {
                  case 'add':
                      var componentValue = dataScheme[component.key];
                      var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(dataScheme, relation.DBCollectionId));
                      var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                      relatedValue += parseFloat(componentValue);
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                    break;
                  case 'subtract':
                      var componentValue = dataScheme[component.key];
                      var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(dataScheme, relation.DBCollectionId));
                      var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                      relatedValue -= parseFloat(componentValue);
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                    break;
                  case 'replace':
                      var componentValue = dataScheme[component.key];
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: componentValue} });
                    break;
                  default:

                }
              });
            }

            if (columnRelation.accountRelations){
              //--- Accounts Relations
              columnRelation.accountRelations.forEach((relation, k) => {
                var componentValue = dataScheme[component.key];
                var creditValue = 0;
                var creditAccountId = "";
                var debitValue = 0;
                var debitAccountId = "";
                if (relation.credit && Object.keys(relation.credit).length > 0){
                  switch (relation.credit.action) {
                    case 'byDataKey':
                      creditValue = parseFloat(componentValue);
                      creditAccountId = getDataValueByKey(dataScheme, relation.credit.DBCollectionKey);
                      break;
                    case 'byAccountName':
                      creditValue = parseFloat(componentValue);
                      creditAccountId = collectionPointer('accounts').findOne({ name: relation.credit.accountName, appid: options.appid, appunit: options.appunit })._id;
                      break;
                    default:
                  }
                }

                if (relation.debit && Object.keys(relation.debit).length > 0){
                  switch (relation.debit.action) {
                    case 'byDataKey':
                      debitValue = parseFloat(componentValue);
                      debitAccountId = getDataValueByKey(dataScheme, relation.debit.DBCollectionKey);
                      break;
                    case 'byAccountName':
                      debitValue = parseFloat(componentValue);
                      debitAccountId = collectionPointer('accounts').findOne({ name: relation.debit.accountName, appid: options.appid, appunit: options.appunit })._id;
                      break;
                    default:
                  }
                }
                
                if (relation.description.indexOf ('byNoteEntry') >= 0) {
                  if (dataScheme.note && dataScheme.note !== ""){
                    relation.description = dataScheme.note;
                  } else {
                    relation.description = relation.description.split(',')[1];
                  }
                }

                console.log('creditValue: ', creditValue);
                console.log('debitValue: ', debitValue);
                console.log('relation.description: ', relation.description);

                var additionalField = false;
                console.log(relation.includeInTransactions);
                console.log(dataScheme);
                console.log(getDataValueByKey(dataScheme, relation.includeInTransactions));
                
                
                if (relation.includeInTransactions && getDataValueByKey(dataScheme, relation.includeInTransactions)) {
                  additionalField = getDataValueByKey(dataScheme, relation.includeInTransactions);
                }
                var transactionId = createTransaction(debitAccountId, creditAccountId, relation.description, new Date().toISOString(), creditValue, options.appid, options.appunit, additionalField, formName);
                accountTransactions.push(transactionId);
              });
            }

          }
        });
      }

      //--- Change other collection entry on creation
      if (options && options.changeOtherCollectionDataOnCreation && options.changeOtherCollectionDataOnCreation.length > 0){
        options.changeOtherCollectionDataOnCreation.forEach(change => {
          if (change.type == 'directValue'){
            collectionPointer(change.collection).update(getDataValueByKey(dataScheme, change.DBCollectionId), { $set: { [change.entryKey]: change.entryValue } });
          } else if (change.type == 'fromFormData'){
            collectionPointer(change.collection).update(getDataValueByKey(dataScheme, change.DBCollectionId), { $set: { [change.entryKey]: getDataValueByKey(dataScheme, change.entryValueKey) } });
          }
        });
      }

      //--- Insert transactions to collection
      if (accountTransactions && accountTransactions.length > 0) dataScheme['accounttransactions'] = accountTransactions;

      //--- inserting created date
      dataScheme['createdOn'] = new Date().toISOString();

      //--- insert data
      if (options && options.userForm){
        if (dataScheme['password'] == '' || dataScheme['confirmpassword'] == ''){
          throw new Meteor.Error("Password fields should not be empty");
        }
        Accounts.createUser({
          username: dataScheme['username'],
          email: dataScheme['email'],
          password: dataScheme['password'],
          profile: {
            appname: dataScheme['appname'],
            appid: dataScheme['appid'],
            firstName: dataScheme['firstname'],
            lastName: dataScheme['lastname'],
            email: dataScheme['email'],
            role: dataScheme['role'],
            appunit: dataScheme['appunit'],
            eid: dataScheme['eid'],
          }
        });
      }else{
        var newId = collectionPointer(collectionName).insert(dataScheme);
        return collectionPointer(collectionName).findOne(newId);
      }
    },

    'form.update': (collectionName, id, dataScheme, options, formName) => {
      if (dataScheme == {} || typeof dataScheme == 'undefined'){
        throw new Meteor.Error("Please fill all required fields");
      }
      //--- Check for authorization
      if (Meteor.userId() == null) throw new Meteor.Error("Unauthorized access");
      var userData = Meteor.user();
      if (userData.profile.appid !== options.appid) throw new Meteor.Error("App mismatch with user");
      var appPermission = false;
      var appData = Apps.findOne(options.appid);
      if (appData && Object.keys(appData).length > 0) {
          if (appData.appname == userData.profile.appname && appData.apptype == options.apptype && appData.appplan == options.appplan) {
            appPermission = true;
          }
      }
      if (appPermission == false) {
        throw new Meteor.Error("You front app details does not match with db app details, Forbidden!");
      }
      if (appData.active == false) {
        throw new Meteor.Error("You app is not active, please contact administrator.");
      }

      if (collectionPointer(collectionName).find({ _id: id, editable: false, appid: options.appid }).count() > 0){
        throw new Meteor.Error("Entry is not updatable");
      }
      if (userData.profile.appunit) {
        options.appunit = userData.profile.appunit;
      } else {
        options.appunit = collectionPointer('units').findOne({ name: 'main', appid: userData.profile.appid  })._id;
      }

      //--- Check for Unique Keys
      let throwDuplicationError = false;
      let duplicateKeys = "";
      if (options && options.uniqueKeys && options.uniqueKeys.length > 0){
        options.uniqueKeys.map((key, i)=>{
          if (typeof dataScheme[key] !== 'undefined' && dataScheme[key] !== ""){
            if (collectionPointer(collectionName).find({[key]: dataScheme[key], _id: { $ne: id }, appid: options.appid, appunit: options.appunit }).count() !== 0){
              if (duplicateKeys !== "") duplicateKeys += ", ";
              duplicateKeys += key;
              throwDuplicationError = true;
            }
          }
        });
        if (throwDuplicationError){
          throw new Meteor.Error("Value of following entries already exists: " + duplicateKeys);
        }
      }

      
      let accountTransactions = [];
      let oldData = collectionPointer(collectionName).findOne(id);
      
      if (oldData && oldData.accounttransactions && oldData.accounttransactions.length > 0){
        //--- Reverse current transactions
        for (var i=0;i<oldData.accounttransactions.length;i++){
          var transaction = oldData.accounttransactions[i];
          reverseTransaction(transaction);
        }
      }

      //--- Apply Component DB Loical relations
      if (options && options.componentDBLogicalRelation && options.componentDBLogicalRelation.length > 0){
        options.componentDBLogicalRelation.forEach((component, i) => {
          if (component.type == 'multiselecttable'){
            component.tableDBLogicalRelations.forEach((columnRelation, j) => {
              if (columnRelation.DBLogicalRelations){
                //--- DB logical relations
                columnRelation.DBLogicalRelations.forEach((relation, k) => {
                  switch (relation.action) {
                    case 'add':
                      dataScheme[component.key].forEach((rows, l) => {
                        let currentData = collectionPointer(relation.DBCollection).findOne(rows[relation.DBCollectionId]);
                        let relatedValue = parseFloat(currentData[relation.DBCollectionKey]);
                        relatedValue += parseFloat(getDataValueByKey(rows, columnRelation.name));
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      oldData[component.key].forEach((rows, l) => {
                        let currentData = collectionPointer(relation.DBCollection).findOne(rows[relation.DBCollectionId]);
                        let relatedValue = parseFloat(currentData[relation.DBCollectionKey]);
                        relatedValue -= parseFloat(oldData[component.key][l][columnRelation.name]);
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      
                      break;
                    case 'subtract':
                      dataScheme[component.key].forEach((rows, l) => {
                        let currentData = collectionPointer(relation.DBCollection).findOne(rows[relation.DBCollectionId]);
                        let relatedValue = parseFloat(currentData[relation.DBCollectionKey]);
                        relatedValue -= parseFloat(getDataValueByKey(rows, columnRelation.name));
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      oldData[component.key].forEach((rows, l) => {
                        let currentData = collectionPointer(relation.DBCollection).findOne(rows[relation.DBCollectionId]);
                        let relatedValue = parseFloat(currentData[relation.DBCollectionKey]);
                        relatedValue += parseFloat(oldData[component.key][l][columnRelation.name]);
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      break;
                    case 'replace':
                      dataScheme[component.key].forEach((rows, l) => {
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: getDataValueByKey(rows, columnRelation.name)} });
                      });
                      break;
                    default:
  
                  }
                });
              }

              if (columnRelation.accountRelations){
                //--- Accounts Relations
                columnRelation.accountRelations.forEach((relation, k) => {
                  dataScheme[component.key].forEach((rows, l) => {
                    let creditValue = 0;
                  let creditAccountId = "";
                  let debitValue = 0;
                  let debitAccountId = "";
                  if (relation.credit && Object.keys(relation.credit).length > 0){
                    switch (relation.credit.action) {
                      case 'byDataKey':
                        if (relation.credit.dataKey){
                          creditValue = parseFloat(getDataValueByKey(rows, relation.credit.dataKey));
                        }else{
                          creditValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                        }
                        creditAccountId = getDataValueByKey(dataScheme, relation.credit.DBCollectionKey);
                        break;
                      case 'byAccountName':
                        if (relation.credit.dataKey){
                          creditValue = parseFloat(getDataValueByKey(rows, relation.credit.dataKey));
                        }else{
                          creditValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                        }
                        creditAccountId = collectionPointer('accounts').findOne({ name: relation.credit.accountName, appid: options.appid, appunit: options.appunit })._id;
                        break;
                      default:
                    }
                  }

                  if (relation.debit && Object.keys(relation.debit).length > 0){
                    switch (relation.debit.action) {
                      case 'byDataKey':
                        if (relation.debit.dataKey){
                          debitValue = parseFloat(getDataValueByKey(rows, relation.debit.dataKey));
                        }else{
                          debitValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                        }
                        debitAccountId = getDataValueByKey(dataScheme, relation.debit.DBCollectionKey);
                        break;
                      case 'byAccountName':
                        if (relation.debit.dataKey){
                          debitValue = parseFloat(getDataValueByKey(rows, relation.debit.dataKey));
                        }else{
                          debitValue = parseFloat(getDataValueByKey(rows, columnRelation.name));
                        }
                        debitAccountId = collectionPointer('accounts').findOne({ name: relation.debit.accountName, appid: options.appid, appunit: options.appunit })._id;
                        break;
                      default:
                    }
                  }
                  
                  if (relation.description.indexOf ('byNoteEntry') >= 0) {
                    if (dataScheme.note && dataScheme.note !== ""){
                      relation.description = dataScheme.note;
                    } else {
                      relation.description = relation.description.split(',')[1];
                    }
                  }

                  var additionalField = false;
                  if (relation.includeInTransactions && getDataValueByKey(dataScheme, relation.includeInTransactions)) {
                    additionalField = getDataValueByKey(dataScheme, relation.includeInTransactions);
                  }
                  var transactionId = createTransaction(debitAccountId, creditAccountId, relation.description, new Date().toISOString(), creditValue, options.appid, options.appunit, additionalField, formName);
                  accountTransactions.push(transactionId);


                  });
                });
              }
              

            });

          }else if (component.type == 'textbox'){
            let columnRelation = component.logicalRelations;
            if (columnRelation.DBLogicalRelations){
              //--- DB Logical relations
              columnRelation.DBLogicalRelations.forEach((relation, k) => {
                switch (relation.action) {
                  case 'add':
                      var componentValue = dataScheme[component.key];
                      
                      
                      var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(dataScheme, relation.DBCollectionId));
                      
                      var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                      relatedValue += parseFloat(componentValue);
                      relatedValue -= parseFloat(oldData[component.key]);
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                    break;
                  case 'subtract':
                      var componentValue = dataScheme[component.key];
                      var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(dataScheme, relation.DBCollectionId));
                      var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                      relatedValue -= parseFloat(componentValue);
                      relatedValue += parseFloat(oldData[component.key]);
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                    break;
                  case 'replace':
                      var componentValue = dataScheme[component.key];
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: componentValue} });
                    break;
                  default:

                }
              });
            }

            if (columnRelation.accountRelations){
              //--- Accounts Relations
              columnRelation.accountRelations.forEach((relation, k) => {
                var componentValue = dataScheme[component.key];
                var creditValue = 0;
                var creditAccountId = "";
                var debitValue = 0;
                var debitAccountId = "";
                if (relation.credit && Object.keys(relation.credit).length > 0){
                  switch (relation.credit.action) {
                    case 'byDataKey':
                      creditValue = parseFloat(componentValue);
                      creditAccountId = getDataValueByKey(dataScheme, relation.credit.DBCollectionKey);
                      break;
                    case 'byAccountName':
                      creditValue = parseFloat(componentValue);
                      creditAccountId = collectionPointer('accounts').findOne({ name: relation.credit.accountName, appid: options.appid, appunit: options.appunit })._id;
                      break;
                    default:
                  }
                }

                if (relation.debit && Object.keys(relation.debit).length > 0){
                  switch (relation.debit.action) {
                    case 'byDataKey':
                      debitValue = parseFloat(componentValue);
                      debitAccountId = getDataValueByKey(dataScheme, relation.debit.DBCollectionKey);
                      break;
                    case 'byAccountName':
                      debitValue = parseFloat(componentValue);
                      debitAccountId = collectionPointer('accounts').findOne({ name: relation.debit.accountName, appid: options.appid, appunit: options.appunit })._id;
                      break;
                    default:
                  }
                }
                
                if (relation.description.indexOf ('byNoteEntry') >= 0) {
                  if (dataScheme.note && dataScheme.note !== ""){
                    relation.description = dataScheme.note;
                  } else {
                    relation.description = relation.description.split(',')[1];
                  }
                }

                var additionalField = false;
                if (relation.includeInTransactions && getDataValueByKey(dataScheme, relation.includeInTransactions)) {
                  additionalField = getDataValueByKey(dataScheme, relation.includeInTransactions);
                }
                var transactionId = createTransaction(debitAccountId, creditAccountId, relation.description, new Date().toISOString(), creditValue, options.appid, options.appunit, additionalField, formName);
                accountTransactions.push(transactionId);
              });
            }

          }
        });
      }
        
      //--- Insert transactions to collection
      if (accountTransactions && accountTransactions.length > 0) dataScheme['accounttransactions'] = accountTransactions;

      //--- inserting updated date
      dataScheme['updatedOn'] = new Date().toISOString();

      //--- If collection have some assiciated account
      if (options && options.accountConfig && Object.keys(options.accountConfig).length > 0){
        if (typeof options.accountConfig.nameKey == 'object') {
            var accountName = '';
            options.accountConfig.nameKey.forEach((dataKeyName, dataKeyNameIndex) => {
                if (dataKeyNameIndex !== 0) accountName += ' ';
                accountName += getDataValueByKey(dataScheme, dataKeyName);
            });
            updateAccount(dataScheme['account'], accountName);
        } else {
            updateAccount(dataScheme['account'], getDataValueByKey(dataScheme, options.accountConfig.nameKey));
        }
      }
      if (options && options.userForm){
        let currentEmail = collectionPointer(collectionName).findOne(id).profile.email;
        collectionPointer(collectionName).update(id, {$set: {
          username: dataScheme['username'],
          password: dataScheme['password'],
          'profile.firstName': dataScheme['firstname'],
          'profile.lastName' : dataScheme['lastname'],
          'profile.role' : dataScheme['role'],
          'profile.appunit' : dataScheme['unit'],
          'profile.email' : dataScheme['email'],
        }});
        if (dataScheme['password'] !== '' && dataScheme['confirmpassword'] !== ''){
          Accounts.setPassword(id, dataScheme['password']);
        }
        if (currentEmail !== dataScheme['email']){
          Accounts.removeEmail(id, currentEmail);
          Accounts.addEmail(id, dataScheme['email'], true);
        }
      }else{
        return collectionPointer(collectionName).update(id, dataScheme);
      }
    },

    'form.delete': (collectionName, id, options, formName) => {
      //--- Check for authorization
      if (Meteor.userId() == null) throw new Meteor.Error("Unauthorized access");
      var userData = Meteor.user();
      if (userData.profile.appid !== options.appid) throw new Meteor.Error("App mismatch with user");
      var appPermission = false;
      var appData = Apps.findOne(options.appid);
      if (appData && Object.keys(appData).length > 0) {
          if (appData.appname == options.appname && appData.apptype == options.apptype && appData.appplan == options.appplan) {
            appPermission = true;
          }
      }
      if (appPermission == false) {
        throw new Meteor.Error("You front app details does not match with db app details, Forbidden!");
      }
      if (appData.active == false) {
        throw new Meteor.Error("You app is not active, please contact administrator.");
      }
      // //--- Get Data For check
      if (userData.profile.appunit) {
        options.appunit = userData.profile.appunit;
      } else {
        options.appunit = collectionPointer('units').findOne({ name: 'main', appid: userData.profile.appid  })._id;
      }
      // let data = collectionPointer(collectionName).find({'_id': id});
      //--- Delete Check
      let allowDelete = true;
      let forbidReason = '';
      if (options && options.deleteCheck && options.deleteCheck.length > 0){
        options.deleteCheck.forEach((item, i) => {
          if (collectionPointer(item.collection).find({[item.entryKey]: id, appid: options.appid, appunit: options.appunit}).count() > 0){
            allowDelete = false;
            forbidReason = 'This entry is already being used in other collection (data table)';
          }
        });

      }
      if (allowDelete == false) throw new Meteor.Error(forbidReason);

      if (collectionPointer(collectionName).find({ _id: id, deletable: false, appid: options.appid, appunit: options.appunit }).count() > 0){
        throw new Meteor.Error("Entry is not deletable");
      }

      let dataScheme = collectionPointer(collectionName).findOne(id);
      if (dataScheme.accounttransactions && dataScheme.accounttransactions.length > 0){
        //--- Reverse current transactions
        for (var i=0;i<dataScheme.accounttransactions.length;i++){
          var transaction = dataScheme.accounttransactions[i];
          reverseTransaction(transaction);
        }
      }
      //--- Apply Component DB Loical relations
      if (options && options.componentDBLogicalRelation && options.componentDBLogicalRelation.length > 0){
        options.componentDBLogicalRelation.forEach((component, i) => {
          if (component.type == 'multiselecttable'){
            component.tableDBLogicalRelations.forEach((columnRelation, j) => {
              if (columnRelation.DBLogicalRelations){
                //--- DB logical relations
                columnRelation.DBLogicalRelations.forEach((relation, k) => {
                  switch (relation.action) {
                    case 'add':
                      dataScheme[component.key].forEach((rows, l) => {
                        let currentData = collectionPointer(relation.DBCollection).findOne(rows[relation.DBCollectionId]);
                        let relatedValue = parseFloat(currentData[relation.DBCollectionKey]);
                        relatedValue -= parseFloat(getDataValueByKey(rows, columnRelation.name));
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      break;
                    case 'subtract':
                      dataScheme[component.key].forEach((rows, l) => {
                        let currentData = collectionPointer(relation.DBCollection).findOne(rows[relation.DBCollectionId]);
                        let relatedValue = parseFloat(currentData[relation.DBCollectionKey]);
                        relatedValue += parseFloat(getDataValueByKey(rows, columnRelation.name));
                        collectionPointer(relation.DBCollection).update(rows[relation.DBCollectionId], { $set: {[relation.DBCollectionKey]: relatedValue} });
                      });
                      break;
                    default:

                  }
                });
              }
            });

          }else if (component.type == 'textbox'){
            let columnRelation = component.logicalRelations;
            if (columnRelation.DBLogicalRelations){
              //--- DB Logical relations
              columnRelation.DBLogicalRelations.forEach((relation, k) => {
                switch (relation.action) {
                  case 'add':
                      var componentValue = dataScheme[component.key];
                      var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(dataScheme, relation.DBCollectionId));
                      var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                      relatedValue -= parseFloat(componentValue);
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                    break;
                  case 'subtract':
                      var componentValue = dataScheme[component.key];
                      var currentData = collectionPointer(relation.DBCollection).findOne(getDataValueByKey(dataScheme, relation.DBCollectionId));
                      var relatedValue = parseFloat(getDataValueByKey(currentData, relation.DBCollectionKey));
                      relatedValue += parseFloat(componentValue);
                      collectionPointer(relation.DBCollection).update(getDataValueByKey(dataScheme, relation.DBCollectionId), { $set: {[relation.DBCollectionKey]: relatedValue} });
                    break;
                  default:

                }
              });
            }

          }
        });
      }

      //--- Change other collection entry on creation
      if (options && options.changeOtherCollectionDataOnDelete && options.changeOtherCollectionDataOnDelete.length > 0){
        options.changeOtherCollectionDataOnDelete.forEach(change => {
          if (change.type == 'directValue'){
            collectionPointer(change.collection).update(getDataValueByKey(dataScheme, change.DBCollectionId), { $set: { [change.entryKey]: change.entryValue } });
          } else if (change.type == 'fromFormData'){
            collectionPointer(change.collection).update(getDataValueByKey(dataScheme, change.DBCollectionId), { $set: { [change.entryKey]: getDataValueByKey(dataScheme, change.entryValueKey) } });
          }
        });
      }

      
      if (options && options.accountConfig &&Object.keys(options.accountConfig).length > 0){
        if (typeof options.accountConfig.nameKey == 'object') {
            var accountName = '';
            options.accountConfig.nameKey.forEach((dataKeyName, dataKeyNameIndex) => {
                if (dataKeyNameIndex !== 0) accountName += ' ';
              accountName += getDataValueByKey(dataScheme, dataKeyName);
            });
            deleteAccount(dataScheme['account'], accountName);
        } else {
            deleteAccount(dataScheme['account'], getDataValueByKey(dataScheme, options.accountConfig.nameKey));
        }
      }

      //--- Delete Entry
      collectionPointer(collectionName).remove(id);
    },
  });
}
