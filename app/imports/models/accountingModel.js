import {
  Collection_AccountGroups,
  Collection_Accounts,
  Collection_AccountCategories,
  Collection_Transactions,
} from '../api/collections';
import collectionPointer from '../api/collectionPointer';

export function createTransaction(debitAccountId, creditAccountId, description, date, amount, appid, appunit, additionalField, formName){
  if (isNaN(amount)) {
    throw new Meteor.Error("Amount should be number");
  }
  let currentDebitAccountBalance = 0;
  if (debitAccountId){
    let debitAccount = Collection_Accounts.findOne(debitAccountId);
    currentDebitAccountBalance = parseFloat(debitAccount.balance);
    let debitAccountCategory = Collection_AccountCategories.findOne(debitAccount.category);
    let debitAccountType = Collection_AccountGroups.findOne(debitAccountCategory.group);
    
    switch (debitAccountType.name) {
      case 'Assets':
        currentDebitAccountBalance += parseFloat(amount);
        break;
      case 'Liabilities':
        currentDebitAccountBalance -= parseFloat(amount);
        break;
      case 'Equity':
        currentDebitAccountBalance -= parseFloat(amount);
        break;
      case 'Revenues':
        currentDebitAccountBalance -= parseFloat(amount);
        break;
      case 'Expenses':
        currentDebitAccountBalance += parseFloat(amount);
        break;
    
      default:
        break;
    }
    
    if (isNaN(currentDebitAccountBalance)) throw new Meteor.Error('Account debit transaction amount is Not a Number');
    Collection_Accounts.update(debitAccountId, { $set: { balance:  currentDebitAccountBalance} });
    
  }else{
    debitAccountId = false;
  }
  let currentCreditAccountBalance = 0;
  if (creditAccountId){
    let creditAccount = Collection_Accounts.findOne(creditAccountId);
    currentCreditAccountBalance = parseFloat(creditAccount.balance);
    
    let creditAccountCategory = Collection_AccountCategories.findOne(creditAccount.category);
    let creditAccountType = Collection_AccountGroups.findOne(creditAccountCategory.group);
    
    switch (creditAccountType.name) {
      case 'Assets':
        currentCreditAccountBalance -= parseFloat(amount);
        break;
      case 'Liabilities':
        currentCreditAccountBalance += parseFloat(amount);
        break;
      case 'Equity':
        currentCreditAccountBalance += parseFloat(amount);
        break;
      case 'Revenues':
        currentCreditAccountBalance += parseFloat(amount);
        break;
      case 'Expenses':
        currentCreditAccountBalance -= parseFloat(amount);
        break;
    
      default:
        break;
    }
    if (isNaN(currentCreditAccountBalance)) throw new Meteor.Error('Account credit transaction amount is Not a Number');
    Collection_Accounts.update(creditAccountId, { $set: { balance:  currentCreditAccountBalance} });
  }else{
    creditAccountId = false;
  }

  let transaction = {
    debitAccount: debitAccountId,
    debitAccountBalance: currentDebitAccountBalance,
    creditAccount: creditAccountId,
    creditAccountBalance: currentCreditAccountBalance,
    transactiondescription: description,
    transactiondate: date,
    transactionamount: amount,
    appid: appid,
    appunit: appunit,
    additionalField: additionalField,
    formName: formName,
  };

  let extraIds = {
    extraIds: [
      { prefix: 't', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
    ],
  };
  //--- Extra extraIds
  if (extraIds && extraIds.length > 0){
    extraIds.forEach(function (extraId) {
      let count = false;
      let searchLogic = {};
      if (extraId.logic) searchLogic = extraId.logic;
      let lastEntry = Collection_Transactions.find(searchLogic, {limit: 1, sort: {index: -1}}).fetch();
      if (lastEntry && lastEntry.length > 0){
        let entryCount = lastEntry[0][extraId.keyName];
        let indexCount = lastEntry[0]['index'];
        if (entryCount){
          entryCount = entryCount.match(/\d+/)[0];
          entryCount = parseInt(entryCount);
          entryCount++;
          indexCount++;

          let newId = extraId.prefix + entryCount + extraId.postfix;
          transaction[extraId.keyName] = newId;
          transaction['index'] = indexCount;
        }
      }else{
        let entryCount = "1";
        let indexCount = 1;
        let newId = extraId.prefix + entryCount + extraId.postfix;
        transaction[extraId.keyName] = newId;
        transaction['index'] = indexCount;
      }
    }, this);
  }

  return Collection_Transactions.insert(transaction);
}

export function reverseTransaction(transactionId){
  let transactionDetails = Collection_Transactions.findOne(transactionId);
  if (transactionDetails){
    let debitAccountId = transactionDetails.debitAccount;
    let creditAccountId = transactionDetails.creditAccount;
    let amount = transactionDetails.transactionamount;
    if (debitAccountId){
      let debitAccount = Collection_Accounts.findOne(debitAccountId);
      let currentDebitAccountBalance = parseFloat(debitAccount.balance);
      let debitAccountCategory = Collection_AccountCategories.findOne(debitAccount.category);
      let debitAccountType = Collection_AccountGroups.findOne(debitAccountCategory.group);
      
      //--- Reverse values
      switch (debitAccountType.name) {
        case 'Assets':
          currentDebitAccountBalance -= parseFloat(amount);
          break;
        case 'Liabilities':
          currentDebitAccountBalance += parseFloat(amount);
          break;
        case 'Equity':
          currentDebitAccountBalance += parseFloat(amount);
          break;
        case 'Revenues':
          currentDebitAccountBalance += parseFloat(amount);
          break;
        case 'Expenses':
          currentDebitAccountBalance -= parseFloat(amount);
          break;
      
        default:
          break;
      }
      
      if (isNaN(currentDebitAccountBalance)) throw new Meteor.Error('Account debit transaction amount is Not a Number');
      Collection_Accounts.update(debitAccountId, { $set: { balance:  currentDebitAccountBalance} });
      
    }else{
      debitAccountId = false;
    }
    if (creditAccountId){
      let creditAccount = Collection_Accounts.findOne(creditAccountId);
      let currentCreditAccountBalance = parseFloat(creditAccount.balance);
      
      let creditAccountCategory = Collection_AccountCategories.findOne(creditAccount.category);
      let creditAccountType = Collection_AccountGroups.findOne(creditAccountCategory.group);
      
      //--- Reverse values
      switch (creditAccountType.name) {
        case 'Assets':
          currentCreditAccountBalance += parseFloat(amount);
          break;
        case 'Liabilities':
          currentCreditAccountBalance -= parseFloat(amount);
          break;
        case 'Equity':
          currentCreditAccountBalance -= parseFloat(amount);
          break;
        case 'Revenues':
          currentCreditAccountBalance -= parseFloat(amount);
          break;
        case 'Expenses':
          currentCreditAccountBalance += parseFloat(amount);
          break;
      
        default:
          break;
      }
      if (isNaN(currentCreditAccountBalance)) throw new Meteor.Error('Account credit transaction amount is Not a Number');
      Collection_Accounts.update(creditAccountId, { $set: { balance:  currentCreditAccountBalance} });
    }else{
      creditAccountId = false;
    }
  
    return Collection_Transactions.remove(transactionId);
  }
  return false;
}

export function createAccount(accname, accountCategoryByName, appid, appunit, creator, creatorCollection){

  dataScheme = {
    name: accname,
    creator: creator,
    creatorCollection: creatorCollection,
    category: collectionPointer('accountcategories').findOne({ name: accountCategoryByName })._id,
    group_collection: collectionPointer('accountcategories')._name,
    group_inputValue: accountCategoryByName,
    balance: 0,
    appid: appid,
    appunit: appunit,
    deletable: false,
    editable: false,
  };

  let extraIds = [
    { prefix: 'acc', keyName: 'eid', postfix: '' },// prefix, keyName, postfix, logic
  ];
  //--- Extra extraIds
  if (extraIds && extraIds.length > 0){
    extraIds.forEach(function (extraId) {
      let count = false;
      let searchLogic = {};
      if (extraId.logic) searchLogic = extraId.logic;
      let lastEntry = collectionPointer('accounts').find(searchLogic, {limit: 1, sort: {index: -1}}).fetch();
      if (lastEntry && lastEntry.length > 0){
        let entryCount = lastEntry[0][extraId.keyName];
        let indexCount = lastEntry[0]['index'];
        if (entryCount){
          entryCount = entryCount.match(/\d+/)[0];
          entryCount = parseInt(entryCount);
          entryCount++;
          indexCount++;

          let newId = extraId.prefix + entryCount + extraId.postfix;
          dataScheme[extraId.keyName] = newId;
          dataScheme['index'] = indexCount;
        }
      }else{
        let entryCount = "1";
        let indexCount = 1;
        let newId = extraId.prefix + entryCount + extraId.postfix;
        dataScheme[extraId.keyName] = newId;
        dataScheme['index'] = indexCount;
      }
    }, this);
  }
  
  return Collection_Accounts.insert(dataScheme);
}

export function updateAccount(id, accname){
  return Collection_Accounts.update(id, {$set:{ name: accname }});
}

export function deleteAccount(id){
  return Collection_Accounts.remove(id);
}
