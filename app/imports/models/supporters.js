export function getDataValueByKey(data, key){
    let result = false;
        if (key.split('.').length > 0){
            //---Nested Logic
            result = data;
            var loc = key;
            loc = loc.split('.');
            loc.forEach(function(location){
                if (typeof result[location] == 'undefined'){
                    result = '';
                    return false;
                }else{
                    result = result[location];
                }
            });
        }else{
            result = data[key];
        }
        return result;
    
}

export function formatDate(date){
    if (date && new Date(date).toDateString() !== 'Invalid Date'){
      // return (new Date(date).toDateString());
      var parsedDate = new Date(date);
      var dd = parsedDate.getDate();
      var mm = parsedDate.getMonth()+1; 
      var yyyy = parsedDate.getFullYear();
      return (dd + '/' + mm + '/' +yyyy);
    }else if(new Date(date).toDateString() == 'Invalid Date'){
      return date;
    }else{
      return '';
    }

}