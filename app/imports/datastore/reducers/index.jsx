import {combineReducers} from 'redux';
import { ADD_ARTICLE, SET_APPDATA } from "../constants/action-types";

function appDataReducer(state = false, action) {
  if (action.type === SET_APPDATA) {
    state = action.appData;
  }
  return state;
}
rootReducer = combineReducers({
  appData: appDataReducer
});
export default rootReducer;
