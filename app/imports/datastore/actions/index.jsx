import { ADD_ARTICLE, SET_APPDATA } from "../constants/action-types";
export function addArticle(payload) {
  return { type: ADD_ARTICLE, payload };
}
export function setAppData(appData) {
  return { type: SET_APPDATA, appData: appData };
}
