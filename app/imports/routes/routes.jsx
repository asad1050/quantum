import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router';
import history from 'history';

// route components
import Dashboard from '../ui/dashboard/Dashboard';
import SignIn from '../ui/signin/SignIn';
import SignUp from '../ui/signup/SignUp';
import Loading from '../ui/generalPages/Loading';
import AppNotFound from '../ui/generalPages/AppNotFound';


import { Apps } from '../api/collections';
import GlobalConfig from '../config/global.config.js';
import Store from '../datastore/index';
import {setAppData} from '../datastore/actions/index';

window.Store = Store;

const browserHistory = history.createBrowserHistory();
const pages = GlobalConfig().pages;
let routePages = [];
export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/"
          render={props => {
            return (
              <Redirect
                            to={{
                              pathname: "/base",
                              state: { from: props.location }
                            }}
                          />
            );
          } 
          }
          >
      </Route>
      <Route exact path="/appnotfound" component={(props)=> <AppNotFound  {...props} />} />
      <Route exact path="/loading" component={(props)=> <Loading  {...props} />} />
      <PublicRoute exact path="/:appname/signin" component={(props)=> <SignIn  {...props} />} />
      <PublicRoute exact path="/:appname/signup" component={(props)=> <SignUp  {...props} />} />
      <Route exact path="/:appname"
        render={props => <Redirect
                            to={{
                              pathname: "/" + props.match.params.appname + "/dashboard",
                              state: { from: props.location }
                            }}
                          />
        }
        >
      </Route>
      {
        pages.map(function (page, key) {
          if (page.subPages){
            page.subPages.map(function (subPage, sbkey) {
              routePages.push(subPage);
            });
          }else{
            routePages.push(page);
          }
        })
      }
      {
        routePages.map(function (page, key) {
          if (page.privateRoute){
            if (page.dashboard){
              return (
                <PrivateRoute key={key} exact path={'/:appname'+page.url} component={(props)=> <Dashboard {...props} > {page.component} </Dashboard>} />
              );
            }else{
              return (
                <PrivateRoute key={key} exact path={'/:appname'+page.url} component={(props)=> page.component} />
              );
            }
          }else{
            if (page.dashboard){
              return (
                <PublicRoute key={key} exact path={'/:appname'+page.url} component={(props)=> <Dashboard {...props} > {page.component} </Dashboard>} />
              );
            }else{
              return (
                <PublicRoute key={key} exact path={'/:appname'+page.url} component={(props)=> page.component} />
              );
            }
          }
        })
      }
    </Switch>
  </Router>
);

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={function (props) {
        if (props.match.params && props.match.params.appname && sessionStorage.getItem("appname") && props.match.params.appname == sessionStorage.getItem("appname")){
          if (Meteor.userId() && Meteor.userId() === sessionStorage.getItem("authuserid")){
            return (
              <Component {...props} />
            );
          }else{
            return (
              <Redirect
                to={{
                  pathname: "/" + props.match.params.appname + "/signin",
                  state: { from: props.location }
                }}
              />
            );
          }
        }else if(props.match.params && props.match.params.appname){
          Meteor.call('app.fetchData', props.match.params.appname, function (err, res) {
            Meteor.logout();
            if (typeof res !== 'undefined' && res.length > 0 && res[0].active){
              window.location.href="/" + res[0].appname + "/";
              sessionStorage.setItem("appname", res[0].appname);
              sessionStorage.setItem("appid", res[0]._id);
              sessionStorage.setItem("apptype", res[0].apptype);
              sessionStorage.setItem("appplan", res[0].appplan);
              sessionStorage.setItem("authuserid", "");
              sessionStorage.setItem("settings", JSON.stringify(res[0].settings));
            }else{
              sessionStorage.setItem("appname", "");
              sessionStorage.setItem("appid", "");
              sessionStorage.setItem("apptype", "");
              sessionStorage.setItem("appplan", "");
              sessionStorage.setItem("authuserid", "");
              sessionStorage.setItem("settings", "");
              window.location.href="/appnotfound";
            }
          });
          return (
            <Redirect
              to={{
                pathname: "/loading",
                state: { from: props.location }
              }}
            />
          );
        }else{
          return (
            <Redirect
              to={{
                pathname: "/appnotfound",
                state: { from: props.location }
              }}
            />
          );
        }
      }
      }
    />
  );
}

function PublicRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={function (props) {
        if (props.match.params && props.match.params.appname && sessionStorage.getItem("appname") && props.match.params.appname == sessionStorage.getItem("appname")){
          return (
            <Component {...props} />
          );
        }else if(props.match.params && props.match.params.appname){
          Meteor.call('app.fetchData', props.match.params.appname, function (err, res) {
            if (typeof res !== 'undefined' && res.length > 0 && res[0].active){
              window.location.href="/" + res[0].appname + "/";
              sessionStorage.setItem("appname", res[0].appname);
              sessionStorage.setItem("appid", res[0]._id);
              sessionStorage.setItem("apptype", res[0].apptype);
              sessionStorage.setItem("appplan", res[0].appplan);
              sessionStorage.setItem("authuserid", "");
              sessionStorage.setItem("settings", JSON.stringify(res[0].settings));
            }else{
              sessionStorage.setItem("appname", "");
              sessionStorage.setItem("appid", "");
              sessionStorage.setItem("apptype", "");
              sessionStorage.setItem("appplan", "");
              sessionStorage.setItem("authuserid", "");
              sessionStorage.setItem("settings", "");
              window.location.href="/appnotfound";
            }
          });
          return (
            <Redirect
              to={{
                pathname: "/loading",
                state: { from: props.location }
              }}
            />
          );
        }else{
          return (
            <Redirect
              to={{
                pathname: "/appnotfound",
                state: { from: props.location }
              }}
            />
          );
        }
      }
      }
    />
  );
}
