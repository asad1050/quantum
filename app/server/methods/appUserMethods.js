
if (Meteor.isServer){
  Meteor.methods({
    'appUsers.create': (firstName, lastName, email, password) => {
      if (firstName == '' || lastName == '' || email == '' || password == ''){
        throw new Meteor.Error("Please fill all Sign Up Fields");
      }
      let profile = {
        'firstname': firstName,
        'lastname': lastName
      };
      Accounts.createUser({
        'username': email,
        'password': password,
        'email': email,
        'profile': profile
      });
    }
  });
}
