import { Apps } from '../../imports/api/collections';
import collectionPointer from '../../imports/api/collectionPointer';
import { Collection_Users } from '../../imports/api/collections';
import accountingModel from '../../imports/models/accountingModel';

if (Meteor.isServer){
  Meteor.methods({
    'app.initData': (appid, appname) => {
      console.log("CHECKING INIT DATA");
      if (appid || appname){
          if (appname) {
            var app = Apps.findOne({ appname: appname });
            appid = app._id;
          } else {
            var app = Apps.findOne(appid);
          }
        
        if (app){
            //--- Unit
            if (collectionPointer('units').find({ name: 'main' }).count() == 0){
                collectionPointer('units').insert({
                    eid: 'unit_1',
                    index: 1,
                    appid: appid,
                    name: 'main',
                    deletable: false,
                    editable: false,
                });
            }
            var appMainUnit = collectionPointer('units').findOne({ name: 'main' });
          //--- Admin user
          if (Collection_Users.find({ 'profile.appid': appid }).count() == 0){
                Accounts.createUser({
                username: app.appname + '_admin',
                email: app.appemail,
                password: app.appname,
                profile: {
                    appname: app.appname,
                    appid: appid,
                    appunit: appMainUnit._id,
                    firstName: app.appusername,
                    lastName: '',
                    role: 'owner',
                    email: app.appname + '_' +app.appemail,
                }
            });
          }
          var appUnits = collectionPointer('units').find({ appid: appid }).fetch();
          appUnits.forEach((appUnit) => {
              //--- Roles
                if (collectionPointer('roles').find({ 'rolename': 'admin', 'appid': appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('roles').insert({
                        eid: 'c1',
                        index: 1,
                        appid: appid,
                        appunit: appUnit._id,
                        rolename: 'admin',
                        deletable: false,
                        editable: false,
                    });
                }
                //--- Account groups
                if (collectionPointer('accountgroups').find({ 'name': 'Assets', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var assetsGroupId = collectionPointer('accountgroups').insert({
                    index: 1,
                    eid: 'g1',
                    name: 'Assets',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountgroups').find({ 'name': 'Liabilities', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    var liabilitiesGroupId = collectionPointer('accountgroups').insert({
                    index: 2,
                    eid: 'g2',
                    name: 'Liabilities',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountgroups').find({ 'name': 'Equity', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    var equityGroupId = collectionPointer('accountgroups').insert({
                    index: 3,
                    eid: 'g3',
                    name: 'Equity',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountgroups').find({ 'name': 'Revenues', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    var revenuesGroupId = collectionPointer('accountgroups').insert({
                    index: 4,
                    eid: 'g4',
                    name: 'Revenues',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountgroups').find({ 'name': 'Expenses', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    var expensesGroupId = collectionPointer('accountgroups').insert({
                    index: 5,
                    eid: 'g5',
                    name: 'Expenses',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }

                //--- Account categories
                //--- Assets
                if (collectionPointer('accountcategories').find({ 'name': 'Current Assets', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var currentAssetsId = collectionPointer('accountcategories').insert({
                    index: 1,
                    eid: 'c1',
                    name: 'Current Assets',
                    group: assetsGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Assets',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountcategories').find({ 'name': 'Fixed Assets', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var fixedAssetsId = collectionPointer('accountcategories').insert({
                    index: 2,
                    eid: 'c2',
                    name: 'Fixed Assets',
                    group: assetsGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Assets',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountcategories').find({ 'name': 'Other Assets', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var otherAssetsId = collectionPointer('accountcategories').insert({
                    index: 3,
                    eid: 'c3',
                    name: 'Other Assets',
                    group: assetsGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Assets',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                //--- Liabilities
                if (collectionPointer('accountcategories').find({ 'name': 'Short Term Liabilities', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var shortTermLiabilitiesId = collectionPointer('accountcategories').insert({
                    index: 4,
                    eid: 'c4',
                    name: 'Short Term Liabilities',
                    group: liabilitiesGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Liabilities',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountcategories').find({ 'name': 'Long Term Liabilities', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var longTermLiabilitiesId = collectionPointer('accountcategories').insert({
                    index: 5,
                    eid: 'c5',
                    name: 'Long Term Liabilities',
                    group: liabilitiesGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Liabilities',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                //--- Owner Equity
                if (collectionPointer('accountcategories').find({ 'name': 'Owners Equity', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var ownersEquityId = collectionPointer('accountcategories').insert({
                    index: 6,
                    eid: 'c6',
                    name: 'Owners Equity',
                    group: equityGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Equity',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                //--- Revenue
                if (collectionPointer('accountcategories').find({ 'name': 'Direct Revenues', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var directRevenuesId = collectionPointer('accountcategories').insert({
                    index: 7,
                    eid: 'c7',
                    name: 'Direct Revenues',
                    group: equityGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Equity',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountcategories').find({ 'name': 'Indirect Revenues', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var indirectRevenuesId = collectionPointer('accountcategories').insert({
                    index: 8,
                    eid: 'c8',
                    name: 'Indirect Revenues',
                    group: revenuesGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Revenues',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                //--- Expenses
                if (collectionPointer('accountcategories').find({ 'name': 'Operating Expenses', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var operatingExpensesId = collectionPointer('accountcategories').insert({
                    index: 9,
                    eid: 'c9',
                    name: 'Operating Expenses',
                    group: expensesGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Expenses',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accountcategories').find({ 'name': 'General Expenses', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var generalExpensesId = collectionPointer('accountcategories').insert({
                    index: 10,
                    eid: 'c10',
                    name: 'General Expenses',
                    group: expensesGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Expenses',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                //--- Assets
                if (collectionPointer('accountcategories').find({ 'name': 'Accounts Receivable', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var accountsReveivableId = collectionPointer('accountcategories').insert({
                    index: 11,
                    eid: 'c11',
                    name: 'Accounts Receivable',
                    group: assetsGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Assets',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                //---Liabilities
                if (collectionPointer('accountcategories').find({ 'name': 'Accounts Payable', 'appid':appid, 'appunit': appUnit._id  }).count() == 0){
                    var accountsPayableId = collectionPointer('accountcategories').insert({
                    index: 12,
                    eid: 'c12',
                    name: 'Accounts Payable',
                    group: liabilitiesGroupId,
                    group_collection: collectionPointer('accountgroups')._name,
                    group_inputValue: 'Liabilities',
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                

                //--- Accounts
                if (collectionPointer('accounts').find({ 'name': 'Cash In Hand', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 1,
                    eid: 'acc1',
                    name: 'Cash In Hand',
                    category: currentAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Current Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Cash In Bank', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 2,
                    eid: 'acc2',
                    name: 'Cash In Bank',
                    category: currentAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Current Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Accounts Receivable', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 3,
                    eid: 'acc3',
                    name: 'Accounts Receivable',
                    category: currentAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Current Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Inventory', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 4,
                    eid: 'acc4',
                    name: 'Inventory',
                    category: currentAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Current Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }

                if (collectionPointer('accounts').find({ 'name': 'Computer Hardware and Software', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 5,
                    eid: 'acc5',
                    name: 'Computer Hardware and Software',
                    category: fixedAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Fixed Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Furniture and Fixes', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 6,
                    eid: 'acc6',
                    name: 'Furniture and Fixes',
                    category: fixedAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Fixed Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Renovation', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 7,
                    eid: 'acc7',
                    name: 'Renovation',
                    category: fixedAssetsId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Fixed Assets',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Accounts Payable', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 8,
                    eid: 'acc8',
                    name: 'Accounts Payable',
                    category: shortTermLiabilitiesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Short Term Liabilities',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Other Liabilities', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 9,
                    eid: 'acc9',
                    name: 'Other Liabilities',
                    category: shortTermLiabilitiesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Short Term Liabilities',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }

                if (collectionPointer('accounts').find({ 'name': 'Owners Equity', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 10,
                    eid: 'acc10',
                    name: 'Owners Equity',
                    category: ownersEquityId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Owners Equity',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }

                if (collectionPointer('accounts').find({ 'name': 'Sales', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 11,
                    eid: 'acc11',
                    name: 'Sales',
                    category: directRevenuesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Direct Revenues',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Indirect Revenues', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 12,
                    eid: 'acc12',
                    name: 'Indirect Revenues',
                    category: indirectRevenuesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Indirect Revenues',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Discount On Sales', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 13,
                    eid: 'acc13',
                    name: 'Discount On Sales',
                    category: operatingExpensesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Operating Expenses',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'Cost Of Goods', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 14,
                    eid: 'acc14',
                    name: 'Cost Of Goods',
                    category: operatingExpensesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'Operating Expenses',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }
                if (collectionPointer('accounts').find({ 'name': 'General Expenses', 'appid':appid, 'appunit': appUnit._id }).count() == 0){
                    collectionPointer('accounts').insert({
                    index: 15,
                    eid: 'acc15',
                    name: 'General Expenses',
                    category: generalExpensesId,
                    group_collection: collectionPointer('accountcategories')._name,
                    group_inputValue: 'General Expenses',
                    balance: 0,
                    appid: appid,
                    appunit: appUnit._id,
                    deletable: false,
                    editable: false,
                    });
                }



                //--- End Accounts initial data

          });
          


        }
      }
      console.log("SUCCESS");
    },
  });
}
