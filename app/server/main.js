import { Meteor } from 'meteor/meteor';
import { Apps, Collection_Users } from '../imports/api/collections';
import '../imports/modules/modules/methods/appMethods';
import './methods/initDataMethod';

Meteor.startup(() => {

  if (Apps.find({ 'appname': 'base' }).count() == 0){
    let app = {
      'appname': "base",
      'apptitle': "Base",
      'appusername': "admin",
      'appemail': "admin@email.com",
      'apptype': "salesandinventory",
      'appplan': "basic",
      'active': true,
    };
    
    
    let appId = Apps.insert(app);
    newApp = Apps.findOne(appId);
    Accounts.createUser({
      username: app.appusername,
      email: app.appemail,
      password: 'admin',
      profile: {
        appname: newApp.appname,
        appid: newApp._id,
        firstName: 'owner',
        lastName: 'admin',
        role: 'owner',
        email: app.appemail,
      } 
    });
  }

  if (Collection_Users.find({ 'username': 'asad' }).count() == 0){
    Accounts.createUser({
      username: 'asad',
      email: 'asad@evarosoft.com',
      password: 's623362336233',
      profile: {
        appname: 'base',
        appid: 'evarosoft_owner',
        firstName: 'Muhammad',
        lastName: 'Asad',
        role: 'evarosoft_owner',
        email: 'asad@evarosoft.com',
      }
    });
  }
 
});

// Accounts.createUser({
//   username: 'base',
//   email: 'base@mail.com',
//   password: 'base',
//   profile: {
//     appName: 'base',
//     appId: 'yig33w4YDnrAJgHqe',
//     firstName: 'Base',
//     lastName: 'Base',
//   }
// });
