import { Meteor } from 'meteor/meteor';
import {Apps} from '../imports/api/collections.js';

Meteor.startup(() => {
  if (Apps.find({'app': 'base'}).fetch().count() == 0){
    Apps.insert({
      'app': 'base',
      'name': 'base Business',
    });
  }
});
